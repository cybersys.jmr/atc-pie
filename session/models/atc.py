
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtCore import Qt, QAbstractTableModel, QModelIndex
from PyQt5.QtGui import QIcon

from base.phone import LandLineStatus
from base.coords import dist_str
from base.instr import Instruction
from base.strip import strip_mime_type
from base.util import some

from session.config import settings
from session.env import env

from gui.actions import strip_dropped_on_ATC
from gui.misc import signals, IconFile


# ---------- Constants ----------

nearby_dist_threshold = .1 # NM

# -------------------------------



class ATC:
	def __init__(self, callsign):
		self.callsign = callsign
		self.social_name = None
		self.position = None
		self.frequency = None
	
	def toolTipText(self):
		txt = ''
		if self.social_name is not None:
			txt += self.social_name + '\n'
		txt += 'Position: '
		if self.position is None:
			txt += 'unknown'
		else:
			distance = env.radarPos().distanceTo(self.position)
			if distance < nearby_dist_threshold:
				txt += 'nearby'
			else:
				txt += '%s°, %s' % (env.radarPos().headingTo(self.position).readTrue(), dist_str(distance))
		return txt









class AtcTableModel(QAbstractTableModel):
	def __init__(self, parent):
		QAbstractTableModel.__init__(self, parent)
		self.textChat_received_icon = QIcon(IconFile.panel_atcChat)
		self.gui = parent
		self.mouse_drop_has_ALT = False # CAUTION: set by external drop event
		self.ATCs = [] # list of ATC objects to appear in the table view
		self.temp_ATCs = set() # subset of self.ATCs; complementing set being entries known from "updateATC" (to keep until "removeATC")
		self.land_lines = {} # callsign -> LandLineStatus
		self.unread_private_msg_from = set() # callsigns who have sent ATC messages since their last view switch
		self.flashing_land_line_icons = set() # callsigns who have a flashing phone line icon (incoming ringing or placed calling)
		self.flashing_icons_toggle = False # will keep toggling
	
	def _emitAtcChanged(self, callsign, column=None):
		try:
			row = next(i for i, atc in enumerate(self.ATCs) if atc.callsign == callsign)
			self.dataChanged.emit(self.index(row, some(column, 0)), self.index(row, some(column, self.columnCount() - 1)))
		except StopIteration:
			pass
	
	def columnCount(self, parent=QModelIndex()):
		return 3  # 0: phone line; 1: callsign/PMs; 2: frequency

	def rowCount(self, parent=QModelIndex()):
		return len(self.ATCs)
	
	def flags(self, index):
		flags = Qt.ItemIsEnabled
		if index.isValid() and index.column() == 1: # callsign column
			flags |= Qt.ItemIsDropEnabled
		return flags
		
	def data(self, index, role):
		atc = self.ATCs[index.row()]
		col = index.column()
		
		if role == Qt.DisplayRole:
			if col == 1:
				return atc.callsign
			elif col == 2:
				if atc.frequency is not None:
					return str(atc.frequency)
		
		elif role == Qt.DecorationRole:
			if col == 0:
				if atc.callsign in self.flashing_land_line_icons and not self.flashing_icons_toggle:
					return None
				lls = self.landLineStatus(atc.callsign)
				if lls == LandLineStatus.IDLE:
					return QIcon(IconFile.pixmap_telephone_idle)
				elif lls == LandLineStatus.PLACED_CALL:
					return QIcon(IconFile.pixmap_telephone_placedCall)
				elif lls == LandLineStatus.INCOMING_CALL:
					return QIcon(IconFile.pixmap_telephone_incomingCall)
				elif lls == LandLineStatus.IN_CALL:
					return QIcon(IconFile.pixmap_telephone_inCall)
			elif col == 1:
				if atc.callsign in self.unread_private_msg_from:
					return self.textChat_received_icon
		
		elif role == Qt.ToolTipRole:
			if col == 0:
				lls = self.landLineStatus(atc.callsign)
				if lls is None:
					return 'No phone line'
				elif lls == LandLineStatus.IDLE:
					return 'Idle'
				elif lls == LandLineStatus.PLACED_CALL:
					return 'Placed call' if atc.callsign in self.flashing_land_line_icons else 'Still outgoing (hung up or held)'
				elif lls == LandLineStatus.INCOMING_CALL:
					return 'Incoming call' if atc.callsign in self.flashing_land_line_icons else 'Still incoming (hung up or held)'
				elif lls == LandLineStatus.IN_CALL:
					return 'Call in progress'
			else:
				return atc.toolTipText()
	
	
	## ACCESS FUNCTIONS
	
	def knownAtcCallsigns(self, pred=None):
		"""
		"pred" filters list; default = all ATCs.
		"""
		return [atc.callsign for atc in self.ATCs if pred is None or pred(atc)]
	
	def atcOnRow(self, row):
		return self.ATCs[row]
	
	# by callsign, raise KeyError if not in model
	def getATC(self, cs):
		try:
			return next(atc for atc in self.ATCs if atc.callsign == cs)
		except StopIteration:
			raise KeyError(cs)
	
	def landLineStatus(self, callsign):
		return self.land_lines.get(callsign) # or None
	
	def outgoingLandLine(self, callsign):
		status = self.landLineStatus(callsign)
		return status == LandLineStatus.PLACED_CALL or status == LandLineStatus.IN_CALL
	
	def incomingLandLine(self, callsign):
		status = self.landLineStatus(callsign)
		return status == LandLineStatus.INCOMING_CALL or status == LandLineStatus.IN_CALL
	
	def landLinesWithStatus(self, status):
		return [atc for atc, lls in self.land_lines.items() if lls == status]
	
	def handoverInstructionTo(self, atc):
		try:
			frq = self.getATC(atc).frequency # may be None
		except KeyError:
			frq = None
		return Instruction(Instruction.HAND_OVER, arg=atc, arg2=(None if frq is None else str(frq)))
	
	
	## MODIFICATION FUNCTIONS
	
	def _addTempIfNotKnown(self, callsign):
		if not any(atc.callsign == callsign for atc in self.ATCs):
			row = len(self.ATCs)
			self.beginInsertRows(QModelIndex(), row, row)
			self.ATCs.append(ATC(callsign))
			self.endInsertRows()
	
	def _clearOrKeepTemp(self, callsign): # clear from list unless a phone line or PM status requires it to stay
		try:
			row = next(i for i, atc in enumerate(self.ATCs) if atc.callsign == callsign)
			if self.incomingLandLine(callsign) or self.outgoingLandLine(callsign) or callsign in self.unread_private_msg_from:
				self.temp_ATCs.add(callsign) # NOTE: might have been there already
			else:
				self.beginRemoveRows(QModelIndex(), row, row)
				del self.ATCs[row]
				self.endRemoveRows()
		except StopIteration:
			pass
	
	def flashRingingIcons(self):
		self.flashing_icons_toggle = not self.flashing_icons_toggle
		for row, atc in enumerate(self.ATCs):
			if atc.callsign in self.flashing_land_line_icons:
				idx = self.index(row, 0)
				self.dataChanged.emit(idx, idx)
	
	def updateATC(self, callsign, pos, name, frq, insertAtTop=False):
		"""
		Updates an ATC if already present; adds it otherwise with the given details.
		"""
		self.temp_ATCs.discard(callsign) # no more a "temp" if it was at this point
		try:
			row, atc = next((i, atc) for i, atc in enumerate(self.ATCs) if atc.callsign == callsign)
		except StopIteration:
			atc = ATC(callsign)
			row = 0 if insertAtTop else len(self.ATCs)
			self.beginInsertRows(QModelIndex(), row, row)
			if insertAtTop:
				self.ATCs.insert(0, atc)
			else:
				self.ATCs.append(atc)
			signals.newATC.emit(callsign)
			self.endInsertRows()
		atc.social_name = name
		atc.position = pos
		atc.frequency = frq
		self.dataChanged.emit(self.index(row, 0), self.index(row, self.columnCount() - 1)) # whole row
	
	def removeATC(self, callsign):
		self._clearOrKeepTemp(callsign)
	
	def setLandLineStatus(self, callsign, status, flash=False):
		if status is None:
			try:
				del self.land_lines[callsign]
			except KeyError:
				pass
		else:
			self.land_lines[callsign] = status
		if flash:
			self.flashing_land_line_icons.add(callsign)
		else:
			self.flashing_land_line_icons.discard(callsign)
		if status is None or status == LandLineStatus.IDLE:
			if callsign in self.temp_ATCs:
				self._clearOrKeepTemp(callsign)
		else:
			self._addTempIfNotKnown(callsign)
		self._emitAtcChanged(callsign, column=0)
	
	def markUnreadPMs(self, callsign, b):
		if b:
			self._addTempIfNotKnown(callsign)
			self.unread_private_msg_from.add(callsign)
		else:
			self.unread_private_msg_from.discard(callsign)
			if callsign in self.temp_ATCs:
				self._clearOrKeepTemp(callsign)
		self._emitAtcChanged(callsign, column=1)
	
	def clear(self):
		self.beginResetModel()
		self.ATCs.clear()
		self.temp_ATCs.clear()
		self.land_lines.clear()
		self.flashing_land_line_icons.clear()
		self.unread_private_msg_from.clear()
		self.endResetModel()
	
	
	## MOUSE STUFF
	
	def itemDoubleClicked(self, index, shift):
		atc = self.atcOnRow(index.row()).callsign
		if shift:
			pos = self.getATC(atc).position
			if pos is None:
				signals.statusBarMsg.emit('Position unknown for %s' % atc)
			else:
				signals.indicatePoint.emit(pos)
		else: # double-clicked without SHIFT
			col = index.column()
			if col == 0:
				llm = settings.session_manager.landLineManager()
				if llm is not None and self.landLineStatus(atc) is not None:
					if self.outgoingLandLine(atc):
						llm.dropLandLine(atc)
					else:
						llm.requestLandLine(atc)
			elif col == 1:
				signals.privateAtcChatRequest.emit(atc)
	
	def supportedDropActions(self):
		return Qt.MoveAction
	
	def mimeTypes(self):
		return [strip_mime_type]
	
	def dropMimeData(self, mime, drop_action, row, column, parent):
		if drop_action == Qt.MoveAction and mime.hasFormat(strip_mime_type):
			strip = env.strips.fromMimeDez(mime)
			atc_callsign = self.ATCs[parent.row()].callsign
			strip_dropped_on_ATC(strip, atc_callsign, self.mouse_drop_has_ALT)
			return True
		return False
