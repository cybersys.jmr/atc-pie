
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from datetime import timedelta, timezone
from sys import stderr

from PyQt5.QtCore import Qt, pyqtSignal, QRegExp, QTime
from PyQt5.QtGui import QIcon, QRegExpValidator
from PyQt5.QtWidgets import QWidget, QInputDialog, QMessageBox, QLineEdit, QComboBox, QTimeEdit, QMenu, QAction

from ui.cpdlcPanel import Ui_cpdlcPanel
from ui.cpdlcConnection import Ui_cpdlcConnection

from base.cpdlc import CpdlcMessage, CPDLC_free_text_element, RspId, CPDLC_element_display_text
from base.fpl import FPL
from base.instr import Instruction
from base.strip import assigned_SQ_detail, assigned_heading_detail, assigned_altitude_detail, assigned_speed_detail
from base.utc import now, rel_datetime_str
from base.util import pop_all

from session.env import env
from session.config import settings
from session.manager import teacher_callsign, SessionType, CpdlcOperationBlocked
from session.models.dataLinks import CpdlcHistoryFilterModel, CpdlcConnectionModel, ConnectionStatus

from gui.actions import send_strip, instruction_to_strip, non_teacher_cpdlc_transfer
from gui.misc import selection, signals, IconFile, SimpleStringListModel
from gui.widgets.basicWidgets import flash_widget, HeadingEditWidget, AltFlEditWidget, SpeedEditWidget, FrequencyPickCombo
from gui.widgets.miscWidgets import RadioKeyEventFilter, XpdrCodeSelectorWidget
from gui.dialog.miscDialogs import yesNo_question


# ---------- Constants ----------

dialogue_window_flash_stylesheet = 'QLabel#acftCallsign_info { background-color: yellow }'
message_buffer_flash_stylesheet = 'QListView#msgBuffer_view { background-color: yellow }'
max_recommended_element_count = 4

# -------------------------------

# Each class below is a widget usable for one or more types of argument in CPDLC message elements.
# It must contain:
#  - an initValue(callsign) method initialising the value to display on msg element selection (arg is connected ACFT);
#  - an argStr() method encoding its argument, so presumably without spaces unless arg alone or last.

class AltArgWidget(AltFlEditWidget):
	def __init__(self, parent):
		AltFlEditWidget.__init__(self, parent)
		self.syncWithEnv(True)
	def initValue(self, callsign):
		alt = None
		strip = env.strips.findUniqueForCallsign(callsign)
		if strip is not None:
			alt = strip.lookup(assigned_altitude_detail)
		if alt is not None:
			self.setAltFlSpec(alt)
	def argStr(self):
		return self.altFlSpec().toStr(unit=False) # avoid space before unit

class ChoiceArgWidget(QComboBox):
	def __init__(self, parent, str_items):
		QComboBox.__init__(self, parent)
		self.addItems(str_items)
	def initValue(self, callsign):
		self.setCurrentIndex(0)
	def argStr(self):
		return self.currentText().upper()

class FrqArgWidget(FrequencyPickCombo):
	def __init__(self, parent):
		FrequencyPickCombo.__init__(self, parent)
	def initValue(self, callsign):
		pass # not much better to propose than last state of widget
	def argStr(self):
		frq = self.getFrequency()
		return '' if frq is None else str(frq)

class HeadingArgWidget(HeadingEditWidget):
	def __init__(self, parent):
		HeadingEditWidget.__init__(self, parent)
	def initValue(self, callsign):
		hdg = None
		strip = env.strips.findUniqueForCallsign(callsign)
		if strip is not None:
			hdg = strip.lookup(assigned_heading_detail)
		if hdg is not None:
			self.setValue(int(hdg.magneticAngle()))
	def argStr(self):
		return self.headingValue(False).read()

class SpeedArgWidget(SpeedEditWidget):
	def __init__(self, parent):
		SpeedEditWidget.__init__(self, parent)
	def initValue(self, callsign):
		spd = None
		strip = env.strips.findUniqueForCallsign(callsign)
		if strip is not None:
			spd = strip.lookup(assigned_speed_detail)
		if spd is not None:
			self.setValue(int(spd.kt))
	def argStr(self):
		return str(self.speedValue().kt)

class TxtArgWidget(QLineEdit):
	def __init__(self, parent, tool_tip, allow_spaces):
		QLineEdit.__init__(self, parent)
		self.setToolTip(tool_tip)
		self.setClearButtonEnabled(True)
		if not allow_spaces:
			self.setValidator(QRegExpValidator(QRegExp('[^ ]*')))
	def initValue(self, callsign):
		self.clear()
	def argStr(self):
		return self.text()

class RouteArgWidget(QLineEdit):
	def __init__(self, parent):
		QLineEdit.__init__(self, parent)
		self.setToolTip('Route')
		self.setClearButtonEnabled(True)
	def initValue(self, callsign):
		rte = None
		strip = env.strips.findUniqueForCallsign(callsign)
		if strip is not None:
			rte = strip.lookup(FPL.ROUTE)
		if rte is None:
			self.clear()
		else:
			self.setText(rte)
	def argStr(self):
		return self.text()

class TimeArgWidget(QTimeEdit):
	def __init__(self, parent):
		QTimeEdit.__init__(self, parent)
		self.setToolTip('UTC')
	def initValue(self, callsign):
		t = now() + timedelta(minutes=15)
		self.setTime(QTime(t.hour, 10 * (t.minute // 10)))
	def argStr(self):
		t = self.time().toPyTime().replace(tzinfo=timezone.utc)
		return '%02d%02dZ' % (t.hour, t.minute)

class XpdrCodeArgWidget(XpdrCodeSelectorWidget):
	def __init__(self, parent):
		XpdrCodeSelectorWidget.__init__(self, parent)
	def initValue(self, callsign):
		sq = None
		strip = env.strips.findUniqueForCallsign(callsign)
		if strip is not None:
			sq = strip.lookup(assigned_SQ_detail)
		if sq is not None:
			self.setSQ(sq)
	def argStr(self):
		return '%04o' % self.getSQ()
	


# CPDLC message element argument types (append '2' if second argument of same type in element):
#  Alt: altitude/FL
#  Atc: ATC callsign
#  Dev: deviation type
#  Dir: left/right
#  Frq: frequency
#  Hdg: heading
#  Leg: holding leg type
#  Pos: position report
#  Pt:  point
#  Rsn: reason
#  Rte: route
#  Spd: speed
#  Sq:  transponder code
#  Tz:  UTC time

def mk_msg_element_arg_widgets(parent):
	return {
		'Alt': AltArgWidget(parent),
		'Atc': TxtArgWidget(parent, 'ATC callsign', False),
		'Dev': ChoiceArgWidget(parent, ['Lateral', 'Level', 'Speed']),
		'Dir': ChoiceArgWidget(parent, ['Left', 'Right']),
		'Frq': FrqArgWidget(parent),
		'Hdg': HeadingArgWidget(parent),
		'Leg': TxtArgWidget(parent, 'Leg type', True),
		'Pos': TxtArgWidget(parent, 'Position report', True),
		'Pt':  TxtArgWidget(parent, 'Point', False),
		'Pt2': TxtArgWidget(parent, 'Point', False),
		'Rsn': TxtArgWidget(parent, 'Reason', True),
		'Rte': RouteArgWidget(parent),
		'Spd': SpeedArgWidget(parent),
		'Sq':  XpdrCodeArgWidget(parent),
		'Tz':  TimeArgWidget(parent),
		'Tz2': TimeArgWidget(parent)
	}

# Msg element format tuples below: element identifier, menu hint, ordered arguments (see above)
uplink_element_formats = [
	('RTEU-2', 'Proceed direct', 'Pt'),
	('RTEU-3', 'At time proceed direct', 'Tz Pt'),
	('RTEU-4', 'At position proceed direct', 'Pt Pt2'),
	('RTEU-6', 'Cleared to position via', 'Pt Rte'),
	('RTEU-11', 'Hold spec', 'Pt Hdg Dir Leg'),
	('RTEU-12', 'Hold as published', 'Pt'),
	('RTEU-13', 'Expect further clearance at', 'Tz'),
	('RTEU-16', 'Position?', ''),
	('RTEU-17', 'ETA?', 'Pt'),
	
	('LATU-9', 'Resume own navigation', ''),
	('LATU-11', 'Turn left/right heading', 'Dir Hdg'),
	('LATU-12', 'Turn left/right track', 'Dir Hdg'),
	('LATU-16', 'Fly heading', 'Hdg'),
	('LATU-19', 'Report passing', 'Pt'),
	
	('LVLU-5', 'Maintain alt./FL', 'Alt'),
	('LVLU-6', 'Climb', 'Alt'),
	('LVLU-8', 'At position climb', 'Tz Alt'),
	('LVLU-9', 'Descend', 'Alt'),
	('LVLU-11', 'At position descend', 'Tz Alt'),
	
	('CSTU-1', 'Cross position at alt./FL', 'Pt Alt'),
	('CSTU-2', 'Cross position at or above', 'Pt Alt'),
	('CSTU-3', 'Cross position at or below', 'Pt Alt'),
	('CSTU-4', 'Cross position at time', 'Pt Tz'),
	('CSTU-5', 'Cross position before', 'Pt Tz'),
	('CSTU-6', 'Cross position after', 'Pt Tz'),
	('CSTU-7', 'Cross position between times', 'Pt Tz Tz2'),
	
	('SPDU-4', 'Maintain speed', 'Spd'),
	('SPDU-5', 'Maintain present speed', ''),
	('SPDU-9', 'Increase speed', 'Spd'),
	('SPDU-11', 'Reduce speed', 'Spd'),
	('SPDU-13', 'Resume normal speed', ''),
	
	('ADVU-2', 'Service terminated', ''),
	('ADVU-3', 'Identified', 'Pt'),
	('ADVU-4', 'Identification lost', ''),
	('ADVU-6', 'Request with next ATC', ''),
	('ADVU-9', 'Squawk', 'Sq'),
	('ADVU-15', 'Squawk ident', ''),
	('ADVU-19', 'Deviation detected', 'Dev'),
	
	# "other"
	('COMU-1', 'Contact ATC', 'Atc Frq'),
	('COMU-2', 'At position contact ATC', 'Pt Atc Frq'),
	('COMU-5', 'Monitor ATC', 'Atc Frq'),
	('EMGU-1', 'Say fuel and POB', ''),
	('SUPU-1', 'When ready', ''),
	('SUPU-2', 'Due to', 'Rsn'),
	('SUPU-3', 'Expedite', '')
]

downlink_element_formats = [
	('RTED-1', 'Request direct', 'Pt'),
	('RTED-5', 'Position report', 'Pos'),
	('RTED-6', 'Request heading', 'Hdg'),
	('RTED-7', 'Request track', 'Hdg'),
	('RTED-8', 'When can we expect back on route?', ''),
	('RTED-10', 'ETA', 'Pt Tz'),
	
	('LATD-3', 'Clear of weather', ''),
	('LATD-4', 'Back on route', ''),
	('LATD-8', 'Passing position', 'Pt'),
	
	('LVLD-1', 'Request FL', 'Alt'),
	('LVLD-6', 'When can we expect lower?', ''),
	('LVLD-7', 'When can we expect higher?', ''),
	
	('SPDD-2', 'When can we expect speed?', 'Spd'),
	('SPDD-6', 'We cannot accept', 'Spd'),
	
	('ADVD-1', 'Squawking', 'Sq'),
	
	# "other"
	('COMD-1', 'Request voice contact', 'Frq'),
	('EMGD-1', 'Pan-pan', ''),
	('EMGD-2', 'Mayday', ''),
	('EMGD-4', 'Cancel EMG', ''),
	('SUPD-1', 'Due to', 'Rsn')
]



class CpdlcConnectionWidget(QWidget, Ui_cpdlcConnection):
	closing = pyqtSignal()
	
	def __init__(self, parent, data_link_model):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.clearMsgBuffer_button.setIcon(QIcon(IconFile.button_clear))
		self.cancelManualElt_button.setIcon(QIcon(IconFile.button_clear))
		self.freeText_edit.setClearButtonEnabled(True)
		self.appendFreeText_button.setEnabled(False)
		self.atc_pov = settings.session_manager.session_type != SessionType.TEACHER
		self.transfer_button.setVisible(self.atc_pov) # ACFT cannot transfer
		self.wilco_button.setVisible(not self.atc_pov) # only ACFT can WILCO
		self.msgElementArgs_widgets = mk_msg_element_arg_widgets(self)
		menu_buttons = {
			'RTE': self.rteEltId_button, 'LAT': self.latEltId_button, 'LVL': self.lvlEltId_button,
			'CST': self.cstEltId_button, 'SPD': self.spdEltId_button, 'ADV': self.advEltId_button
		}
		elt_menus = {group: QMenu(self) for group in menu_buttons}
		other_elt_menu = QMenu(self)
		for elt_spec in uplink_element_formats if self.atc_pov else downlink_element_formats:
			elt_id, menu_hint, arg_fmt = elt_spec
			action = QAction('%s: %s' % (elt_id, menu_hint), self)
			action.triggered.connect(lambda ignore_checked, spec=elt_spec: self.manualElementIdSelected(*spec))
			elt_menus.get(elt_id[:3], other_elt_menu).addAction(action)
		for group, menu in elt_menus.items():
			button = menu_buttons.get(group, self.otherEltId_button)
			if menu.isEmpty():
				button.hide()
			else:
				button.setMenu(menu)
		self.otherEltId_button.setMenu(other_elt_menu)
		for w in self.msgElementArgs_widgets.values():
			w.hide()
		self.message_buffer_display_model = SimpleStringListModel(self, False) # reordering here will not sync with "self.message_buffer"
		self.msgBuffer_view.setModel(self.message_buffer_display_model)
		self.data_link_model = data_link_model # CAUTION: accessed for identity test outside of class
		self.messages_tableView.setModel(self.data_link_model)
		self.message_buffer = []
		self.current_manual_element = None
		# initialise display
		self.linkStatusChanged()
		self.clearMessageBuffer()
		# buttons/actions, signal connections
		self.freeText_edit.textChanged.connect(lambda txt: self.appendFreeText_button.setEnabled(len(txt) > 0))
		self.transfer_button.clicked.connect(self.transferButtonClicked)
		self.disconnect_button.clicked.connect(self.disconnectLink)
		self.resolveProblems_button.clicked.connect(self.resolveProblems)
		self.acceptTransfer_button.clicked.connect(lambda: self.acceptRejectTransfer(True))
		self.rejectTransfer_button.clicked.connect(lambda: self.acceptRejectTransfer(False))
		self.clearMsgBuffer_button.clicked.connect(self.clearMessageBuffer)
		self.sendMsgBuffer_button.clicked.connect(self.sendMessageBuffer)
		self.instructAsRequested_button.clicked.connect(self.appendRequestedInstructions)
		self.sendMsgPanel_select.currentIndexChanged.connect(self.switchPanel)
		self.roger_button.clicked.connect(lambda: self.sendShortMessage(RspId.uplink_ROGER if self.atc_pov else RspId.downlink_ROGER))
		self.wilco_button.clicked.connect(lambda: self.sendShortMessage(RspId.downlink_WILCO)) # button should not be visible as ATC
		self.unable_button.clicked.connect(lambda: self.sendShortMessage(RspId.uplink_UNABLE if self.atc_pov else RspId.downlink_UNABLE))
		self.affirm_button.clicked.connect(lambda: self.sendShortMessage(RspId.uplink_AFFIRM if self.atc_pov else RspId.downlink_AFFIRM))
		self.negative_button.clicked.connect(lambda: self.sendShortMessage(RspId.uplink_NEGATIVE if self.atc_pov else RspId.downlink_NEGATIVE))
		self.stby_button.clicked.connect(lambda: self.sendShortMessage(RspId.uplink_STANDBY if self.atc_pov else RspId.downlink_STANDBY))
		self.appendManualElement_button.clicked.connect(self.appendManualElement)
		self.cancelManualElt_button.clicked.connect(self.clearManualElementId)
		self.appendFreeText_button.clicked.connect(self.appendFreeTextElement)
		# CAUTION: the following connections must be disconnected before window deletion
		self.data_link_model.statusChanged.connect(self.linkStatusChanged)
		self.data_link_model.rowsInserted.connect(self.messages_tableView.scrollToBottom)
		self.data_link_model.statusChanged.connect(self._checkAutoClose)
	
	def closeEvent(self, event):
		self.data_link_model.rowsInserted.disconnect(self.messages_tableView.scrollToBottom)
		self.data_link_model.statusChanged.disconnect(self.linkStatusChanged)
		self.data_link_model.statusChanged.disconnect(self._checkAutoClose)
		self.closing.emit()
		self.deleteLater()
	
	def _checkAutoClose(self):
		if settings.CPDLC_closes_windows:
			if self.data_link_model.isTerminated() and self.data_link_model.statusColour() == ConnectionStatus.OK:
				self.close()
	
	def linkStatusChanged(self):
		lvnoxfr = self.data_link_model.isLive() and self.data_link_model.pendingTransferTo() is None
		# UPDATE DISPLAY/BUTTONS
		self.acftCallsign_info.setText(self.data_link_model.acftCallsign())
		self.status_info.setText(self.data_link_model.statusStr())
		pbt = self.data_link_model.markedProblemTime()
		self.status_info.setToolTip('' if pbt is None else 'Problem occurred at ' + rel_datetime_str(pbt))
		# Top buttons
		self.transfer_button.setEnabled(self.data_link_model.isLive())
		self.disconnect_button.setEnabled(lvnoxfr)
		self.resolveProblems_button.setVisible(self.data_link_model.statusColour() == ConnectionStatus.PROBLEM)
		self.acceptRejectTransfer_panel.setVisible(self.atc_pov and self.data_link_model.pendingTransferFrom() is not None)
		# Bottom
		self.send_frame.setVisible(lvnoxfr)
		self.instructAsRequested_button.setVisible(self.atc_pov and self.data_link_model.pendingInstrMsg(False) is not None)
	
	def appendElementsToMsgBuffer(self, msg_elements):
		self.message_buffer.extend(msg_elements)
		for elt in msg_elements:
			self.message_buffer_display_model.appendString(CPDLC_element_display_text(elt))
		self.messageBuffer_widget.show()
		self.sendMsgBuffer_button.setEnabled(True)
		self.msgBuffer_view.scrollToBottom()
		if len(self.message_buffer) > max_recommended_element_count:
			QMessageBox.warning(self, 'Long CPDLC message',
					'You are exceeding the recommended maximum CPDLC message element count. Consider splitting.')
		self.quickStd_page.setEnabled(False)
		if self.sendMsgPanel_stack.currentWidget() is self.quickStd_page:
			self.sendMsgPanel_stack.setCurrentWidget(self.manualElement_page)
		flash_widget(self, message_buffer_flash_stylesheet)
	
	def clearMessageBuffer(self):
		self.message_buffer.clear()
		self.sendMsgBuffer_button.setEnabled(False)
		self.messageBuffer_widget.hide()
		self.message_buffer_display_model.clearList()
		self.quickStd_page.setEnabled(True)
	
	def sendMessageBuffer(self):
		if self.sendMsgPanel_stack.currentWidget() is self.freeTxt_page and len(self.freeText_edit.text()) > 0:
			QMessageBox.critical(self, 'Forgotten CPDLC input?', 'Text line input is not empty. Please append or clear text.')
		elif self.sendMsgPanel_stack.currentWidget() is self.manualElement_page \
				and self.manualElement_stack.currentWidget() is self.eltIdSelected_page:
			QMessageBox.critical(self, 'Forgotten CPDLC input?', 'Manual element input is incomplete. Please append or clear panel.')
		else:
			msg = CpdlcMessage(self.atc_pov, self.message_buffer[:])
			acft_callsign = self.data_link_model.acftCallsign()
			try:
				settings.session_manager.sendCpdlcMsg(acft_callsign, msg)
				instrlst = msg.recognisedInstructions()
				if instrlst is not None:
					for instr in instrlst:
						instruction_to_strip(instr, callsign=acft_callsign)
				self.data_link_model.appendMessage(msg)
				self.clearMessageBuffer()
			except CpdlcOperationBlocked as err:
				QMessageBox.critical(self, 'CPDLC error', str(err))
	
	
	## TOP BUTTONS
	
	def transferButtonClicked(self): # NOTE button hidden to teacher
		was_pending = self.data_link_model.pendingTransferTo()
		if was_pending is None: # new transfer
			if self.data_link_model.expectingMsg() or self.data_link_model.markedProblemTime():
				QMessageBox.warning(self, 'CPDLC transfer warning', 'Connection still expecting a message or has problems to resolve.')
			items = set(env.ATCs.knownAtcCallsigns())
			if settings.session_manager.session_type == SessionType.STUDENT:
				items.discard(teacher_callsign)
			if len(items) == 0:
				QMessageBox.critical(self, 'CPDLC transfer error', 'No ATC to transfer authority to.')
			else:
				item, ok = QInputDialog.getItem(self, 'Transfer data authority', 'Select ATC:', list(items), editable=False)
				if ok:
					non_teacher_cpdlc_transfer(self.data_link_model, item)
		elif yesNo_question(self, 'Abort pending transfer',
					'A transfer to %s is already pending.' % was_pending, 'Abort?'):
			try:
				settings.session_manager.sendCpdlcTransferRequest(self.data_link_model.acftCallsign(), was_pending, False)
				self.data_link_model.setTransferTo(None)
			except CpdlcOperationBlocked as err:
				QMessageBox.critical(self, 'CPDLC error', str(err))
	
	def disconnectLink(self):
		cs = self.data_link_model.acftCallsign()
		if yesNo_question(self, 'Terminate data link', 'Disconnect current data link with %s?' % cs):
			try:
				settings.session_manager.disconnectCpdlc(cs)
				self.data_link_model.terminate(self.atc_pov)
			except CpdlcOperationBlocked as err:
				QMessageBox.critical(self, 'CPDLC error', str(err))
	
	def resolveProblems(self):
		self.data_link_model.resolveProblems()
	
	def acceptRejectTransfer(self, accept):
		xfr = self.data_link_model.pendingTransferFrom()
		if xfr is not None:
			try:
				settings.session_manager.sendCpdlcTransferResponse(self.data_link_model.acftCallsign(), xfr, accept)
				if accept:
					self.data_link_model.acceptIncomingTransfer()
				else:
					self.data_link_model.terminate(False)
			except CpdlcOperationBlocked as err:
				QMessageBox.critical(self, 'CPDLC error', str(err))
	
	
	## MSG BUFFER BUTTONS
	
	def appendRequestedInstructions(self):
		# assuming ATC PoV (not teaching)
		pendmsg = self.data_link_model.pendingInstrMsg(False) # contains recognised requested instructions
		if pendmsg is not None:
			acft = env.radarContactByCallsign(self.data_link_model.acftCallsign())
			self.appendElementsToMsgBuffer([instr.toCpdlcUplinkMsgElt(acft) for instr in pendmsg.recognisedInstructions()])
	
	def switchPanel(self, page_number):
		self.sendMsgPanel_select.setCurrentIndex(page_number) # needed in case switchPanel is called programmatically
		self.sendMsgPanel_stack.setCurrentIndex(page_number)
		if self.sendMsgPanel_stack.currentWidget() is self.freeTxt_page:
			self.freeText_edit.setFocus()
	
	
	## SEND PANEL BUTTONS
	
	## Manual element page
	def manualElementIdSelected(self, elt_id, menu_hint, arg_fmt):
		self.current_manual_element = elt_id
		self.manualElement_stack.setCurrentWidget(self.eltIdSelected_page)
		self.selectedEltId_info.setText('[%s]' % menu_hint)
		self.selectedEltId_info.setToolTip(elt_id)
		# Populate the argument zone
		first = True
		for arg in arg_fmt.split():
			widget = self.msgElementArgs_widgets[arg]
			widget.initValue(self.data_link_model.acftCallsign())
			self.msgElementArgs_layout.addWidget(widget)
			widget.show()
			if first:
				widget.setFocus()
				first = False
	
	def clearManualElementId(self):
		self.current_manual_element = None
		self.manualElement_stack.setCurrentWidget(self.eltIdChoice_page)
		# Clear the message argument widget
		w = self.msgElementArgs_layout.takeAt(0)
		while w:
			w.widget().hide()
			w = self.msgElementArgs_layout.takeAt(0)
	
	def appendManualElement(self):
		elt_str = self.current_manual_element
		for i in range(self.msgElementArgs_layout.count()):
			elt_str += ' ' + self.msgElementArgs_layout.itemAt(i).widget().argStr()
		self.appendElementsToMsgBuffer([elt_str])
		self.clearManualElementId()
	
	## Free text page
	def appendFreeTextElement(self):
		self.appendElementsToMsgBuffer([CPDLC_free_text_element(self.atc_pov, self.freeText_edit.text())])
		self.freeText_edit.clear()
	
	## Short message page
	def sendShortMessage(self, rsp_id):
		acft_callsign = self.data_link_model.acftCallsign()
		if rsp_id == RspId.downlink_WILCO:
			acft = next((acft for acft in settings.session_manager.getAircraft() if acft.identifier == acft_callsign), None)
			pendmsg = self.data_link_model.pendingInstrMsg(True) # contains recognised uplink instructions
			if acft is not None and pendmsg is not None and yesNo_question(self,
					'CPDLC instruction acknowledgement', pendmsg.displayText(), 'Execute with ACFT?'):
				try:
					instructions = pendmsg.recognisedInstructions()
					acft.instruct(instructions, True) # read back to allow checking + answer to "say intentions"
					for instr in instructions:
						instruction_to_strip(instr, callsign=acft_callsign)
				except Instruction.Error as err:
					QMessageBox.critical(self, 'CPDLC instruction error', 'Unable to perform instruction: %s\nAborting.' % err)
					return
		msg = CpdlcMessage(self.atc_pov, rsp_id)
		try:
			settings.session_manager.sendCpdlcMsg(acft_callsign, msg)
			self.data_link_model.appendMessage(msg)
		except CpdlcOperationBlocked as err:
			QMessageBox.critical(self, 'CPDLC error', str(err))






class CpdlcPanel(QWidget, Ui_cpdlcPanel):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.clearHistory_button.setIcon(QIcon(IconFile.button_clear))
		self.last_selected_callsign = '' # will filter all ACFT
		self.filter_model = CpdlcHistoryFilterModel(self, env.cpdlc)
		self.connections_tableView.setModel(self.filter_model)
		self.open_child_windows = [] # CpdlcConnectionWidget list
		# Signals
		self.connections_tableView.doubleClicked.connect(self.openFilteredDataLinkWindow)
		self.connections_tableView.selectionModel().selectionChanged.connect(self.tableSelectionChanged)
		self.activeConnections_radioButton.toggled.connect(self.filter_model.setActiveConnFilter)
		self.historyWith_radioButton.toggled.connect(self.updateCallsignFilter)
		self.closeOpenWindows_button.clicked.connect(self.closeChildWindows)
		self.clearHistory_button.clicked.connect(self.clearTerminatedDialoguesFromHistory)
		# Finish set-up
		self.activeConnections_radioButton.setChecked(True)
		signals.cpdlcTransferRequest.connect(self.receiveTransferRequest)
		signals.cpdlcTransferResponse.connect(self.receiveTransferResponse)
		signals.cpdlcInitLink.connect(self._checkAutoRaise)
		signals.cpdlcMessageReceived.connect(self.msgReceived)
		signals.appendCpdlcMsgElement.connect(self._catchMessageElementToAppend)
		signals.cpdlcProblem.connect(self._checkAutoRaise) # signal has more arg's than method
		signals.cpdlcWindowRequest.connect(self.openCallsignLatestDataLinkWindow)
		signals.selectionChanged.connect(self.updateCallsignFilter)
		signals.selectionChanged.connect(self.connections_tableView.clearSelection)
		signals.sessionEnded.connect(self.closeChildWindows)
		env.cpdlc.clearingFromHistory.connect(self.closeDataLinkWindow)
	
	def _checkAutoRaise(self, callsign): # CAUTION: cpdlcProblem signal connected with more arg's
		if settings.CPDLC_raises_windows and not settings.session_start_temp_lock:
			self.openCallsignLatestDataLinkWindow(callsign, False)
	
	def _catchMessageElementToAppend(self, callsign, msg_element):
		link = env.cpdlc.lastDataLink(callsign)
		if link is not None:
			window = self.getOpenDataLinkWindow(link)
			window.appendElementsToMsgBuffer([msg_element])
	
	def updateCallsignFilter(self):
		sel = selection.selectedCallsign()
		if sel is not None:
			self.last_selected_callsign = sel
			self.historyWith_radioButton.setText('History with ' + sel)
		if self.historyWith_radioButton.isChecked():
			self.filter_model.setCallsignFilter(self.last_selected_callsign)
		else: # user wants to filter on live status (accept all callsigns)
			self.filter_model.setCallsignFilter(None)
	
	def globalSelectionChanged(self): # CAUTION: this could be the result of clearing because of a table selection change
		if selection.selectedCallsign() is not None:
			self.connections_tableView.clearSelection()
	
	def tableSelectionChanged(self): # CAUTION: this could be the result of clearing because of a global selection change
		if len(self.connections_tableView.selectionModel().selectedIndexes()) > 0 :
			selection.deselect()
	
	def msgReceived(self, sender, msg):
		if not msg.isAcknowledgement():
			self._checkAutoRaise(sender)
	
	def getOpenDataLinkWindow(self, data_link_model):
		try:
			window = next(w for w in self.open_child_windows if w.data_link_model is data_link_model)
		except StopIteration:
			window = CpdlcConnectionWidget(self, data_link_model)
			window.setWindowFlags(Qt.Window)
			window.setWindowTitle('%s data link window for %s' % (settings.my_callsign, data_link_model.acftCallsign()))
			window.setWindowIcon(QIcon(IconFile.panel_CPDLC))
			window.installEventFilter(RadioKeyEventFilter(self))
			self.open_child_windows.append(window)
			window.closing.connect(lambda w=window: pop_all(self.open_child_windows, lambda w2: w2 is w))
		window.show()
		window.raise_()
		return window
	
	def openFilteredDataLinkWindow(self, index):
		link = env.cpdlc.dataLinkOnRow(self.filter_model.mapToSource(index).row())
		flash_widget(self.getOpenDataLinkWindow(link), dialogue_window_flash_stylesheet)
	
	def openCallsignLatestDataLinkWindow(self, callsign, only_if_live):
		link = env.cpdlc.lastDataLink(callsign)
		if link is None:
			QMessageBox.warning(self, 'Last CPDLC dialogue', 'No CPDLC history for %s.' % callsign)
		elif not only_if_live or link.isLive():
			flash_widget(self.getOpenDataLinkWindow(link), dialogue_window_flash_stylesheet)
	
	def closeDataLinkWindow(self, data_link_model):
		try:
			next(w for w in self.open_child_windows if w.data_link_model is data_link_model).close()
		except StopIteration:
			pass
	
	def closeChildWindows(self):
		for w in self.open_child_windows[:]: # copy list because each close action modifies the original
			w.close()
	
	def clearTerminatedDialoguesFromHistory(self):
		env.cpdlc.clearHistory(CpdlcConnectionModel.isTerminated)
	
	def receiveTransferRequest(self, callsign, atc, proposing):
		link = env.cpdlc.lastDataLink(callsign)
		if proposing and (link is None or link.isTerminated()): # ATC proposing us a transfer
			if settings.controller_pilot_data_link:
				env.cpdlc.beginDataLink(callsign, transferFrom=atc)
			else:
				try:
					settings.session_manager.sendCpdlcTransferResponse(callsign, atc, False) # automatically reject
				except CpdlcOperationBlocked:
					pass # no point looping; we were just trying to be nice and send note that we were not accepting XFRs
		elif not proposing and link is not None and link.pendingTransferFrom() == atc: # ATC cancelling transfer
			link.terminate(True)
		else:
			print('ERROR: %s proposing or aborting CPDLC transfer without data authority for %s.' % (atc, callsign), file=stderr)
	
	def receiveTransferResponse(self, callsign, atc, accept):
		link = env.cpdlc.liveDataLink(callsign)
		if link is not None and link.pendingTransferTo() == atc:
			if accept:
				link.terminate(True)
				if settings.CPDLC_send_strips_on_accepted_transfers:
					strip = env.strips.findUniqueForCallsign(callsign)
					if strip is not None:
						send_strip(strip, atc)
			else: # our proposal rejected by ATC
				link.setTransferTo(None)
				link.markProblem('Transfer rejected by %s' % atc) # do not QMessageBox here because dialogs will stack
		else:
			print('ERROR: %s responding to non proposed CPDLC transfer for %s.' % (atc, callsign), file=stderr)

