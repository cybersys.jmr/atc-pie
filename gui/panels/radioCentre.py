
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from datetime import timedelta, timezone

from PyQt5.QtWidgets import QWidget, QDialog
from PyQt5.QtCore import Qt, QDateTime
from ui.radiobox import Ui_radioBox
from ui.radios import Ui_radioCentrePane
from ui.recordAtisDialog import Ui_recordAtisDialog

from base.util import some
from base.utc import now
from base.radio import EMG_comm_freq
from base.text import replace_text_aliases

from session.config import settings
from session.env import env
from session.manager import SessionType

from gui.misc import signals, selection


# ---------- Constants ----------

soft_sound_level = .25
loud_sound_level = 1

# -------------------------------


class RadioBox(QWidget, Ui_radioBox):
	next_ID = 0 # STATIC unique identifier for constructed radio boxes
	
	def __init__(self, parent, radio):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.box_ID = RadioBox.next_ID
		RadioBox.next_ID += 1
		self.RDF_tickBox.setVisible(settings.session_manager.session_type == SessionType.FLIGHTGEAR)
		self.PTT_button.setEnabled(False)
		self.radio = radio
		self.frequency_combo.addFrequencies((frq, descr) for frq, descr, t in env.frequencies())
		self.frequency_combo.addFrequencies([(EMG_comm_freq, 'EMG')])
		self.updateRdfButton()
		self.tuneRadio()
		self.onOff_button.clicked.connect(self.switchOnOff) # clicked manually (has toggle arg too)
		self.PTT_button.pressed.connect(lambda: self.doPTT(True))
		self.PTT_button.released.connect(lambda: self.doPTT(False))
		self.softVolume_tickBox.toggled.connect(self.setRadioVolume)
		self.frequency_combo.frequencyChanged.connect(self.tuneRadio)
		self.RDF_tickBox.toggled.connect(self.updateRdfMonitor)
		signals.localSettingsChanged.connect(self.updateRdfButton)
	
	def isKbdPTT(self):
		return self.kbdPTT_checkBox.isChecked()
	
	def updateRdfButton(self):
		self.RDF_tickBox.setEnabled(settings.radio_direction_finding)
		if not settings.radio_direction_finding:
			self.RDF_tickBox.setChecked(False)
	
	def updateRdfMonitor(self):
		self.radio.setRdfMonitored(settings.radio_direction_finding and self.RDF_tickBox.isChecked())
	
	def switchOnOff(self, toggle):
		if not toggle:
			self.doPTT(False)
		self.radio.switchOnOff(toggle)
		on = self.radio.isOn()
		self.onOff_button.setChecked(on)
		self.PTT_button.setEnabled(on)
		self.removeBox_button.setEnabled(not on)
	
	def doPTT(self, toggle):
		if self.PTT_button.isEnabled():
			self.radio.setPTT(toggle)
			self.PTT_button.setChecked(toggle) # in case keyed with keyboard
		
	def tuneRadio(self):
		self.doPTT(False)
		self.radio.setFrequency(some(self.frequency_combo.getFrequency(), EMG_comm_freq)) # using EMG as fallback
	
	def setRadioVolume(self):
		if settings.radios_silenced:
			self.radio.setVolume(0)
		else:
			self.radio.setVolume(soft_sound_level if self.softVolume_tickBox.isChecked() else loud_sound_level)



class RecordAtisDialog(QDialog, Ui_recordAtisDialog):
	def __init__(self, parent):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.frequency_combo.addFrequencies([(f, d) for f, d, t in env.frequencies() if t == 'recorded'])
		self.status_infoLabel.clear()
		if settings.last_recorded_ATIS is None:
			info_letter = 'A'
		else:
			info_letter = chr((ord(settings.last_recorded_ATIS[0]) - ord('A') + 1) % 26 + ord('A'))
		custom_appendix = replace_text_aliases(settings.ATIS_custom_appendix, selection, False)
		self.notepad_textEdit.setPlainText(env.suggestedATIS(info_letter, custom_appendix))
		self.infoLetter_edit.setText(info_letter)
		self.nextAtisReminder_tickBox.setChecked(settings.record_ATIS_reminder is not None)
		if settings.record_ATIS_reminder is None:
			self.suggestNextAtisReminderTime()
		else:
			qdt = QDateTime(settings.record_ATIS_reminder.year, settings.record_ATIS_reminder.month, settings.record_ATIS_reminder.day,
							settings.record_ATIS_reminder.hour, settings.record_ATIS_reminder.minute, timeSpec=Qt.UTC)
			self.nextAtisReminder_edit.setDateTime(qdt)
		self.notepad_textEdit.textChanged.connect(self.status_infoLabel.clear)
		self.record_button.clicked.connect(self.record)
		self.nextAtisReminder_tickBox.toggled.connect(self.nextAtisReminder_edit.setFocus)
		self.close_button.clicked.connect(self.closeMe)
	
	def record(self):
		frq = self.frequency_combo.getFrequency()
		if frq is None:
			self.status_infoLabel.setText('Invalid frequency.')
		else:
			text_without_empty_lines = '\n'.join(line for line in self.notepad_textEdit.toPlainText().split('\n') if line != '')
			settings.last_recorded_ATIS = self.infoLetter_edit.text().upper(), frq, text_without_empty_lines
			settings.session_manager.recordAtis(self)
			self.status_infoLabel.setText('Information %s recorded' % settings.last_recorded_ATIS[0])
		self.suggestNextAtisReminderTime()
	
	def closeMe(self):
		if self.nextAtisReminder_tickBox.isChecked():
			settings.record_ATIS_reminder = self.nextAtisReminder_edit.dateTime().toPyDateTime().replace(tzinfo=timezone.utc)
		else:
			settings.record_ATIS_reminder = None
		self.accept()
	
	def suggestNextAtisReminderTime(self):
		t = now() + timedelta(minutes=35)
		self.nextAtisReminder_edit.setDateTime(QDateTime(t.year, t.month, t.day, t.hour, (0 if t.minute // 30 == 0 else 30), timeSpec=Qt.UTC))



class RadioCentrePane(QWidget, Ui_radioCentrePane):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.setEnabled(False)
		self.pubFreq_tickBox.setText(settings.location_code + ':')
		self.pubFreq_edit.addFrequencies([(f, d) for f, d, t in env.frequencies() if t != 'recorded'])
		# connections
		self.addBox_button.clicked.connect(self.addRadioBox)
		self.recordATIS_button.clicked.connect(signals.recordAtisDialogRequest.emit)
		self.muteAllRadios_tickBox.toggled.connect(self.toggleSilenceRadios)
		self.pubFreq_tickBox.toggled.connect(self.pubFreqToggled)
		self.pubFreq_edit.frequencyChanged.connect(self.setPubFreq)
		signals.kbdPTT.connect(self.generalKeyboardPTT)
		signals.sessionStarted.connect(self.sessionHasStarted)
		signals.sessionEnded.connect(self.sessionHasEnded)
	
	def sessionHasStarted(self):
		self.setEnabled(True)
		self.recordATIS_button.setEnabled(env.airport_data is not None)
		self.pubFreq_tickBox.setText(settings.my_callsign + ':')
	
	def sessionHasEnded(self):
		while self.radios_table.rowCount() > 0:
			self.removeRadioBox(tblRow=0)
		self.muteAllRadios_tickBox.setChecked(False)
		self.pubFreq_tickBox.setChecked(False)
		self.setEnabled(False)
	
	def pubFreqToggled(self, toggle):
		if toggle:
			self.pubFreq_tickBox.setText(settings.my_callsign + ':')
			self.pubFreq_edit.lineEdit().selectAll()
		else:
			settings.publicised_frequency = None
	
	def setPubFreq(self):
		settings.publicised_frequency = self.pubFreq_edit.getFrequency()
	
	def radioBox(self, row):
		return self.radios_table.cellWidget(row, 0)
		
	def addRadioBox(self):
		new_radio = settings.session_manager.createRadio()
		if new_radio is not None:
			settings.radios.append(new_radio)
			box = RadioBox(self, new_radio)
			row = self.radios_table.rowCount()
			box.kbdPTT_checkBox.setChecked(row == 0)
			box.removeBox_button.clicked.connect(lambda ignore_arg, rid=box.box_ID: self.removeRadioBox(boxID=rid))
			self.radios_table.insertRow(row)
			self.radios_table.setCellWidget(row, 0, box)
			self.radios_table.scrollToBottom()
			self.radios_table.resizeColumnToContents(0)
			self.radios_table.resizeRowToContents(row)
	
	def removeRadioBox(self, boxID=None, tblRow=None):
		if tblRow is None: # radio identified by ID on manual removal; get row from ID
			try:
				tblRow = next(row for row in range(self.radios_table.rowCount()) if self.radioBox(row).box_ID == boxID)
			except StopIteration:
				return # abort
		self.radioBox(tblRow).switchOnOff(False)
		self.radios_table.removeRow(tblRow)
		try:
			del settings.radios[tblRow]
		except IndexError:
			pass
	
	def generalKeyboardPTT(self, toggle):
		for i in range(self.radios_table.rowCount()):
			box = self.radioBox(i)
			if box.isKbdPTT():
				box.doPTT(toggle)
	
	def toggleSilenceRadios(self, toggle):
		settings.radios_silenced = toggle
		for i in range(self.radios_table.rowCount()):
			self.radioBox(i).setRadioVolume() # takes care of zero volume if silenced here
