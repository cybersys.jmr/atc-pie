
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from sys import stderr
from datetime import timedelta

from PyQt5.QtCore import Qt, QUrl, QTimer
from PyQt5.QtGui import QDesktopServices, QIcon
from PyQt5.QtWidgets import QMainWindow, QInputDialog, QFileDialog, QMessageBox, QMenu, QAction, QActionGroup, QLabel
from ui.mainWindow import Ui_mainWindow

from base.coords import EarthCoords
from base.fpl import FPL
from base.utc import now, timestr
from base.weather import hPa2inHg, mkWeather
from base.nav import world_routing_db
from base.strip import soft_link_detail

from session.config import settings
from session.env import env, default_tower_height
from session.manager import SessionManager, SessionType, student_callsign, teacher_callsign
from session.managers.flightGearMP import FlightGearSessionManager
from session.managers.fsdMP import FsdSessionManager
from session.managers.teacher import TeacherSessionManager
from session.managers.student import StudentSessionManager
from session.managers.solo import SoloSessionManager_AD, SoloSessionManager_CTR
from session.models.liveStrips import default_rack_name
from session.models.discardedStrips import ShelfFilterModel

from ext.resources import read_bg_img, read_route_presets, import_entry_exit_data, make_FGFS_models_liveries
from ext.sct import extract_sector
from ext.fgcom import test_FGCom_standalone, test_FGCom_Mumble
from ext.fgfs import FlightGearTowerViewer
from ext.sr import speech_recognition_available, prepare_SR_language_files, cleanup_SR_language_files

from gui.misc import IconFile, signals, selection
from gui.actions import new_strip_dialog, edit_strip, receive_strip, recover_strip, strip_auto_print_check
from gui.panels.radioCentre import RecordAtisDialog
from gui.panels.selectionInfo import SelectionInfoToolbarWidget
from gui.panels.teachingConsole import TeachingConsole
from gui.panels.unitConv import UnitConversionWindow
from gui.panels.workspace import CentralWorkspaceWidget
from gui.widgets.basicWidgets import flash_widget, Ticker, AlarmClockButton
from gui.widgets.miscWidgets import RadioKeyEventFilter, QuickReference, WorldAirportNavigator
from gui.dialog.settings import LocalSettingsDialog, GeneralSettingsDialog, SystemSettingsDialog, SoloSessionSettingsDialog
from gui.dialog.startSession import StartSoloDialog_AD, StartFgSessionDialog, \
		StartFsdDialog, StartStudentSessionDialog, StartTeacherSessionDialog
from gui.dialog.runways import RunwayUseDialog
from gui.dialog.miscDialogs import yesNo_question, hostPort_input, AboutDialog, DiscardedStripsDialog, EnvironmentInfoDialog


# ---------- Constants ----------

stylesheet_file = 'CONFIG/main-stylesheet.qss'
dock_layout_file = 'CONFIG/dock-layout.bin'

subsecond_tick_interval = 333 # ms
subminute_tick_interval = 20 * 1000 # ms
status_bar_message_timeout = 5000 # ms
session_start_temp_lock_duration = 5000 # ms
forward_time_skip = timedelta(seconds=10)

dock_flash_stylesheet = 'QDockWidget::title { background: yellow }'

OSM_zoom_level = 7
OSM_base_URL_fmt = 'http://www.openstreetmap.org/#map=%d/%f/%f'

airport_gateway_URL = 'http://gateway.x-plane.com/airports/page'
lenny64_newEvent_URL = 'http://flightgear-atc.alwaysdata.net/new_event.php'
video_tutorial_URL = 'http://www.youtube.com/playlist?list=PL1EQKKHhDVJvvWpcX_BqeOIsmeW2A_8Yb'
FAQ_URL = 'http://wiki.flightgear.org/ATC-pie_FAQ'

# -------------------------------


def mk_OSM_URL(coords):
	return OSM_base_URL_fmt % (OSM_zoom_level, coords.lat, coords.lon)


def setDockAndActionIcon(icon_file, action, dock):
	icon = QIcon(icon_file)
	action.setIcon(icon)
	dock.setWindowIcon(icon)
	
def raise_dock(dock):
	dock.show()
	dock.raise_()
	dock.widget().setFocus()
	flash_widget(dock, dock_flash_stylesheet)

	
def open_raise_window(window):
	window.show()
	window.raise_()



class MainWindow(QMainWindow, Ui_mainWindow):
	def __init__(self, launcher, parent=None):
		QMainWindow.__init__(self, parent)
		self.setupUi(self)
		self.central_workspace = CentralWorkspaceWidget(self)
		self.setCentralWidget(self.central_workspace)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setAttribute(Qt.WA_DeleteOnClose)
		self.launcher = launcher
		settings.controlled_tower_viewer = FlightGearTowerViewer(self)
		settings.session_manager = SessionManager(self, None) # Dummy manager
		self.updateWindowTitle()
		self.last_RDF_qdm = None
		self.session_start_temp_lock_timer = QTimer(self)
		self.session_start_temp_lock_timer.setSingleShot(True)
		self.session_start_temp_lock_timer.timeout.connect(self.releaseSessionStartTempLock)
		
		## Restore saved dock layout
		try:
			with open(dock_layout_file, 'rb') as f: # Restore saved dock arrangement
				self.restoreState(f.read())
		except FileNotFoundError: # Fall back on default dock arrangement
			# left docks, top zone: contact details (hidden), notepads (hidden), TWR ctrl (hidden), navigator, FPLs, weather (on top)
			self.tabifyDockWidget(self.selection_info_dock, self.notepads_dock)
			self.tabifyDockWidget(self.selection_info_dock, self.towerView_dock)
			self.tabifyDockWidget(self.selection_info_dock, self.navigator_dock)
			self.tabifyDockWidget(self.selection_info_dock, self.FPL_dock)
			self.tabifyDockWidget(self.selection_info_dock, self.weather_dock)
			self.selection_info_dock.hide()
			self.notepads_dock.hide()
			self.towerView_dock.hide()
			# left docks, bottom zone: instructions (hidden), radios (hidden), CPDLC (hidden), notifications (on top)
			self.tabifyDockWidget(self.instructions_dock, self.radio_dock)
			self.tabifyDockWidget(self.instructions_dock, self.CPDLC_dock)
			self.tabifyDockWidget(self.instructions_dock, self.notification_dock)
			self.instructions_dock.hide()
			self.radio_dock.hide()
			self.CPDLC_dock.hide()
			# right docks: RWY boxes (hidden), strips, ATC coord.
			self.rwyBoxes_dock.hide()
			# bottom docks (all hidden): radio chat, ATC chat
			self.radioTextChat_dock.hide()
			self.atcTextChat_dock.hide()
			# toolbars, in order: general, strip/FPL actions (hidden), contact info, radar assistance (hidden), workspace windows, docks (right-most)
			self.insertToolBar(self.docks_toolbar, self.general_toolbar) # = "insert before"
			self.insertToolBar(self.docks_toolbar, self.stripFplActions_toolbar)
			self.insertToolBar(self.docks_toolbar, self.selectionInfo_toolbar)
			self.insertToolBar(self.docks_toolbar, self.radarAssistance_toolbar)
			self.insertToolBar(self.docks_toolbar, self.workspace_toolbar)
			self.radarAssistance_toolbar.hide()
			self.stripFplActions_toolbar.hide()
		
		## Permanent tool/status bar widgets
		self.selectionInfo_toolbarWidget = SelectionInfoToolbarWidget(self)
		self.selectionInfo_toolbar.addWidget(self.selectionInfo_toolbarWidget)
		self.METAR_statusBarLabel = QLabel()
		self.PTT_statusBarLabel = QLabel()
		self.PTT_statusBarLabel.setToolTip('Keyboard PTT')
		self.RDF_statusBarLabel = QLabel()
		self.RDF_statusBarLabel.setToolTip('Current signal / last QDM')
		self.wind_statusBarLabel = QLabel()
		self.QNH_statusBarLabel = QLabel()
		self.QNH_statusBarLabel.setToolTip('hPa / inHg')
		self.clock_statusBarLabel = QLabel()
		self.alarmClock_statusBarButtons = [AlarmClockButton('1', self), AlarmClockButton('2', self)]
		self.statusbar.addWidget(self.METAR_statusBarLabel)
		self.statusbar.addPermanentWidget(self.PTT_statusBarLabel)
		self.statusbar.addPermanentWidget(self.RDF_statusBarLabel)
		self.statusbar.addPermanentWidget(self.wind_statusBarLabel)
		self.statusbar.addPermanentWidget(self.QNH_statusBarLabel)
		for b in self.alarmClock_statusBarButtons:
			self.statusbar.addPermanentWidget(b)
			b.alarm.connect(lambda lbl=b.label(): self.notification_pane.notifyAlarmClockTimedOut('Alarm clock %s timed out' % lbl))
		self.statusbar.addPermanentWidget(self.clock_statusBarLabel)
		
		# Populate menus (toolbar visibility and airport viewpoints)
		toolbar_menu = QMenu()
		self.general_viewToolbar_action = self.general_toolbar.toggleViewAction()
		self.stripActions_viewToolbar_action = self.stripFplActions_toolbar.toggleViewAction()
		self.docks_viewToolbar_action = self.docks_toolbar.toggleViewAction()
		self.selectionInfo_viewToolbar_action = self.selectionInfo_toolbar.toggleViewAction()
		self.radarAssistance_viewToolbar_action = self.radarAssistance_toolbar.toggleViewAction()
		self.workspace_viewToolbar_action = self.workspace_toolbar.toggleViewAction()
		toolbar_menu.addAction(self.general_viewToolbar_action)
		toolbar_menu.addAction(self.stripActions_viewToolbar_action)
		toolbar_menu.addAction(self.docks_viewToolbar_action)
		toolbar_menu.addAction(self.selectionInfo_viewToolbar_action)
		toolbar_menu.addAction(self.radarAssistance_viewToolbar_action)
		toolbar_menu.addAction(self.workspace_viewToolbar_action)
		self.toolbars_view_menuAction.setMenu(toolbar_menu)
		
		if env.airport_data is None or len(env.airport_data.viewpoints) == 0:
			self.viewpointSelection_view_menuAction.setEnabled(False)
		else:
			viewPointSelection_menu = QMenu()
			viewPointSelection_actionGroup = QActionGroup(self)
			for vp_i, (vp_pos, vp_h, vp_name) in enumerate(env.airport_data.viewpoints):
				action = QAction('%s - %d ft ASFC' % (vp_name, vp_h + .5), self)
				action.setCheckable(True)
				action.triggered.connect(lambda ignore_checked, i=vp_i: self.selectIndicateViewpoint(i))
				viewPointSelection_actionGroup.addAction(action)
			actions = viewPointSelection_actionGroup.actions()
			viewPointSelection_menu.addActions(actions)
			self.viewpointSelection_view_menuAction.setMenu(viewPointSelection_menu)
			try:
				actions[settings.selected_viewpoint].setChecked(True)
			except IndexError:
				actions[0].setChecked(True)
		
		## Memory-persistent windows and dialogs
		self.solo_connect_dialog_AD = StartSoloDialog_AD(self)
		self.FG_connect_dialog = StartFgSessionDialog(self)
		self.FSD_connect_dialog = StartFsdDialog(self)
		self.start_student_session_dialog = StartStudentSessionDialog(self)
		self.start_teacher_session_dialog = StartTeacherSessionDialog(self)
		self.recall_cheat_dialog = DiscardedStripsDialog(self, ShelfFilterModel(self, env.discarded_strips, False), 'Sent and deleted strips')
		self.shelf_dialog = DiscardedStripsDialog(self, ShelfFilterModel(self, env.discarded_strips, True), 'Strip shelf')
		self.environment_info_dialog = EnvironmentInfoDialog(self)
		self.about_dialog = AboutDialog(self)
		self.teaching_console = TeachingConsole(parent=self)
		self.unit_converter = UnitConversionWindow(parent=self)
		self.world_airport_navigator = WorldAirportNavigator(parent=self)
		self.quick_reference = QuickReference(parent=self)
		for w in self.teaching_console, self.unit_converter, self.world_airport_navigator, self.quick_reference:
			w.setWindowFlags(Qt.Window)
			w.installEventFilter(RadioKeyEventFilter(w))
		
		# Make a few actions always visible
		self.addAction(self.newStrip_action)
		self.addAction(self.newLinkedStrip_action)
		self.addAction(self.newFPL_action)
		self.addAction(self.newLinkedFPL_action)
		self.addAction(self.flagUnflagSelection_action)
		self.addAction(self.recordAtis_action)
		self.addAction(self.startTimer1_action)
		self.addAction(self.forceStartTimer1_action)
		self.addAction(self.startTimer2_action)
		self.addAction(self.forceStartTimer2_action)
		self.addAction(self.muteNotifications_options_action)
		self.addAction(self.quickReference_help_action)
		self.addAction(self.saveDockLayout_view_action)
		self.addAction(self.recallWindowState_view_action)
		
		# Populate icons
		self.newStrip_action.setIcon(QIcon(IconFile.action_newStrip))
		self.newLinkedStrip_action.setIcon(QIcon(IconFile.action_newLinkedStrip))
		self.newFPL_action.setIcon(QIcon(IconFile.action_newFPL))
		self.newLinkedFPL_action.setIcon(QIcon(IconFile.action_newLinkedFPL))
		self.teachingConsole_view_action.setIcon(QIcon(IconFile.panel_teaching))
		self.unitConversionTool_view_action.setIcon(QIcon(IconFile.panel_unitConv))
		self.worldAirportNavigator_view_action.setIcon(QIcon(IconFile.panel_airportList))
		self.environmentInfo_view_action.setIcon(QIcon(IconFile.panel_envInfo))
		self.generalSettings_options_action.setIcon(QIcon(IconFile.action_generalSettings))
		self.soloSessionSettings_system_action.setIcon(QIcon(IconFile.action_sessionSettings))
		self.runwaysInUse_options_action.setIcon(QIcon(IconFile.action_runwayUse))
		self.newLooseStripBay_view_action.setIcon(QIcon(IconFile.action_newLooseStripBay))
		self.newRadarScreen_view_action.setIcon(QIcon(IconFile.action_newRadarScreen))
		self.newStripRackPanel_view_action.setIcon(QIcon(IconFile.action_newRackPanel))
		self.popOutCurrentWindow_view_action.setIcon(QIcon(IconFile.action_popOutWindow))
		self.reclaimPoppedOutWindows_view_action.setIcon(QIcon(IconFile.action_reclaimWindows))
		self.primaryRadar_options_action.setIcon(QIcon(IconFile.option_primaryRadar))
		self.approachSpacingHints_options_action.setIcon(QIcon(IconFile.option_approachSpacingHints))
		self.runwayOccupationWarnings_options_action.setIcon(QIcon(IconFile.option_runwayOccupationMonitor))
		self.routeConflictWarnings_options_action.setIcon(QIcon(IconFile.option_routeConflictWarnings))
		self.trafficIdentification_options_action.setIcon(QIcon(IconFile.option_identificationAssistant))
		
		setDockAndActionIcon(IconFile.panel_ATCs, self.atcCoordination_dockView_action, self.atcCoordination_dock)
		setDockAndActionIcon(IconFile.panel_atcChat, self.atcTextChat_dockView_action, self.atcTextChat_dock)
		setDockAndActionIcon(IconFile.panel_CPDLC, self.cpdlc_dockView_action, self.CPDLC_dock)
		setDockAndActionIcon(IconFile.panel_FPLs, self.FPLs_dockView_action, self.FPL_dock)
		setDockAndActionIcon(IconFile.panel_instructions, self.instructions_dockView_action, self.instructions_dock)
		setDockAndActionIcon(IconFile.panel_navigator, self.navpoints_dockView_action, self.navigator_dock)
		setDockAndActionIcon(IconFile.panel_notepads, self.notepads_dockView_action, self.notepads_dock)
		setDockAndActionIcon(IconFile.panel_notifications, self.notificationArea_dockView_action, self.notification_dock)
		setDockAndActionIcon(IconFile.panel_radios, self.fgcom_dockView_action, self.radio_dock)
		setDockAndActionIcon(IconFile.panel_runwayBox, self.runwayBoxes_dockView_action, self.rwyBoxes_dock)
		setDockAndActionIcon(IconFile.panel_selInfo, self.radarContactDetails_dockView_action, self.selection_info_dock)
		setDockAndActionIcon(IconFile.panel_racks, self.strips_dockView_action, self.strip_dock)
		setDockAndActionIcon(IconFile.panel_txtChat, self.radioTextChat_dockView_action, self.radioTextChat_dock)
		setDockAndActionIcon(IconFile.panel_twrView, self.towerView_dockView_action, self.towerView_dock)
		setDockAndActionIcon(IconFile.panel_weather, self.weather_dockView_action, self.weather_dock)
		
		# action TICKED STATES (set here before connections)
		self.windowedWorkspace_view_action.setChecked(settings.saved_workspace_windowed_view)
		self.muteNotifications_options_action.setChecked(settings.mute_notifications)
		self.primaryRadar_options_action.setChecked(settings.primary_radar_active)
		self.routeConflictWarnings_options_action.setChecked(settings.route_conflict_warnings)
		self.trafficIdentification_options_action.setChecked(settings.traffic_identification_assistant)
		self.runwayOccupationWarnings_options_action.setChecked(settings.monitor_runway_occupation)
		self.approachSpacingHints_options_action.setChecked(settings.APP_spacing_hints)
		
		# action CONNECTIONS
		# non-menu actions
		self.newStrip_action.triggered.connect(lambda: new_strip_dialog(self, default_rack_name, linkToSelection=False))
		self.newLinkedStrip_action.triggered.connect(lambda: new_strip_dialog(self, default_rack_name, linkToSelection=True))
		self.newFPL_action.triggered.connect(lambda: self.FPL_panel.createLocalFPL(link=None))
		self.newLinkedFPL_action.triggered.connect(lambda: self.FPL_panel.createLocalFPL(link=selection.strip))
		self.flagUnflagSelection_action.triggered.connect(self.flagUnflagSelection)
		self.recordAtis_action.triggered.connect(self.recordATIS)
		self.startTimer1_action.triggered.connect(lambda: self.startGuiTimer(0, False))
		self.forceStartTimer1_action.triggered.connect(lambda: self.startGuiTimer(0, True))
		self.startTimer2_action.triggered.connect(lambda: self.startGuiTimer(1, False))
		self.forceStartTimer2_action.triggered.connect(lambda: self.startGuiTimer(1, True))
		# system menu
		self.soloSession_system_action.triggered.connect(lambda: self.startStopSession(self.start_solo))
		self.flightGearSession_system_action.triggered.connect(lambda: self.startStopSession(self.start_FlightGearSession))
		self.fsdConnection_system_action.triggered.connect(lambda: self.startStopSession(self.start_FSD))
		self.teacherSession_system_action.triggered.connect(lambda: self.startStopSession(self.start_teaching))
		self.studentSession_system_action.triggered.connect(lambda: self.startStopSession(self.start_learning))
		self.reloadBgImages_system_action.triggered.connect(self.reloadBackgroundImages)
		self.reloadAdditionalViewers_system_action.triggered.connect(self.reloadAdditionalViewers)
		self.reloadFgAcftModels_system_action.triggered.connect(self.reloadFgAcftModels)
		self.reloadRoutePresetsAndEntryExitPoints_system_action.triggered.connect(self.reloadRoutePresetsAndEntryExitPoints)
		self.reloadStylesheetAndColours_system_action.triggered.connect(self.reloadStylesheetAndColours)
		self.airportGateway_system_action.triggered.connect(lambda: self.goToURL(airport_gateway_URL))
		self.openStreetMap_system_action.triggered.connect(lambda: self.goToURL(mk_OSM_URL(env.radarPos())))
		self.announceFgSession_system_action.triggered.connect(lambda: self.goToURL(lenny64_newEvent_URL))
		self.measuringLogsCoordinates_system_action.toggled.connect(self.switchMeasuringCoordsLog)
		self.extractSectorFile_system_action.triggered.connect(self.extractSectorFile)
		self.repositionBgImages_system_action.triggered.connect(self.repositionRadarBgImages)
		self.phoneSquelchEdit_system_action.toggled.connect(self.atcCoordination_pane.phoneSquelch_widget.setVisible)
		self.testFgcomConfiguration_system_action.triggered.connect(self.testFgcomConfiguration)
		self.soloSessionSettings_system_action.triggered.connect(self.openSoloSessionSettings)
		self.locationSettings_system_action.triggered.connect(self.openLocalSettings)
		self.systemSettings_system_action.triggered.connect(self.openSystemSettings)
		self.changeLocation_system_action.triggered.connect(self.changeLocation)
		self.quit_system_action.triggered.connect(self.close)
		# view menu
		self.saveDockLayout_view_action.triggered.connect(self.saveDockLayout)
		self.recallWindowState_view_action.triggered.connect(self.recallWindowState)
		self.atcCoordination_dockView_action.triggered.connect(lambda: raise_dock(self.atcCoordination_dock))
		self.atcTextChat_dockView_action.triggered.connect(lambda: raise_dock(self.atcTextChat_dock))
		self.cpdlc_dockView_action.triggered.connect(lambda: raise_dock(self.CPDLC_dock))
		self.FPLs_dockView_action.triggered.connect(lambda: raise_dock(self.FPL_dock))
		self.instructions_dockView_action.triggered.connect(lambda: raise_dock(self.instructions_dock))
		self.navpoints_dockView_action.triggered.connect(lambda: raise_dock(self.navigator_dock))
		self.notepads_dockView_action.triggered.connect(lambda: raise_dock(self.notepads_dock))
		self.notificationArea_dockView_action.triggered.connect(lambda: raise_dock(self.notification_dock))
		self.fgcom_dockView_action.triggered.connect(lambda: raise_dock(self.radio_dock))
		self.runwayBoxes_dockView_action.triggered.connect(lambda: raise_dock(self.rwyBoxes_dock))
		self.radarContactDetails_dockView_action.triggered.connect(lambda: raise_dock(self.selection_info_dock))
		self.strips_dockView_action.triggered.connect(lambda: raise_dock(self.strip_dock))
		self.radioTextChat_dockView_action.triggered.connect(lambda: raise_dock(self.radioTextChat_dock))
		self.towerView_dockView_action.triggered.connect(lambda: raise_dock(self.towerView_dock))
		self.weather_dockView_action.triggered.connect(lambda: raise_dock(self.weather_dock))
		self.windowedWorkspace_view_action.toggled.connect(self.central_workspace.switchWindowedView)
		self.popOutCurrentWindow_view_action.triggered.connect(self.central_workspace.popOutCurrentWindow)
		self.reclaimPoppedOutWindows_view_action.triggered.connect(self.central_workspace.reclaimPoppedOutWidgets)
		self.newLooseStripBay_view_action.triggered.connect(lambda: self.central_workspace.addWorkspaceWidget(CentralWorkspaceWidget.LOOSE_BAY))
		self.newRadarScreen_view_action.triggered.connect(lambda: self.central_workspace.addWorkspaceWidget(CentralWorkspaceWidget.RADAR_SCREEN))
		self.newStripRackPanel_view_action.triggered.connect(lambda: self.central_workspace.addWorkspaceWidget(CentralWorkspaceWidget.STRIP_PANEL))
		self.towerView_view_action.triggered.connect(self.toggleTowerWindow)
		self.addViewer_view_action.triggered.connect(self.addView)
		self.listViewers_view_action.triggered.connect(self.listAdditionalViews)
		self.activateAdditionalViewers_view_action.toggled.connect(self.activateAdditionalViews)
		self.removeViewer_view_action.triggered.connect(self.removeView)
		self.environmentInfo_view_action.triggered.connect(self.environment_info_dialog.exec)
		self.lastRecordedAtis_view_menuAction.triggered.connect(self.showLastRecordedAtis)
		self.teachingConsole_view_action.triggered.connect(lambda: open_raise_window(self.teaching_console))
		self.unitConversionTool_view_action.triggered.connect(lambda: open_raise_window(self.unit_converter))
		self.worldAirportNavigator_view_action.triggered.connect(lambda: open_raise_window(self.world_airport_navigator))
		self.latestCpdlcDialogue_action.triggered.connect(self.showLastCpdlcDialogueForSelection)
		self.closeCpdlcWindows_view_action.triggered.connect(self.cpdlc_pane.closeChildWindows)
		# options menu
		self.runwaysInUse_options_action.triggered.connect(self.configureRunwayUse)
		self.muteNotifications_options_action.toggled.connect(self.muteNotificationSounds)
		self.primaryRadar_options_action.toggled.connect(self.switchPrimaryRadar)
		self.routeConflictWarnings_options_action.toggled.connect(self.switchConflictWarnings)
		self.trafficIdentification_options_action.toggled.connect(self.switchTrafficIdentification)
		self.runwayOccupationWarnings_options_action.toggled.connect(self.switchRwyOccupationIndications)
		self.approachSpacingHints_options_action.toggled.connect(self.switchApproachSpacingHints)
		self.generalSettings_options_action.triggered.connect(self.openGeneralSettings)
		# cheat menu
		self.pauseSimulation_cheat_action.toggled.connect(self.pauseSession)
		self.skipTimeForward_cheat_action.triggered.connect(self.skipTimeForwardOnce)
		self.spawnAircraft_cheat_action.triggered.connect(self.spawnAircraft)
		self.killSelectedAircraft_cheat_action.triggered.connect(self.killSelectedAircraft)
		self.popUpMsgOnRejectedInstr_cheat_action.toggled.connect(self.setRejectedInstrPopUp)
		self.showRecognisedVoiceStrings_cheat_action.toggled.connect(self.setShowRecognisedVoiceStrings)
		self.ensureClearWeather_cheat_action.toggled.connect(self.ensureClearWeather)
		self.ensureDayLight_cheat_action.triggered.connect(self.towerView_pane.ensureDayLight)
		self.changeTowerHeight_cheat_action.triggered.connect(self.changeTowerHeight)
		self.radarCheatMode_cheat_action.toggled.connect(self.setRadarCheatMode)
		self.showAcftCheatToggles_cheat_action.toggled.connect(self.showAcftCheatToggles)
		self.recallDiscardedStrip_cheat_action.triggered.connect(self.recall_cheat_dialog.exec)
		# help menu
		self.quickReference_help_action.triggered.connect(lambda: open_raise_window(self.quick_reference))
		self.videoTutorial_help_action.triggered.connect(lambda: self.goToURL(video_tutorial_URL))
		self.FAQ_help_action.triggered.connect(lambda: self.goToURL(FAQ_URL))
		self.about_help_action.triggered.connect(self.about_dialog.exec)
		
		## More signal connections
		signals.openShelfRequest.connect(self.shelf_dialog.exec)
		signals.privateAtcChatRequest.connect(lambda: raise_dock(self.atcTextChat_dock))
		signals.weatherDockRaiseRequest.connect(lambda: raise_dock(self.weather_dock))
		signals.recordAtisDialogRequest.connect(self.recordATIS)
		signals.stripRecall.connect(recover_strip)
		env.radar.blip.connect(env.strips.refreshViews)
		env.radar.lostContact.connect(self.aircraftHasDisappeared)
		signals.aircraftKilled.connect(self.aircraftHasDisappeared)
		env.strips.rwyBoxFreed.connect(lambda box, strip: env.airport_data.resetRwySepTimer(box, strip.lookup(FPL.WTC)))
		signals.statusBarMsg.connect(lambda msg: self.statusbar.showMessage(msg, status_bar_message_timeout))
		signals.newWeather.connect(self.updateWeatherIfPrimary)
		signals.kbdPTT.connect(self.setKbdPttState)
		signals.sessionStarted.connect(self.sessionHasStarted)
		signals.sessionEnded.connect(self.sessionHasEnded)
		signals.towerViewProcessToggled.connect(self.towerView_view_action.setChecked)
		signals.towerViewProcessToggled.connect(self.towerView_cheat_menu.setEnabled)
		signals.stripInfoChanged.connect(env.strips.refreshViews)
		signals.fastClockTick.connect(self.updateClock)
		signals.fastClockTick.connect(env.cpdlc.checkForTimeOuts)
		signals.fastClockTick.connect(self.updateRdfSignal)
		signals.slowClockTick.connect(self.checkForRealTimeClockTriggers)
		signals.stripEditRequest.connect(lambda strip: edit_strip(self, strip))
		signals.selectionChanged.connect(self.updateStripFplActions)
		signals.receiveStrip.connect(receive_strip)
		signals.handoverFailure.connect(self.recoverFailedHandover)
		signals.sessionPaused.connect(self.sessionHasPaused)
		signals.sessionResumed.connect(self.sessionHasResumed)
		signals.realTimeSkipped.connect(self.realTimeHasSkipped)
		signals.aircraftKilled.connect(env.radar.silentlyForgetContact)
		signals.rackVisibilityLost.connect(self.collectClosedRacks)
		signals.localSettingsChanged.connect(self.localSettingsChanged)
		
		## MISC GUI setup
		self.strip_pane.setViewRacks([default_rack_name]) # will be moved out if a rack panel's saved "visible_racks" claims it [*1]
		self.strip_pane.restoreState(settings.saved_strip_dock_state) # [*1]
		self.rwyBox_pane.setVerticalLayout(settings.vertical_runway_box_layout)
		self.central_workspace.restoreWorkspaceWindows(settings.saved_workspace_windows)
		self.central_workspace.switchWindowedView(settings.saved_workspace_windowed_view) # keep this after restoring windows!
		self.towerView_cheat_menu.setEnabled(False)
		self.solo_cheat_menu.setEnabled(False)
		self.updateClock()
		self.updateWeatherIfPrimary(settings.primary_METAR_station, None)
		self.updateStripFplActions()
		self.RDF_statusBarLabel.setVisible(settings.radio_direction_finding)
		self.PTT_statusBarLabel.setVisible(False)
		
		self.subsecond_ticker = Ticker(self, signals.fastClockTick.emit)
		self.subminute_ticker = Ticker(self, signals.slowClockTick.emit)
		self.subsecond_ticker.startTicking(subsecond_tick_interval)
		self.subminute_ticker.startTicking(subminute_tick_interval)
		# Disable some base airport stuff if doing CTR
		if env.airport_data is None:
			self.towerView_view_action.setEnabled(False)
			self.runwaysInUse_options_action.setEnabled(False)
			self.runwayOccupationWarnings_options_action.setEnabled(False)
		# Finish
		if speech_recognition_available:
			prepare_SR_language_files()
		self.applyStylesheet() # do this last to force radar tag boxes to resize appropriately on emitted signal
	
	def flagUnflagSelection(self):
		acft = selection.acft
		if acft is not None:
			acft.flagged = not acft.flagged
			signals.selectionChanged.emit()

	def recordATIS(self):
		if settings.session_manager.isRunning() and env.airport_data is not None:
			RecordAtisDialog(self).exec()
	
	def startGuiTimer(self, i, force_start):
		if force_start or not self.alarmClock_statusBarButtons[i].timerIsRunning():
			self.alarmClock_statusBarButtons[i].setTimer()
	
	def goToURL(self, url):
		if not QDesktopServices.openUrl(QUrl(url)):
			QMessageBox.critical(self, 'Error opening web browser',
					'Could not invoke desktop web browser.\nGo to: %s' % url)
	
	def applyStylesheet(self):
		try:
			self.setStyleSheet(open(stylesheet_file).read()) # NOTE apparently, we cannot catch syntax errors in stylesheet
			signals.mainStylesheetApplied.emit()
		except FileNotFoundError:
			pass

	def recoverFailedHandover(self, strip, msg):
		recover_strip(strip)
		QMessageBox.critical(self, 'Handover failed', msg)
	
	def localSettingsChanged(self):
		env.rdf.clearAllSignals()
		self.RDF_statusBarLabel.setVisible(settings.radio_direction_finding)
		settings.session_manager.weatherLookUpRequest(settings.primary_METAR_station)

	
	# ---------------------     GUI auto-update functions     ---------------------- #
	
	def updateWindowTitle(self):
		if settings.session_manager.isRunning():
			running_prefix = 'Solo' if settings.session_manager.session_type == SessionType.SOLO else settings.my_callsign
			running_prefix += ' @ '
		else:
			running_prefix = ''
		self.setWindowTitle('ATC-pie - %s%s (%s)' % (running_prefix, env.locationName(), settings.location_code))
	
	def updateClock(self):
		self.clock_statusBarLabel.setText('UTC ' + timestr(seconds=True))
	
	def checkForRealTimeClockTriggers(self):
		if settings.session_manager.isRunning():
			strip_auto_print_check()
			if settings.record_ATIS_reminder is not None and now() > settings.record_ATIS_reminder:
				settings.record_ATIS_reminder = None
				self.notification_pane.notifyAlarmClockTimedOut('Reminder: record ATIS')
	
	def releaseSessionStartTempLock(self):
		settings.session_start_temp_lock = False
	
	def updateWeatherIfPrimary(self, station, weather): # NOTE weather may be None here
		if station == settings.primary_METAR_station:
			# Update status bar info
			self.METAR_statusBarLabel.setText(None if weather is None else weather.METAR())
			self.wind_statusBarLabel.setText('Wind ' + ('---' if weather is None else weather.readWind()))
			qnh = None if weather is None else weather.QNH() # NOTE qnh may still be None
			self.QNH_statusBarLabel.setText('QNH ' + ('---' if qnh is None else ('%d / %.2f' % (qnh, hPa2inHg * qnh))))
			# Update tower view
			if weather is not None and not settings.TWR_view_clear_weather_cheat:
				settings.controlled_tower_viewer.setWeather(weather)

	def setKbdPttState(self, toggle):
		settings.keyboard_PTT_pressed = toggle
		self.updatePTT()

	def updatePTT(self):
		self.PTT_statusBarLabel.setText('PTT' if settings.keyboard_PTT_pressed else ' - - - ')
	
	def updateRdfSignal(self):
		if settings.radio_direction_finding:
			sig = env.rdf.latestLiveSignal()
			if sig is None:
				s1 = ' - - - '
			else:
				s1 = sig.direction.read()
				self.last_RDF_qdm = sig.direction.opposite()
			s2 = ' - - - ' if self.last_RDF_qdm is None else self.last_RDF_qdm.read()
			self.RDF_statusBarLabel.setText('RDF %s / %s' % (s1, s2))
	
	def updateSessionStartStopActions(self):
		running = settings.session_manager.isRunning()
		for gt, ma in {
					SessionType.SOLO: self.soloSession_system_action,
					SessionType.FLIGHTGEAR: self.flightGearSession_system_action,
					SessionType.FSD: self.fsdConnection_system_action,
					SessionType.STUDENT: self.studentSession_system_action,
					SessionType.TEACHER: self.teacherSession_system_action
				}.items():
			if gt == settings.session_manager.session_type:
				ma.setEnabled(True)
				ma.setChecked(running)
			else:
				ma.setEnabled(not running)
				ma.setChecked(False)
	
	def updateStripFplActions(self):
		self.newLinkedStrip_action.setEnabled(selection.strip is None and not selection.acft == selection.fpl is None)
		self.newLinkedFPL_action.setEnabled(selection.strip is not None and selection.strip.linkedFPL() is None)
	
	def sessionHasStarted(self):
		self.updateWindowTitle()
		self.session_start_temp_lock_timer.start(session_start_temp_lock_duration)
		self.updateSessionStartStopActions()
		self.solo_cheat_menu.setEnabled(settings.session_manager.session_type == SessionType.SOLO)
		if settings.session_manager.session_type == SessionType.STUDENT:
			for toggle_action in self.radarCheatMode_cheat_action, self.showAcftCheatToggles_cheat_action:
				toggle_action.setChecked(False)
				toggle_action.setEnabled(False)
		self.updatePTT()
		self.PTT_statusBarLabel.setVisible(True)
		env.radar.startSweeping()
		
	def sessionHasEnded(self):
		env.radar.stopSweeping()
		env.radar.resetContacts()
		env.strips.removeAllStrips()
		env.FPLs.clearFPLs()
		env.rdf.clearAllSignals()
		env.cpdlc.clearHistory()
		env.weather_information.clear()
		self.updateWindowTitle()
		self.updateSessionStartStopActions()
		self.pauseSimulation_cheat_action.setChecked(False)
		self.solo_cheat_menu.setEnabled(False)
		for toggle_action in self.radarCheatMode_cheat_action, self.showAcftCheatToggles_cheat_action:
			toggle_action.setEnabled(True)
		self.PTT_statusBarLabel.setVisible(False)
		self.last_RDF_qdm = None
		selection.deselect()
		settings.simulation_paused_at = None
		settings.record_ATIS_reminder = None
		print('Session ended.')
	
	def sessionHasPaused(self):
		env.radar.stopSweeping()
		settings.simulation_paused_at = now()
		for b in self.alarmClock_statusBarButtons:
			b.suspendResume()
	
	def sessionHasResumed(self):
		if settings.simulation_paused_at is not None:
			pause_delay = now() - settings.simulation_paused_at
			for acft in settings.session_manager.getAircraft():
				acft.moveHistoryTimesForward(pause_delay)
			if env.airport_data is not None:
				env.airport_data.delayRwySepTimers(pause_delay)
			for b in self.alarmClock_statusBarButtons:
				b.suspendResume()
			settings.simulation_paused_at = None
			env.radar.startSweeping()

	def realTimeHasSkipped(self, time_skipped):
		if env.airport_data is not None:
			env.airport_data.delayRwySepTimers(-time_skipped)
		for b in self.alarmClock_statusBarButtons:
			if b.timerIsRunning():
				b.skipTime(time_skipped)
		env.radar.scan()
	
	def aircraftHasDisappeared(self, acft):
		strip = env.linkedStrip(acft)
		if strip is not None:
			strip.linkAircraft(None)
		if selection.acft is acft:
			if strip is None: # was not linked
				selection.deselect()
			else:
				selection.selectStrip(strip)
	
	def collectClosedRacks(self, racks):
		self.strip_pane.setViewRacks(self.strip_pane.getViewRacks() + racks)
	

	
	
	# ---------------------     Session start functions     ---------------------- #
	
	def start_solo(self):
		if env.airport_data is None: # start CTR solo simulation
			n, ok = QInputDialog.getInt(self, 'Solo CTR session', 'Initial traffic count:', value=2, min=0, max=99)
			if ok:
				settings.my_callsign = settings.location_code
				settings.session_manager = SoloSessionManager_CTR(self)
				settings.session_manager.start(n)
		else: # start AD solo simulation
			self.solo_connect_dialog_AD.exec()
			if self.solo_connect_dialog_AD.result() > 0: # not rejected
				settings.my_callsign = settings.location_code
				settings.session_manager = SoloSessionManager_AD(self)
				settings.session_manager.start(self.solo_connect_dialog_AD.chosenInitialTrafficCount())
	
	def start_FlightGearSession(self):
		self.FG_connect_dialog.exec()
		if self.FG_connect_dialog.result() > 0: # not rejected
			settings.my_callsign = self.FG_connect_dialog.chosenCallsign()
			settings.session_manager = FlightGearSessionManager(self)
			settings.session_manager.start()
	
	def start_FSD(self):
		self.FSD_connect_dialog.exec()
		if self.FSD_connect_dialog.result() > 0: # not rejected
			settings.my_callsign = self.FSD_connect_dialog.chosenCallsign()
			settings.session_manager = FsdSessionManager(self)
			settings.session_manager.start()
	
	def start_teaching(self):
		self.start_teacher_session_dialog.exec()
		if self.start_teacher_session_dialog.result() > 0: # not rejected
			settings.my_callsign = teacher_callsign
			settings.session_manager = TeacherSessionManager(self)
			settings.session_manager.start()
	
	def start_learning(self):
		self.start_student_session_dialog.exec()
		if self.start_student_session_dialog.result() > 0: # not rejected
			settings.my_callsign = student_callsign
			settings.session_manager = StudentSessionManager(self)
			settings.session_manager.start()

	
	
	# ---------------------     GUI menu actions     ---------------------- #
	
	## SYSTEM MENU ##
	
	def startStopSession(self, start_func):
		if settings.session_manager.isRunning(): # Stop session
			selection.deselect()
			settings.session_manager.stop()
		else: # Start session
			settings.session_start_temp_lock = True
			start_func()
		self.updateSessionStartStopActions()
		if not settings.session_manager.isRunning():
			settings.session_start_temp_lock = False

	def reloadBackgroundImages(self):
		print('Reload: background images')
		settings.radar_background_images, settings.loose_strip_bay_backgrounds = read_bg_img(settings.location_code, env.navpoints)
		signals.backgroundImagesReloaded.emit()
		QMessageBox.information(self, 'Done reloading', 'Background images reloaded. Check for console error messages.')
	
	def reloadAdditionalViewers(self):
		print('Reload: additional viewers')
		settings.loadAdditionalViews()
		QMessageBox.information(self, 'Done reloading', 'Additional viewers reloaded. Check for console error messages.')

	def reloadFgAcftModels(self):
		print('Reload: FG aircraft models')
		make_FGFS_models_liveries()
		QMessageBox.information(self, 'Done reloading', 'FG aircraft models reloaded. Check for console error messages.')
	
	def reloadStylesheetAndColours(self):
		print('Reload: stylesheet and colours')
		self.applyStylesheet() # emits its own signal
		settings.loadColourSettings()
		signals.colourConfigReloaded.emit()
		QMessageBox.information(self, 'Done reloading', 'Stylesheet and colour configuration reloaded. Check for console error messages.')
	
	def reloadRoutePresetsAndEntryExitPoints(self):
		print('Reload: route presets and AD entry/exit points')
		settings.route_presets = read_route_presets(stderr)
		world_routing_db.clearEntryExitPoints()
		import_entry_exit_data(stderr)
		QMessageBox.information(self, 'Done reloading', 'Route presets and entry/exit points reloaded. Check for console error messages.')
	
	def switchMeasuringCoordsLog(self, toggle):
		settings.measuring_tool_logs_coordinates = toggle
	
	def extractSectorFile(self):
		txt, ignore = QFileDialog.getOpenFileName(self, caption='Select sector file to extract from')
		if txt != '':
			extract_sector(txt, env.radarPos(), settings.map_range)
			QMessageBox.information(self, 'Done extracting',
				'Background drawings extracted.\nSee console for summary and files created in the "OUTPUT" directory.')
	
	def repositionRadarBgImages(self):
		radar_panel = self.central_workspace.getCurrentRadarPanel()
		if radar_panel is None:
			QMessageBox.critical(self, 'Image positioning error', 'This requires an active radar panel in the central workspace.')
		else:
			radar_panel.positionVisibleBgImages()

	def testFgcomConfiguration(self):
		if settings.FGCom_use_Mumble_plugin:
			test_FGCom_Mumble(self)
		else:
			test_FGCom_standalone(self)
	
	def openSoloSessionSettings(self):
		SoloSessionSettingsDialog(self).exec()
	
	def openLocalSettings(self):
		dialog = LocalSettingsDialog(self)
		dialog.exec()
		if dialog.result() > 0 and settings.session_manager.isRunning():
			env.radar.startSweeping()
	
	def openSystemSettings(self):
		SystemSettingsDialog(self).exec()
	
	def changeLocation(self):
		if yesNo_question(self, 'Change location', 'This will close the current session.', 'Are you sure?'):
			self.launcher.show()
			self.close()
	
	
	## VIEW MENU ##
	
	def saveDockLayout(self): # STYLE catch file write error
		with open(dock_layout_file, 'wb') as f:
			f.write(self.saveState())
		QMessageBox.information(self, 'Save dock layout', 'Current dock layout saved.')
	
	def recallWindowState(self):
		try:
			with open(dock_layout_file, 'rb') as f:
				self.restoreState(f.read())
		except FileNotFoundError:
			QMessageBox.critical(self, 'Recall dock layout', 'No saved layout to recall.')
	
	def toggleTowerWindow(self):
		if self.towerView_view_action.isChecked():
			if env.airport_data is not None and len(env.airport_data.viewpoints) == 0:
				QMessageBox.warning(self, 'No viewpoint',
						'Airport data does not specify a viewpoint. ATC-pie will position one near a runway.\n'
						'Update your source file if one should be available.')
			settings.controlled_tower_viewer.start()
		else:
			settings.controlled_tower_viewer.stop()
	
	def selectIndicateViewpoint(self, vp_index):
		if vp_index != settings.selected_viewpoint:
			settings.selected_viewpoint = vp_index
			settings.tower_height_cheat_offset = 0
			if settings.controlled_tower_viewer.running:
				self.towerView_pane.updateTowerPosition()
		signals.indicatePoint.emit(env.viewpoint()[0])
	
	def activateAdditionalViews(self, toggle):
		settings.additional_views_active = toggle
	
	def listAdditionalViews(self):
		if len(settings.additional_views) == 0:
			txt = 'No additional viewers registered.'
		else:
			lst = [' - %s on port %d' % host_port for host_port in settings.additional_views]
			txt = 'Additional viewers:\n%s' % '\n'.join(lst)
		QMessageBox.information(self, 'Additional viewers', txt)
	
	def addView(self):
		host, port = hostPort_input(self, 'Add an external viewer', 'Enter "host:port" for viewer to send traffic to:')
		if host is not None: # not cancelled
			settings.additional_views.append((host, port))
			QMessageBox.information(self, 'Add an external viewer', 'Viewer added: %s on port %d.' % (host, port))
	
	def removeView(self):
		if len(settings.additional_views) == 0:
			QMessageBox.critical(self, 'Remove viewer', 'No additional viewers registered.')
		else:
			items = ['%d: %s on port %d' % (i, settings.additional_views[i][0], settings.additional_views[i][1])
								for i in range(len(settings.additional_views))]
			item, ok = QInputDialog.getItem(self, 'Remove viewer', 'Select viewer to remove:', items, editable=False)
			if ok:
				del settings.additional_views[int(item.split(':', maxsplit=1)[0])]
	
	def showLastRecordedAtis(self):
		if settings.last_recorded_ATIS is None:
			QMessageBox.warning(self, 'Last recorded ATIS', 'No information recorded')
		else:
			QMessageBox.information(self, 'Last recorded ATIS', 'Notepad recorded as information %s on %s\n\n%s' % settings.last_recorded_ATIS)

	def showLastCpdlcDialogueForSelection(self):
		cs = selection.selectedCallsign()
		if cs is None:
			QMessageBox.critical(self, 'Last CPDLC dialogue', 'No callsign in selection.')
		else:
			signals.cpdlcWindowRequest.emit(cs, False)
	
	
	## OPTIONS MENU ##
	
	def configureRunwayUse(self):
		RunwayUseDialog(self).exec()
	
	def muteNotificationSounds(self, toggle):
		settings.mute_notifications = toggle
	
	def switchPrimaryRadar(self, toggle):
		settings.primary_radar_active = toggle
		env.radar.scan()
	
	def switchConflictWarnings(self, toggle):
		settings.route_conflict_warnings = toggle
	
	def switchTrafficIdentification(self, toggle):
		settings.traffic_identification_assistant = toggle
		if not toggle:
			for strip in env.strips.listAll():
				strip.writeDetail(soft_link_detail, None)
			signals.stripInfoChanged.emit()
	
	def switchRwyOccupationIndications(self, toggle):
		settings.monitor_runway_occupation = toggle
	
	def switchApproachSpacingHints(self, toggle):
		settings.APP_spacing_hints = toggle
		signals.stripInfoChanged.emit()
	
	def openGeneralSettings(self):
		GeneralSettingsDialog(self).exec()
	
	
	## CHEAT MENU ##
	
	def pauseSession(self, toggle):
		if toggle:
			settings.session_manager.pauseSession()
		else:
			settings.session_manager.resumeSession()

	def skipTimeForwardOnce(self):
		if settings.session_manager.session_type == SessionType.SOLO \
				or settings.session_manager.session_type == SessionType.TEACHER: # allows kbd shortcut while teaching
			settings.session_manager.skipTimeForward(forward_time_skip)
	
	def spawnAircraft(self):
		n, ok = QInputDialog.getInt(self, 'Spawn new aircraft', 'Try to spawn:', value=1, min=1, max=99)
		if ok:
			for i in range(n): # WARNING: session should be running
				settings.session_manager.spawnNewControlledAircraft()
	
	def killSelectedAircraft(self):
		selected = selection.acft
		if selected is None:
			QMessageBox.critical(self, 'Cheat error', 'No aircraft selected.')
		else:
			selection.deselect()
			link = env.cpdlc.lastDataLink(selected.identifier)
			if link is not None and not link.isTerminated():
				link.terminate(True)
				link.resolveProblems()
			settings.session_manager.killAircraft(selected) # WARNING: killAircraft method must exist
			env.radar.scan()
	
	def setRejectedInstrPopUp(self, toggle):
		settings.solo_erroneous_instruction_warning = toggle
	
	def ensureClearWeather(self, toggle):
		settings.TWR_view_clear_weather_cheat = toggle
		if toggle:
			weather = mkWeather(settings.location_code) # clear weather with location code as station
			settings.controlled_tower_viewer.setWeather(weather)
		else:
			weather = env.primaryWeather()
			if weather is not None:
				settings.controlled_tower_viewer.setWeather(weather)
	
	def setShowRecognisedVoiceStrings(self, toggle):
		settings.show_recognised_voice_strings = toggle
	
	def changeTowerHeight(self):
		try:
			viewpoint_height = env.airport_data.viewpoints[settings.selected_viewpoint][1]
			original_info = 'original value is %d' % viewpoint_height
		except IndexError: # when no viewpoint is specified in source data file
			viewpoint_height = default_tower_height
			original_info = 'no original viewpoint in source data'
		new_height, ok = QInputDialog.getInt(self, 'Cheat tower height', 'New height in feet (%s):' % original_info,
				value=(viewpoint_height + settings.tower_height_cheat_offset), min=10, max=1500, step=10)
		if ok:
			settings.tower_height_cheat_offset = new_height - viewpoint_height
			self.towerView_pane.updateTowerPosition()
	
	def setRadarCheatMode(self, toggle):
		settings.radar_cheat = toggle
		env.radar.scan()
	
	def showAcftCheatToggles(self, toggle):
		self.selection_info_pane.showCheatToggle(toggle)
		self.selectionInfo_toolbarWidget.showCheatToggle(toggle)
	
	
	
	# -----------------     Internal GUI events      ------------------ #
	
	def closeEvent(self, event):
		env.radar.stopSweeping()
		if settings.session_manager.isRunning():
			settings.session_manager.stop()
		if settings.controlled_tower_viewer.running:
			settings.controlled_tower_viewer.stop(wait=True)
		if speech_recognition_available:
			cleanup_SR_language_files()
		print('Closing main window.')
		settings.saved_strip_racks = env.strips.rackNames()
		settings.saved_strip_dock_state = self.strip_pane.stateSave()
		settings.saved_workspace_windowed_view = self.central_workspace.windowedView()
		settings.saved_workspace_windows = self.central_workspace.workspaceWindowsStateSave()
		signals.mainWindowClosing.emit()
		signals.disconnect()
		settings.saveGeneralAndSystemSettings()
		settings.saveLocalSettings(env.airport_data)
		settings.savePresetChatMessages()
		env.resetEnv()
		settings.resetForNewWindow()
		EarthCoords.clearRadarPos()
		QMainWindow.closeEvent(self, event)
