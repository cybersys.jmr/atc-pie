
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from datetime import timedelta, timezone

from PyQt5.QtWidgets import QDialog, QMessageBox
from PyQt5.QtCore import QDateTime
from PyQt5.QtGui import QIcon
from ui.fplDetailsDialog import Ui_fplDetailsDialog
from ui.stripDetailsDialog import Ui_stripDetailsDialog

from base.util import some
from base.db import wake_turb_cat
from base.fpl import FPL
from base.route import Route
from base.utc import now
from base.strip import rack_detail, assigned_SQ_detail, \
		assigned_heading_detail, assigned_speed_detail, assigned_altitude_detail

from session.env import env

from gui.misc import signals, IconFile
from gui.dialog.miscDialogs import yesNo_question
from gui.dialog.routeDialog import RouteDialog
from gui.widgets.miscWidgets import RadioKeyEventFilter


# ---------- Constants ----------

unracked_strip_str = '(unracked)'
link_new_FPL_str = 'New flight plan (opens editor)'

# -------------------------------


def FPL_match_test(callsign_filter, fpl):
	return fpl.onlineStatus() != FPL.CLOSED \
			and env.linkedStrip(fpl) is None \
			and callsign_filter.upper() in some(fpl[FPL.CALLSIGN], '').upper() \
			and fpl.flightIsInTimeWindow(timedelta(hours=24))



class SharedDetailSheet:
	"""
	WARNING! Methods to define in subclasses:
	- get_detail (reads from strip or FPL)
	- set_detail (writes to strip or FPL)
	- resetData (should call this resetData after doing its additional work)
	- saveChangesAndClose (call this shared saveChangesAndClose)
	"""
	def __init__(self):
		self.route_edit.viewRoute_signal.connect(self.viewRoute)
		self.depAirportPicker_widget.recognised.connect(self.route_edit.setDEP)
		self.arrAirportPicker_widget.recognised.connect(self.route_edit.setARR)
		self.depAirportPicker_widget.unrecognised.connect(lambda: self.route_edit.setDEP(None))
		self.arrAirportPicker_widget.unrecognised.connect(lambda: self.route_edit.setARR(None))
		self.save_button.clicked.connect(self.saveChangesAndClose)
		self.resetData()
		self.aircraftType_edit.editTextChanged.connect(self.autoFillWTCfromType)
		self.autoFillWTC_button.toggled.connect(self.autoFillWTCtoggled)
	
	def autoFillWTCfromType(self, dez):
		if self.autoFillWTC_button.isChecked():
			self.wakeTurbCat_select.setCurrentText(some(wake_turb_cat(dez), ''))
	
	def autoFillWTCtoggled(self, toggle):
		if toggle:
			self.autoFillWTCfromType(self.aircraftType_edit.currentText())
	
	def viewRoute(self):
		origin_AD = self.depAirportPicker_widget.recognisedAirfield()
		dest_AD = self.arrAirportPicker_widget.recognisedAirfield()
		if origin_AD is not None and dest_AD is not None:
			route_to_view = Route(origin_AD, dest_AD, self.route_edit.getRouteText())
			tas = self.TAS_edit.speedValue() if self.TAS_enable.isChecked() else None
			RouteDialog(route_to_view, speedHint=tas, acftHint=self.aircraftType_edit.getAircraftType(), parent=self).exec()
	
	def resetData(self):
		# FPL.CALLSIGN
		self.callsign_edit.setText(some(self.get_detail(FPL.CALLSIGN), ''))
		#	FLIGHT_RULES
		self.flightRules_select.setCurrentText(some(self.get_detail(FPL.FLIGHT_RULES), ''))
		# FPL.ACFT_TYPE
		self.aircraftType_edit.setCurrentText(some(self.get_detail(FPL.ACFT_TYPE), ''))
		# FPL.WTC
		self.wakeTurbCat_select.setCurrentText(some(self.get_detail(FPL.WTC), ''))
		# FPL.ICAO_DEP
		self.depAirportPicker_widget.setEditText(some(self.get_detail(FPL.ICAO_DEP), ''))
		# FPL.ICAO_ARR
		self.arrAirportPicker_widget.setEditText(some(self.get_detail(FPL.ICAO_ARR), ''))
		#	ROUTE
		self.route_edit.setRouteText(some(self.get_detail(FPL.ROUTE), ''))
		# CRUISE_ALT
		cr_alt = self.get_detail(FPL.CRUISE_ALT)
		self.cruiseAlt_enable.setChecked(cr_alt is not None)
		if cr_alt is not None:
			self.cruiseAlt_edit.setAltFlSpec(cr_alt)
		#	TAS
		tas = self.get_detail(FPL.TAS)
		self.TAS_enable.setChecked(tas is not None)
		if tas is not None:
			self.TAS_edit.setSpeedValue(tas)
		#	COMMENTS
		self.comments_edit.setPlainText(some(self.get_detail(FPL.COMMENTS), ''))
		self.callsign_edit.setFocus()
		
	def saveChangesAndClose(self):
		# FPL.CALLSIGN
		self.set_detail(FPL.CALLSIGN, self.callsign_edit.text().upper())
		# FPL.FLIGHT_RULES
		self.set_detail(FPL.FLIGHT_RULES, self.flightRules_select.currentText())
		# FPL.ACFT_TYPE
		self.set_detail(FPL.ACFT_TYPE, self.aircraftType_edit.getAircraftType())
		# FPL.WTC
		self.set_detail(FPL.WTC, self.wakeTurbCat_select.currentText())
		# FPL.ICAO_DEP
		self.set_detail(FPL.ICAO_DEP, self.depAirportPicker_widget.currentText())
		# FPL.ICAO_ARR
		self.set_detail(FPL.ICAO_ARR, self.arrAirportPicker_widget.currentText())
		# FPL.ROUTE
		self.set_detail(FPL.ROUTE, self.route_edit.getRouteText())
		# FPL.CRUISE_ALT
		self.set_detail(FPL.CRUISE_ALT, (self.cruiseAlt_edit.altFlSpec() if self.cruiseAlt_enable.isChecked() else None))
		# FPL.TAS
		self.set_detail(FPL.TAS, (self.TAS_edit.speedValue() if self.TAS_enable.isChecked() else None))
		# FPL.COMMENTS
		self.set_detail(FPL.COMMENTS, self.comments_edit.toPlainText())













# =========== STRIP =========== #

class StripDetailSheetDialog(QDialog, Ui_stripDetailsDialog, SharedDetailSheet):
	def __init__(self, gui, strip):
		QDialog.__init__(self, gui)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setWindowIcon(QIcon(IconFile.pixmap_strip))
		self.linkFPL_reset_button.setIcon(QIcon(IconFile.action_newFPL))
		self.assignedAltitude_edit.syncWithEnv(True)
		self.strip = strip
		self.FPL_link_on_save = None # None to link a new FPL
		self.FPL_matches = [] # Flight plans matching the last callsign edit
		if self.strip.lookup(rack_detail) is None: # unracked strip
			self.rack_select.addItem(unracked_strip_str)
			self.rack_select.setEnabled(False)
		else:
			self.rack_select.addItems(env.strips.rackNames())
		self.depAirportPicker_widget.recognised.connect(lambda ad: self.depAirportName_info.setText(ad.long_name))
		self.arrAirportPicker_widget.recognised.connect(lambda ad: self.arrAirportName_info.setText(ad.long_name))
		self.depAirportPicker_widget.unrecognised.connect(self.depAirportName_info.clear)
		self.arrAirportPicker_widget.unrecognised.connect(self.arrAirportName_info.clear)
		self.cruiseAlt_enable.toggled.connect(self.assignCruiseAlt_button.setEnabled)
		self.assignCruiseAlt_button.clicked.connect(self.assignCruiseLevel)
		linkedFPL = self.strip.linkedFPL()
		if linkedFPL is None:
			self.fplLink_stackedWidget.setCurrentWidget(self.noFplLink_page)
			self.updateFplToLinkInfo()
			self.matchingFPLs_button.clicked.connect(self.matchingFPLsButtonClicked)
			self.callsign_edit.textChanged.connect(self.updateMatchingFPLs)
			self.linkFPL_tickBox.toggled.connect(self.updateFplToLinkInfo)
			self.linkFPL_reset_button.clicked.connect(self.fplToLinkReset)
		else: # a flight plan is already linked
			self.matchingFPLs_button.setVisible(False)
			self.fplLink_stackedWidget.setCurrentWidget(self.gotFplLink_page)
			self.linkedFPL_info.setText(linkedFPL.shortDescr())
		SharedDetailSheet.__init__(self)
		if self.strip.linkedFPL() is None:
			self.updateMatchingFPLs(self.callsign_edit.text())
	
	def matchingFPLsButtonClicked(self):
		if len(self.FPL_matches) == 1:
			if yesNo_question(self, 'Single matching FPL', self.FPL_matches[0].shortDescr(),
					'Do you want to link this flight plan when saving the strip?'):
				self.linkFPL_tickBox.setChecked(True)
				self.FPL_link_on_save = self.FPL_matches[0]
				self.updateFplToLinkInfo()
		else:
			msg = '%d flight plans today matching callsign filter:\n' % len(self.FPL_matches)
			msg += '\n'.join('  %s' % fpl.shortDescr() for fpl in self.FPL_matches)
			QMessageBox.information(self, 'Matching FPLs', msg)
	
	def fplToLinkReset(self):
		self.FPL_link_on_save = None
		self.updateFplToLinkInfo()
	
	def updateFplToLinkInfo(self):
		if self.linkFPL_tickBox.isChecked():
			if self.FPL_link_on_save is None:
				self.linkFPL_info.setText(link_new_FPL_str)
			else:
				self.linkFPL_info.setText(self.FPL_link_on_save.shortDescr())
			self.linkFPL_reset_button.setEnabled(self.FPL_link_on_save is not None)
		else:
			self.linkFPL_info.setText('none')
			self.linkFPL_reset_button.setEnabled(False)
	
	def updateMatchingFPLs(self, cs):
		self.FPL_matches = env.FPLs.findAll(lambda fpl: FPL_match_test(cs, fpl))
		self.matchingFPLs_button.setVisible(len(self.FPL_matches) > 0)
		self.matchingFPLs_button.setText('(%d)' % len(self.FPL_matches))
	
	def assignCruiseLevel(self):
		self.assignedAltitude_edit.setAltFlSpec(self.cruiseAlt_edit.altFlSpec())
		self.assignAltitude.setChecked(True)
	
	def get_detail(self, detail):
		return self.strip.lookup(detail)
	
	def set_detail(self, detail, new_val):
		self.strip.writeDetail(detail, new_val)
	
	def selectedRack(self):
		return None if self.strip.lookup(rack_detail) is None else self.rack_select.currentText()
	
	def resetData(self):
		## Rack
		self.rack_select.setCurrentText(some(self.strip.lookup(rack_detail), unracked_strip_str))
		## Assigned stuff
		# Squawk code
		assSQ = self.strip.lookup(assigned_SQ_detail)
		self.assignSquawkCode.setChecked(assSQ is not None)
		if assSQ is not None:
			self.xpdrCode_select.setSQ(assSQ)
		# Heading
		assHdg = self.strip.lookup(assigned_heading_detail)
		self.assignHeading.setChecked(assHdg is not None)
		if assHdg is not None:
			self.assignedHeading_edit.setValue(int(assHdg.read()))
		# Altitude/FL
		assAlt = self.strip.lookup(assigned_altitude_detail)
		self.assignAltitude.setChecked(assAlt is not None)
		if assAlt is not None:
			self.assignedAltitude_edit.setAltFlSpec(assAlt)
		# Speed
		assSpd = self.strip.lookup(assigned_speed_detail)
		self.assignSpeed.setChecked(assSpd is not None)
		if assSpd is not None:
			self.assignedSpeed_edit.setSpeedValue(assSpd)
		## Links and conflicts:
		acft = self.strip.linkedAircraft()
		if acft is None:
			self.xpdrConflicts_info.setText('no link')
		else:
			clst = self.strip.xpdrConflicts()
			if len(clst) == 0:
				self.xpdrConflicts_info.setText('no conflicts')
			else:
				self.xpdrConflicts_info.setText('conflicts ' + \
						', '.join(FPL.detailStrNames.get(d, ('CODE' if d == assigned_SQ_detail else '??' + str(d))) for d in clst))
		fpl = self.strip.linkedFPL()
		if fpl is None:
			self.fplConflicts_info.setText('no link')
		else:
			clst = self.strip.fplConflicts()
			if len(clst) == 0:
				self.fplConflicts_info.setText('no conflicts')
			else:
				self.fplConflicts_info.setText('conflicts ' + ', '.join(FPL.detailStrNames[d] for d in clst))
		## FPL stuff
		SharedDetailSheet.resetData(self)
	
	def saveChangesAndClose(self):
		SharedDetailSheet.saveChangesAndClose(self)
		## Assigned stuff
		# Squawk code
		if self.assignSquawkCode.isChecked():
			self.set_detail(assigned_SQ_detail, self.xpdrCode_select.getSQ())
		else:
			self.set_detail(assigned_SQ_detail, None)
		# Heading
		if self.assignHeading.isChecked():
			self.set_detail(assigned_heading_detail, self.assignedHeading_edit.headingValue(False))
		else:
			self.set_detail(assigned_heading_detail, None)
		# Altitude/FL
		if self.assignAltitude.isChecked():
			self.set_detail(assigned_altitude_detail, self.assignedAltitude_edit.altFlSpec())
		else:
			self.set_detail(assigned_altitude_detail, None)
		# Speed
		if self.assignSpeed.isChecked():
			self.set_detail(assigned_speed_detail, self.assignedSpeed_edit.speedValue())
		else:
			self.set_detail(assigned_speed_detail, None)
		# DONE with details
		if self.strip.linkedFPL() is None:
			if self.linkFPL_tickBox.isChecked():
				if self.FPL_link_on_save is None:
					signals.newLinkedFPLrequest.emit(self.strip)
				else:
					self.strip.linkFPL(self.FPL_link_on_save)
		else: # a flight plan is already linked
			if self.pushToFPL_tickBox.isChecked():
				self.strip.pushToFPL()
		self.accept()
		# WARNING: deal with self.rack_select change after dialog accept (use selectedRack method)



















# =========== FPL =========== #

class FPLdetailSheetDialog(QDialog, Ui_fplDetailsDialog, SharedDetailSheet):
	def __init__(self, gui, fpl):
		QDialog.__init__(self, gui)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setWindowIcon(QIcon(IconFile.panel_FPLs))
		self.fpl = fpl
		if fpl.isOnline():
			self.onlineStatus_info.setText(fpl.onlineStatusStr())
			self.localChanges_info.setText(', '.join(FPL.detailStrNames[d] for d in fpl.modified_details))
		else:
			self.onlineStatus_info.setText('not online')
			self.localChanges_info.setText('N/A')
		self.stripLinked_info.setText('no' if env.linkedStrip(fpl) is None else 'yes')
		SharedDetailSheet.__init__(self)
	
	def get_detail(self, detail):
		return self.fpl[detail]
	
	def set_detail(self, detail, new_val):
		self.fpl[detail] = new_val
	
	def resetData(self):
		# FPL.TIME_OF_DEP
		dep = self.fpl[FPL.TIME_OF_DEP]
		self.depTime_enable.setChecked(dep is not None)
		if dep is None:
			dep = now()
		self.depTime_edit.setDateTime(QDateTime(dep.year, dep.month, dep.day, dep.hour, dep.minute))
		# FPL.EET
		eet = self.fpl[FPL.EET]
		self.EET_enable.setChecked(eet is not None)
		if eet is not None:
			minutes = int(eet.total_seconds() / 60 + .5)
			self.EETh_edit.setValue(minutes // 60)
			self.EETmin_edit.setValue(minutes % 60)
		#	ICAO_ALT
		self.altAirportPicker_widget.setEditText(some(self.fpl[FPL.ICAO_ALT], ''))
		#	SOULS
		souls = self.fpl[FPL.SOULS]
		self.soulsOnBoard_enable.setChecked(souls is not None)
		if souls is not None:
			self.soulsOnBoard_edit.setValue(souls)
		SharedDetailSheet.resetData(self)
	
	def saveChangesAndClose(self):
		SharedDetailSheet.saveChangesAndClose(self)
		# FPL.TIME_OF_DEP
		if self.depTime_enable.isChecked():
			self.set_detail(FPL.TIME_OF_DEP, self.depTime_edit.dateTime().toPyDateTime().replace(tzinfo=timezone.utc))
		else:
			self.set_detail(FPL.TIME_OF_DEP, None)
		# FPL.EET
		if self.EET_enable.isChecked():
			self.set_detail(FPL.EET, timedelta(hours=self.EETh_edit.value(), minutes=self.EETmin_edit.value()))
		else:
			self.set_detail(FPL.EET, None)
		# FPL.ICAO_ALT
		self.set_detail(FPL.ICAO_ALT, self.altAirportPicker_widget.currentText())
		# FPL.SOULS
		self.set_detail(FPL.SOULS, (self.soulsOnBoard_edit.value() if self.soulsOnBoard_enable.isChecked() else None))
		# Done details!
		self.accept()

