
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from datetime import timedelta
from hashlib import md5

from PyQt5.QtCore import Qt, QAbstractListModel
from PyQt5.QtWidgets import QTabWidget, QDialog, QMessageBox, QInputDialog, QFileDialog
from ui.localSettingsDialog import Ui_localSettingsDialog
from ui.generalSettingsDialog import Ui_generalSettingsDialog
from ui.systemSettingsDialog import Ui_systemSettingsDialog
from ui.soloSessionSettingsDialog import Ui_soloSessionSettingsDialog

from base.acft import snapshot_history_size
from base.util import some

from session.config import settings, XpdrAssignmentRange
from session.env import env

from ext.fgfs import fgTwrCommonOptions
from ext.mumble import mumble_available
from ext.sr import speech_recognition_available
from ext.tts import speech_synthesis_available

from gui.misc import signals, SimpleStringListModel
from gui.dialog.runways import RunwayParametersWidget
from gui.panels.notifier import Notification, icon_files, sound_files
from gui.widgets.miscWidgets import RadioKeyEventFilter


# ---------- Constants ----------

# -------------------------------


class SemiCircRule:
	rules = OFF, E_W, N_S = range(3)




# =================================
#
#           S Y S T E M
#
# =================================

class SystemSettingsDialog(QDialog, Ui_systemSettingsDialog):
	#STATIC:
	last_tab_used = 0
	
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.solo_page.setEnabled(not settings.session_manager.isRunning())
		self.flightgear_page.setEnabled(not settings.session_manager.isRunning())
		self.fsdMP_page.setEnabled(not settings.session_manager.isRunning())
		self.towerView_page.setEnabled(env.airport_data is not None and not settings.controlled_tower_viewer.running)
		self.voice_page.setEnabled(not settings.session_manager.isRunning())
		self.speechRecognition_groupBox.setEnabled(speech_recognition_available)
		if speech_recognition_available:
			self.sphinxAcousticModel_edit.setClearButtonEnabled(True)
		self.lennyPasswordChange_info.clear()
		self.landLines_groupBox.setEnabled(mumble_available)
		self.lennyPassword_edit.setPlaceholderText('No password set' if settings.lenny64_password_md5 == '' else '(unchanged)')
		self.fillFromSettings()
		self.settings_tabs.setCurrentIndex(SystemSettingsDialog.last_tab_used)
		self.fgSocialName_edit.editingFinished.connect(lambda: self.fsdSocialName_edit.setText(self.fgSocialName_edit.text()))
		self.fsdSocialName_edit.editingFinished.connect(lambda: self.fgSocialName_edit.setText(self.fsdSocialName_edit.text()))
		self.produceExtViewerCmd_button.clicked.connect(self.showExternalViewerFgOptions)
		self.lennyPassword_edit.textChanged.connect(self._updateLennyPasswordInfo)
		self.browseForSphinxAcousticModel_button.clicked.connect(self.browseForSphinxAcousticModel)
		self.buttonBox.accepted.connect(self.storeSettings)
	
	def _updateLennyPasswordInfo(self, s):
		self.lennyPasswordChange_info.setText('Changing password' if s != '' and settings.lenny64_password_md5 != '' else '')
	
	def browseForSphinxAcousticModel(self):
		txt = QFileDialog.getExistingDirectory(self, caption='Choose Sphinx acoustic model directory')
		if txt != '':
			self.sphinxAcousticModel_edit.setText(txt)
	
	def showExternalViewerFgOptions(self):
		required_options = fgTwrCommonOptions()
		required_options.append('--multiplay=out,100,this_host,%d' % settings.FGFS_views_send_port)
		required_options.append('--multiplay=in,100,,%d' % self.towerView_fgmsPort_edit.value())
		required_options.append('--telnet=,,100,,%d,' % self.towerView_telnetPort_edit.value())
		print('Options required for external FlightGear viewer with current dialog options: ' + ' '.join(required_options))
		msg = 'Options required with present configuration (also sent to console):\n'
		msg += '\n'.join('  ' + opt for opt in required_options)
		msg += '\n\nNB: Replace "this_host" with appropriate value.'
		QMessageBox.information(self, 'Required FlightGear options', msg)

	def fillFromSettings(self):
		## Solo and teacher session types
		self.soloAircraftTypes_edit.setPlainText('\n'.join(settings.solo_aircraft_types))
		self.restrictAirlineChoiceToLiveries_tickBox.setChecked(settings.solo_restrict_to_available_liveries)
		self.preferEntryExitAirports_tickBox.setChecked(settings.solo_prefer_entry_exit_ADs)
		self.sphinxAcousticModel_edit.setText(settings.sphinx_acoustic_model_dir)
		## FlightGear
		self.fgmsServerHost_edit.setText(settings.FGMS_server_host)
		self.fgmsServerPort_edit.setValue(settings.FGMS_server_port)
		self.fgmsLegacyProtocol_tickBox.setChecked(settings.FGMS_legacy_protocol)
		self.fgSocialName_edit.setText(settings.MP_social_name)
		self.ircServerHost_edit.setText(settings.FG_IRC_server_host)
		self.ircServerPort_edit.setValue(settings.FG_IRC_server_port)
		self.ircChannel_edit.setText(settings.FG_IRC_channel)
		self.orsxServer_edit.setText(settings.ORSX_server_name)
		self.orsxHandoverRange_edit.setValue(some(settings.ORSX_handover_range, 0))
		self.lennyAccountEmail_edit.setText(settings.lenny64_account_email)
		self.lennyPassword_edit.clear() # unchanged if stays blank
		self.fgFplLookUpInterval_edit.setValue(0 if settings.FG_FPL_update_interval is None \
				else int(settings.FG_FPL_update_interval.total_seconds()) / 60)
		self.fgWeatherLookUpInterval_edit.setValue(0 if settings.FG_METAR_update_interval is None \
				else int(settings.FG_METAR_update_interval.total_seconds()) / 60)
		(self.fgcom_mumble_radioButton if settings.FGCom_use_Mumble_plugin else self.fgcom_standalone_radioButton).setChecked(True)
		self.fgcomExe_edit.setText(settings.FGCom_executable)
		self.fgcomServer_edit.setText(settings.FGCom_server)
		self.fgcomReservedPort_edit.setValue(settings.FGCom_reserved_port)
		self.fgcomMumbleHost_edit.setText(settings.FGCom_Mumble_host)
		self.fgcomMumblePort_edit.setValue(settings.FGCom_Mumble_port)
		self.fgcomMumbleSoundEffects_tickBox.setChecked(settings.FGCom_Mumble_sound_effects)
		## FSD
		self.fsdServerHost_edit.setText(settings.FSD_server_host)
		self.fsdServerPort_edit.setValue(settings.FSD_server_port)
		self.fsdCid_edit.setText(settings.FSD_cid)
		self.fsdRating_edit.setValue(settings.FSD_rating)
		self.fsdPassword_edit.setText(settings.FSD_password)
		self.fsdSocialName_edit.setText(settings.MP_social_name)
		(self.fsdWeather_askFsd_radioButton if settings.FSD_weather_from_server else self.fsdWeather_fetchReal_radioButton).setChecked(True)
		self.fsdWeatherLookUpInterval_edit.setValue(0 if settings.FSD_METAR_update_interval is None \
				else int(settings.FSD_METAR_update_interval.total_seconds()) / 60)
		## Tower view
		(self.towerView_external_radioButton if settings.external_tower_viewer_process else self.towerView_internal_radioButton).setChecked(True)
		self.towerView_fgmsPort_edit.setValue(settings.tower_viewer_UDP_port)
		self.towerView_telnetPort_edit.setValue(settings.tower_viewer_telnet_port)
		self.fgCommand_edit.setText(settings.FGFS_executable)
		self.fgRootDir_edit.setText(settings.FGFS_root_dir)
		self.fgAircraftDir_edit.setText(settings.FGFS_aircraft_dir)
		self.fgSceneryDir_edit.setText(settings.FGFS_scenery_dir)
		self.externalTowerViewerHost_edit.setText(settings.external_tower_viewer_host)
		## Phone lines
		self.mumbleServerHost_edit.setText(settings.land_line_server_host)
		self.mumbleServerPort_edit.setValue(settings.land_line_server_port)
		self.mumbleMoveToChannel_edit.setText(settings.land_line_move_to_channel)

	def storeSettings(self):
		if ':' in self.fsdSocialName_edit.text():
			QMessageBox.critical(self, 'Invalid entry', 'Invalid FSD social name.')
			return
		SystemSettingsDialog.last_tab_used = self.settings_tabs.currentIndex()
		## Solo and teacher session types
		settings.solo_aircraft_types = [s for s in self.soloAircraftTypes_edit.toPlainText().split('\n') if s != '']
		settings.solo_restrict_to_available_liveries = self.restrictAirlineChoiceToLiveries_tickBox.isChecked()
		settings.solo_prefer_entry_exit_ADs = self.preferEntryExitAirports_tickBox.isChecked()
		settings.sphinx_acoustic_model_dir = self.sphinxAcousticModel_edit.text()
		## FlightGear
		settings.FGMS_server_host = self.fgmsServerHost_edit.text()
		settings.FGMS_server_port = self.fgmsServerPort_edit.value()
		settings.FGMS_legacy_protocol = self.fgmsLegacyProtocol_tickBox.isChecked()
		settings.MP_social_name = self.fgSocialName_edit.text().strip()
		settings.FG_IRC_server_host = self.ircServerHost_edit.text()
		settings.FG_IRC_server_port = self.ircServerPort_edit.value()
		settings.FG_IRC_channel = self.ircChannel_edit.text()
		settings.ORSX_server_name = self.orsxServer_edit.text()
		settings.ORSX_handover_range = None if self.orsxHandoverRange_edit.value() == 0 else self.orsxHandoverRange_edit.value()
		settings.lenny64_account_email = self.lennyAccountEmail_edit.text()
		new_lenny64_pwd = self.lennyPassword_edit.text()
		if new_lenny64_pwd != '': # password change!
			digester = md5()
			digester.update(bytes(new_lenny64_pwd, 'utf8'))
			settings.lenny64_password_md5 = ''.join('%02x' % x for x in digester.digest())
		fgwxint = self.fgWeatherLookUpInterval_edit.value()
		settings.FG_METAR_update_interval = None if fgwxint == 0 else timedelta(minutes=fgwxint)
		fgfplint = self.fgFplLookUpInterval_edit.value()
		settings.FG_FPL_update_interval = None if fgfplint == 0 else timedelta(minutes=fgfplint)
		settings.FGCom_use_Mumble_plugin = self.fgcom_mumble_radioButton.isChecked()
		settings.FGCom_executable = self.fgcomExe_edit.text()
		settings.FGCom_server = self.fgcomServer_edit.text()
		settings.FGCom_reserved_port = self.fgcomReservedPort_edit.value()
		settings.FGCom_Mumble_host = self.fgcomMumbleHost_edit.text()
		settings.FGCom_Mumble_port = self.fgcomMumblePort_edit.value()
		settings.FGCom_Mumble_sound_effects = self.fgcomMumbleSoundEffects_tickBox.isChecked()
		## FSD
		settings.FSD_server_host = self.fsdServerHost_edit.text()
		settings.FSD_server_port = self.fsdServerPort_edit.value()
		settings.FSD_cid = self.fsdCid_edit.text()
		settings.FSD_rating = self.fsdRating_edit.value()
		settings.FSD_password = self.fsdPassword_edit.text()
		# self.fsdSocialName_edit already saved by self.fgSocialName_edit
		settings.FSD_weather_from_server = self.fsdWeather_askFsd_radioButton.isChecked()
		fsdwxint = self.fsdWeatherLookUpInterval_edit.value()
		settings.FSD_METAR_update_interval = None if fsdwxint == 0 else timedelta(minutes=fsdwxint)
		## Tower view
		settings.external_tower_viewer_process = self.towerView_external_radioButton.isChecked()
		settings.tower_viewer_UDP_port = self.towerView_fgmsPort_edit.value()
		settings.tower_viewer_telnet_port = self.towerView_telnetPort_edit.value()
		settings.FGFS_executable = self.fgCommand_edit.text()
		settings.FGFS_root_dir = self.fgRootDir_edit.text()
		settings.FGFS_aircraft_dir = self.fgAircraftDir_edit.text()
		settings.FGFS_scenery_dir = self.fgSceneryDir_edit.text()
		settings.external_tower_viewer_host = self.externalTowerViewerHost_edit.text()
		## Phone lines
		settings.land_line_server_host = self.mumbleServerHost_edit.text()
		settings.land_line_server_port = self.mumbleServerPort_edit.value()
		settings.land_line_move_to_channel = self.mumbleMoveToChannel_edit.text()
		
		signals.systemSettingsChanged.emit()
		self.accept()




# =================================
#
#       S O L O   S Y S T E M
#
# =================================


class SoloSessionSettingsDialog(QDialog, Ui_soloSessionSettingsDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.cpdlcConnections_label.setEnabled(settings.controller_pilot_data_link)
		self.cpdlcConnections_widget.setEnabled(settings.controller_pilot_data_link)
		self.airportMode_groupBox.setEnabled(env.airport_data is not None)
		self.voiceInstr_off_radioButton.setChecked(True) # sets a defult; auto-excludes if voice instr selected below
		self.readBack_off_radioButton.setChecked(True) # sets a defult; auto-excludes if other selection below
		self.voiceInstr_on_radioButton.setEnabled(speech_recognition_available)
		self.readBack_voice_radioButton.setEnabled(speech_synthesis_available)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.fillFromSettings()
		self.buttonBox.accepted.connect(self.storeSettings)
	
	def fillFromSettings(self):
		self.maxAircraftCount_edit.setValue(settings.solo_max_aircraft_count)
		self.minSpawnDelay_seconds_edit.setValue(int(settings.solo_min_spawn_delay.total_seconds()))
		self.maxSpawnDelay_minutes_edit.setValue(int(settings.solo_max_spawn_delay.total_seconds()) / 60)
		self.cpdlcConnectionBalance_edit.setValue(int(100 * settings.solo_CPDLC_balance))
		self.distractorCount_edit.setValue(settings.solo_distracting_traffic_count)
		self.ARRvsDEP_edit.setValue(int(100 * settings.solo_ARRvsDEP_balance))
		self.ILSvsVisual_edit.setValue(int(100 * settings.solo_ILSvsVisual_balance))
		self.soloWeatherChangeInterval_edit.setValue(0 if settings.solo_weather_change_interval is None \
				else int(settings.solo_weather_change_interval.total_seconds()) / 60)
		self.voiceInstr_on_radioButton.setChecked(self.voiceInstr_on_radioButton.isEnabled() and settings.solo_voice_instructions)
		self.readBack_wilcoBeep_radioButton.setChecked(settings.solo_wilco_beeps)
		self.readBack_voice_radioButton.setChecked(self.readBack_voice_radioButton.isEnabled() and settings.solo_voice_readback)
	
	def storeSettings(self):
		settings.solo_max_aircraft_count = self.maxAircraftCount_edit.value()
		settings.solo_min_spawn_delay = timedelta(seconds=self.minSpawnDelay_seconds_edit.value())
		settings.solo_max_spawn_delay = timedelta(minutes=self.maxSpawnDelay_minutes_edit.value())
		settings.solo_CPDLC_balance = self.cpdlcConnectionBalance_edit.value() / 100
		settings.solo_distracting_traffic_count = self.distractorCount_edit.value()
		settings.solo_ARRvsDEP_balance = self.ARRvsDEP_edit.value() / 100
		settings.solo_ILSvsVisual_balance = self.ILSvsVisual_edit.value() / 100
		settings.solo_weather_change_interval = None if self.soloWeatherChangeInterval_edit.value() == 0 \
				else timedelta(minutes=self.soloWeatherChangeInterval_edit.value())
		settings.solo_voice_instructions = self.voiceInstr_on_radioButton.isChecked()
		settings.solo_wilco_beeps = self.readBack_wilcoBeep_radioButton.isChecked()
		settings.solo_voice_readback = self.readBack_voice_radioButton.isChecked()
		signals.soloSessionSettingsChanged.emit()
		self.accept()







# =================================
#
#           G E N E R A L
#
# =================================

class SoundNotificationsListModel(QAbstractListModel):
	def __init__(self, parent):
		QAbstractListModel.__init__(self, parent)
		self.listed_types = sorted([t for t in Notification.types if t in sound_files], key=Notification.tstr)
		self.tick_list = [t in settings.sound_notifications for t in self.listed_types]
	
	def applyChoices(self):
		settings.sound_notifications.clear()
		for i, t in enumerate(self.listed_types):
			if self.tick_list[i]:
				settings.sound_notifications.add(t)
	
	# MODEL STUFF
	def rowCount(self, parent=None):
		return len(self.listed_types)
	
	def flags(self, index):
		return Qt.ItemIsEnabled | Qt.ItemIsUserCheckable
	
	def data(self, index, role):
		if role == Qt.DisplayRole:
			t = self.listed_types[index.row()]
			txt = Notification.tstr(t)
			if t not in icon_files:
				txt += ' (*)' # UI key under table: "not logged in the notification panel"
			return txt
		if role == Qt.CheckStateRole:
			return Qt.Checked if self.tick_list[index.row()] else Qt.Unchecked
	
	def setData(self, index, value, role):
		if index.isValid() and role == Qt.CheckStateRole:
			self.tick_list[index.row()] = value == Qt.Checked
			return True
		return False



class GeneralSettingsDialog(QDialog, Ui_generalSettingsDialog):
	#STATIC:
	last_tab_used = 0
	
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.positionHistoryTraceTime_edit.setMaximum(snapshot_history_size) # limit is one snapshot per second
		self.autoLinkStrip_off_radioButton.setChecked(True) # sets a default; auto-excludes if different selection below
		self.installEventFilter(RadioKeyEventFilter(self))
		self.msg_presets_model = SimpleStringListModel(self, True)
		self.messageList_view.setModel(self.msg_presets_model)
		self.fillFromSettings()
		self.settings_tabs.setCurrentIndex(GeneralSettingsDialog.last_tab_used)
		self.addMsgPreset_button.clicked.connect(self.addPresetMessage)
		self.rmMsgPreset_button.clicked.connect(self.removePresetMessage)
		self.buttonBox.accepted.connect(self.storeSettings)
	
	def addPresetMessage(self):
		msg, ok = QInputDialog.getText(self, 'New preset text message', 'Enter message (aliases allowed):')
		if ok:
			self.msg_presets_model.appendString(msg)
	
	def removePresetMessage(self):
		ilst = self.messageList_view.selectedIndexes()
		if ilst != []:
			self.msg_presets_model.removeRow(ilst[0].row())

	def fillFromSettings(self):
		self.routeVectWarnings_tickBox.setChecked(settings.strip_route_vect_warnings)
		self.cpdlcStatusIntegrationToStrips_tickBox.setChecked(settings.strip_CPDLC_integration)
		self.verticalRwyBoxLayout_tickBox.setChecked(settings.vertical_runway_box_layout)
		self.confirmHandovers_tickBox.setChecked(settings.confirm_handovers)
		self.confirmLossyStripReleases_tickBox.setChecked(settings.confirm_lossy_strip_releases)
		self.confirmLinkedStripDeletions_tickBox.setChecked(settings.confirm_linked_strip_deletions)
		self.autoFillStripFromXPDR_tickBox.setChecked(settings.strip_autofill_on_ACFT_link)
		self.autoFillStripFromFPL_tickBox.setChecked(settings.strip_autofill_on_FPL_link)
		self.autoFillStripBeforeHandovers_tickBox.setChecked(settings.strip_autofill_before_handovers)
		self.autoLinkStrip_identModeS_radioButton.setChecked(settings.strip_autolink_on_ident)
		self.autoLinkStrip_identAll_radioButton.setChecked(settings.strip_autolink_on_ident and settings.strip_autolink_include_modeC)
		
		self.positionHistoryTraceTime_edit.setValue(int(settings.radar_contact_trace_time.total_seconds()))
		self.toleratedInvisibleSweeps_edit.setValue(int(settings.invisible_blips_before_contact_lost))
		self.flSpeedLine2_radioButton.setChecked(not settings.radar_tag_FL_at_bottom)
		self.flSpeedLine3_radioButton.setChecked(settings.radar_tag_FL_at_bottom)
		self.tagSpeedUnits_radioButton.setChecked(not settings.radar_tag_speed_tens)
		self.tagSpeedTens_radioButton.setChecked(settings.radar_tag_speed_tens)
		{0: self.wtcNotShown_radioButton, 1: self.wtcFollowsType_radioButton, 2: self.wtcFollowsSpeed_radioButton}.get(
			settings.radar_tag_WTC_position, self.wtcNotShown_radioButton).setChecked(True)
		self.interpretXpdrFl_tickBox.setChecked(settings.radar_tag_interpret_XPDR_FL)
		
		self.headingTolerance_edit.setValue(settings.heading_tolerance)
		self.altitudeTolerance_edit.setValue(settings.altitude_tolerance)
		self.speedTolerance_edit.setValue(settings.speed_tolerance)
		self.conflictWarningTime_edit.setValue(int(settings.route_conflict_anticipation.total_seconds()) // 60)
		self.trafficConsidered_select.setCurrentIndex(settings.route_conflict_traffic)

		self.cpdlcTimeout_edit.setValue(0 if settings.CPDLC_ACK_timeout is None else int(settings.CPDLC_ACK_timeout.total_seconds()))
		self.cpdlcXfrAcceptedSendsStrip_tickBox.setChecked(settings.CPDLC_send_strips_on_accepted_transfers)
		self.cpdlcRaiseWindows_tickBox.setChecked(settings.CPDLC_raises_windows)
		self.cpdlcCloseWindows_tickBox.setChecked(settings.CPDLC_closes_windows)

		self.autoAtcChatWindowPopUp_tickBox.setChecked(settings.private_ATC_msg_auto_raise)
		self.notifyGeneralChatRoomMsg_tickBox.setChecked(settings.ATC_chatroom_msg_notifications)
		self.textChatMessagesVisibleTime_edit.setValue(0 if settings.text_chat_history_time is None \
				else int(settings.text_chat_history_time.total_seconds()) // 60)
		self.msg_presets_model.setStringList(settings.preset_chat_messages)
		
		self.sound_notification_model = SoundNotificationsListModel(self)
		self.soundNotification_listView.setModel(self.sound_notification_model)
		self.pttMutesSounds_tickBox.setChecked(settings.PTT_mutes_notifications)
	
	def storeSettings(self):
		GeneralSettingsDialog.last_tab_used = self.settings_tabs.currentIndex()
		
		settings.strip_route_vect_warnings = self.routeVectWarnings_tickBox.isChecked()
		settings.strip_CPDLC_integration = self.cpdlcStatusIntegrationToStrips_tickBox.isChecked()
		settings.vertical_runway_box_layout = self.verticalRwyBoxLayout_tickBox.isChecked()
		settings.confirm_handovers = self.confirmHandovers_tickBox.isChecked()
		settings.confirm_lossy_strip_releases = self.confirmLossyStripReleases_tickBox.isChecked()
		settings.confirm_linked_strip_deletions = self.confirmLinkedStripDeletions_tickBox.isChecked()
		settings.strip_autofill_on_ACFT_link = self.autoFillStripFromXPDR_tickBox.isChecked()
		settings.strip_autofill_on_FPL_link = self.autoFillStripFromFPL_tickBox.isChecked()
		settings.strip_autofill_before_handovers = self.autoFillStripBeforeHandovers_tickBox.isChecked()
		settings.strip_autolink_on_ident = not self.autoLinkStrip_off_radioButton.isChecked()
		settings.strip_autolink_include_modeC = self.autoLinkStrip_identAll_radioButton.isChecked()
		
		settings.radar_contact_trace_time = timedelta(seconds=self.positionHistoryTraceTime_edit.value())
		settings.invisible_blips_before_contact_lost = self.toleratedInvisibleSweeps_edit.value()
		settings.radar_tag_FL_at_bottom = self.flSpeedLine3_radioButton.isChecked()
		settings.radar_tag_speed_tens = self.tagSpeedTens_radioButton.isChecked()
		settings.radar_tag_WTC_position = 1 if self.wtcFollowsType_radioButton.isChecked() \
			else 2 if self.wtcFollowsSpeed_radioButton.isChecked() else 0
		settings.radar_tag_interpret_XPDR_FL = self.interpretXpdrFl_tickBox.isChecked()
		
		settings.heading_tolerance = self.headingTolerance_edit.value()
		settings.altitude_tolerance = self.altitudeTolerance_edit.value()
		settings.speed_tolerance = self.speedTolerance_edit.value()
		settings.route_conflict_anticipation = timedelta(minutes=self.conflictWarningTime_edit.value())
		settings.route_conflict_traffic = self.trafficConsidered_select.currentIndex()

		settings.CPDLC_ACK_timeout = None if self.cpdlcTimeout_edit.value() == 0 \
				else timedelta(seconds=self.cpdlcTimeout_edit.value())
		settings.CPDLC_send_strips_on_accepted_transfers = self.cpdlcXfrAcceptedSendsStrip_tickBox.isChecked()
		settings.CPDLC_raises_windows = self.cpdlcRaiseWindows_tickBox.isChecked()
		settings.CPDLC_closes_windows = self.cpdlcCloseWindows_tickBox.isChecked()

		settings.private_ATC_msg_auto_raise = self.autoAtcChatWindowPopUp_tickBox.isChecked()
		settings.ATC_chatroom_msg_notifications = self.notifyGeneralChatRoomMsg_tickBox.isChecked()
		settings.text_chat_history_time = None if self.textChatMessagesVisibleTime_edit.value() == 0 \
				else timedelta(minutes=self.textChatMessagesVisibleTime_edit.value())
		settings.preset_chat_messages = self.msg_presets_model.stringList()
		
		self.sound_notification_model.applyChoices()
		settings.PTT_mutes_notifications = self.pttMutesSounds_tickBox.isChecked()
		
		signals.generalSettingsChanged.emit()
		self.accept()






# =================================
#
#           L O C A L
#
# =================================

class LocalSettingsDialog(QDialog, Ui_localSettingsDialog):
	#STATIC:
	last_tab_used = 0
	
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.setWindowTitle('%s location settings - %s' % (('CTR' if env.airport_data is None else 'AD'), settings.location_code))
		self.range_group_boxes = [self.range1_groupBox, self.range2_groupBox, self.range3_groupBox, self.range4_groupBox]
		self.range_name_edits = [self.range1_name_edit, self.range2_name_edit, self.range3_name_edit, self.range4_name_edit]
		self.range_lo_edits = [self.range1_lo_edit, self.range2_lo_edit, self.range3_lo_edit, self.range4_lo_edit]
		self.range_hi_edits = [self.range1_hi_edit, self.range2_hi_edit, self.range3_hi_edit, self.range4_hi_edit]
		self.range_col_edits = [self.range1_colourPicker, self.range2_colourPicker, self.range3_colourPicker, self.range4_colourPicker]
		if env.airport_data is None: # CTR session
			self.stripPrinter_groupBox.setEnabled(False)
			self.settings_tabs.removeTab(self.settings_tabs.indexOf(self.AD_tab))
			self.spawnCTR_minFL_edit.valueChanged.connect(self.spawnCTR_minFL_valueChanged)
			self.spawnCTR_maxFL_edit.valueChanged.connect(self.spawnCTR_maxFL_valueChanged)
		else: # AD session
			self.runway_tabs = QTabWidget(self)
			self.runway_tabs.setTabShape(QTabWidget.Triangular)
			self.runway_tabs.setTabPosition(QTabWidget.South)
			for rwy in env.airport_data.allRunways(sortByName=True):
				self.runway_tabs.addTab(RunwayParametersWidget(self, rwy), rwy.name)
			if env.airport_data.transition_altitude is not None:
				self.transitionAltitude_edit.setEnabled(False)
				self.transitionAltitude_edit.setToolTip('Fixed by airport data')
			self.settings_tabs.addTab(self.runway_tabs, 'Runways')
			self.settings_tabs.removeTab(self.settings_tabs.indexOf(self.CTR_tab))
			self.spawnAPP_minFL_edit.valueChanged.connect(self.spawnAPP_minFL_valueChanged)
			self.spawnAPP_maxFL_edit.valueChanged.connect(self.spawnAPP_maxFL_valueChanged)
			self.TWRrangeCeiling_edit.valueChanged.connect(self.TWRrangeCeiling_valueChanged)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.fillFromSettings()
		self.settings_tabs.setCurrentIndex(GeneralSettingsDialog.last_tab_used)
		self.buttonBox.accepted.connect(self.storeSettings)
	
	def spawnAPP_minFL_valueChanged(self, v):
		if v < self.TWRrangeCeiling_edit.value():
			self.TWRrangeCeiling_edit.setValue(v)
		if v > self.spawnAPP_maxFL_edit.value():
			self.spawnAPP_maxFL_edit.setValue(v)
	
	def spawnAPP_maxFL_valueChanged(self, v):
		if v < self.spawnAPP_minFL_edit.value():
			self.spawnAPP_minFL_edit.setValue(v)
	
	def TWRrangeCeiling_valueChanged(self, v):
		if v > self.spawnAPP_minFL_edit.value():
			self.spawnAPP_minFL_edit.setValue(v)
	
	def spawnCTR_minFL_valueChanged(self, v):
		if v > self.spawnCTR_maxFL_edit.value():
			self.spawnCTR_maxFL_edit.setValue(v)
	
	def spawnCTR_maxFL_valueChanged(self, v):
		if v < self.spawnCTR_minFL_edit.value():
			self.spawnCTR_minFL_edit.setValue(v)
		
	def selectSemiCircRule(self, rule):
		radio_button = {
				SemiCircRule.OFF: self.semiCircRule_radioButton_off,
				SemiCircRule.E_W: self.semiCircRule_radioButton_EW,
				SemiCircRule.N_S: self.semiCircRule_radioButton_NS
			}[rule]
		radio_button.setChecked(True)
		
	def selectedSemiCircRule(self):
		if self.semiCircRule_radioButton_off.isChecked(): return SemiCircRule.OFF
		elif self.semiCircRule_radioButton_EW.isChecked(): return SemiCircRule.E_W
		elif self.semiCircRule_radioButton_NS.isChecked(): return SemiCircRule.N_S

	def fillFromSettings(self):
		# Equipment tab
		self.radioDirectionFinding_tickBox.setChecked(settings.radio_direction_finding)
		self.cpdlc_tickBox.setChecked(settings.controller_pilot_data_link)
		self.capability_noSSR_radioButton.setChecked(settings.SSR_mode_capability == '0')
		self.capability_modeA_radioButton.setChecked(settings.SSR_mode_capability == 'A')
		self.capability_modeC_radioButton.setChecked(settings.SSR_mode_capability == 'C')
		self.capability_modeS_radioButton.setChecked(settings.SSR_mode_capability == 'S')
		self.radarHorizontalRange_edit.setValue(settings.radar_range)
		self.radarFloor_edit.setValue(settings.radar_signal_floor_level)
		self.radarUpdateInterval_edit.setValue(int(settings.radar_sweep_interval.total_seconds()))
		self.stripAutoPrint_DEP_tickBox.setChecked(settings.auto_print_strips_include_DEP)
		self.stripAutoPrint_ARR_tickBox.setChecked(settings.auto_print_strips_include_ARR)
		self.stripAutoPrint_ifrOnly_tickBox.setChecked(settings.auto_print_strips_IFR_only)
		self.stripAutoPrint_leadTime_edit.setValue(int(settings.auto_print_strips_anticipation.total_seconds()) // 60)
		# Rules tab
		self.horizontalSeparation_edit.setValue(settings.horizontal_separation)
		self.verticalSeparation_edit.setValue(settings.vertical_separation)
		self.conflictWarningFloorFL_edit.setValue(settings.conflict_warning_floor_FL)
		self.transitionAltitude_edit.setValue(env.transitionAltitude())
		self.uncontrolledVFRcode_edit.setValue(settings.uncontrolled_VFR_XPDR_code)
		self.primaryMetarStation_edit.setText(settings.primary_METAR_station.upper())
		self.radioName_edit.setText(settings.location_radio_name)
		# XPDR ranges tab
		for i, rng in enumerate(settings.XPDR_assignment_ranges[:len(self.range_group_boxes)]):
			self.range_group_boxes[i].setChecked(True)
			self.range_name_edits[i].setText(rng.name)
			self.range_lo_edits[i].setValue(rng.lo)
			self.range_hi_edits[i].setValue(rng.hi)
			self.range_col_edits[i].setChoice(rng.col)
		# Other settings tab
		if env.airport_data is None: # CTR mode
			self.spawnCTR_minFL_edit.setValue(settings.solo_CTR_floor_FL)
			self.spawnCTR_maxFL_edit.setValue(settings.solo_CTR_ceiling_FL)
			self.CTRrangeDistance_edit.setValue(settings.solo_CTR_range_dist)
			self.routingPoints_edit.setText(' '.join(settings.solo_CTR_routing_points))
			self.selectSemiCircRule(settings.solo_CTR_semi_circular_rule)
		else: # AD mode
			self.spawnAPP_minFL_edit.setValue(settings.solo_APP_ceiling_FL_min)
			self.spawnAPP_maxFL_edit.setValue(settings.solo_APP_ceiling_FL_max)
			self.TWRrangeDistance_edit.setValue(settings.solo_TWR_range_dist)
			self.TWRrangeCeiling_edit.setValue(settings.solo_TWR_ceiling_FL)
		self.atisCustomAppendix_edit.setPlainText(settings.ATIS_custom_appendix)
	
	def storeSettings(self):
		## CHECK SETTINGS FIRST
		try:
			new_ranges = []
			for i in range(len(self.range_group_boxes)):
				if self.range_group_boxes[i].isChecked():
					name = self.range_name_edits[i].text()
					if any(rng.name == name for rng in new_ranges):
						raise ValueError('Duplicate range name')
					colour = self.range_col_edits[i].getChoice()
					new_ranges.append(XpdrAssignmentRange(name, self.range_lo_edits[i].value(), self.range_hi_edits[i].value(), colour))
		except ValueError as err:
			QMessageBox.critical(self, 'Assignment range error', str(err))
			return
		if env.airport_data is None:
			try:
				bad = next(p for p in self.routingPoints_edit.text().split() if len(env.navpoints.findAll(code=p)) != 1)
				QMessageBox.critical(self, 'Invalid entry', 'Unknown navpoint or navpoint not unique: %s' % bad)
				return
			except StopIteration:
				pass # no bad navpoints
		
		## ALL SETTINGS OK. Save them and accept the dialog.
		GeneralSettingsDialog.last_tab_used = self.settings_tabs.currentIndex()
		
		if env.airport_data is not None:
			for i in range(self.runway_tabs.count()):
				self.runway_tabs.widget(i).applyToRWY()
		
		settings.radio_direction_finding = self.radioDirectionFinding_tickBox.isChecked()
		settings.controller_pilot_data_link = self.cpdlc_tickBox.isChecked()
		settings.SSR_mode_capability = '0' if self.capability_noSSR_radioButton.isChecked() \
				else 'A' if self.capability_modeA_radioButton.isChecked() \
				else 'C' if self.capability_modeC_radioButton.isChecked() else 'S'
		settings.radar_range = self.radarHorizontalRange_edit.value()
		settings.radar_signal_floor_level = self.radarFloor_edit.value()
		settings.radar_sweep_interval = timedelta(seconds=self.radarUpdateInterval_edit.value())
		settings.auto_print_strips_include_DEP = self.stripAutoPrint_DEP_tickBox.isChecked()
		settings.auto_print_strips_include_ARR = self.stripAutoPrint_ARR_tickBox.isChecked()
		settings.auto_print_strips_IFR_only = self.stripAutoPrint_ifrOnly_tickBox.isChecked()
		settings.auto_print_strips_anticipation = timedelta(minutes=self.stripAutoPrint_leadTime_edit.value())
		
		settings.vertical_separation = self.verticalSeparation_edit.value()
		settings.conflict_warning_floor_FL = self.conflictWarningFloorFL_edit.value()
		settings.location_radio_name = self.radioName_edit.text()
		settings.transition_altitude = self.transitionAltitude_edit.value() # NOTE useless if a TA is set in apt.dat
		settings.uncontrolled_VFR_XPDR_code = self.uncontrolledVFRcode_edit.value()
		settings.primary_METAR_station = self.primaryMetarStation_edit.text().upper()
		settings.horizontal_separation = self.horizontalSeparation_edit.value()
		
		settings.XPDR_assignment_ranges = new_ranges
		
		if env.airport_data is None: # CTR mode
			settings.solo_CTR_floor_FL = self.spawnCTR_minFL_edit.value()
			settings.solo_CTR_ceiling_FL = self.spawnCTR_maxFL_edit.value()
			settings.solo_CTR_range_dist = self.CTRrangeDistance_edit.value()
			settings.solo_CTR_routing_points = self.routingPoints_edit.text().split()
			settings.solo_CTR_semi_circular_rule = self.selectedSemiCircRule()
		else: # AD mode
			settings.solo_APP_ceiling_FL_min = self.spawnAPP_minFL_edit.value() // 10 * 10
			settings.solo_APP_ceiling_FL_max = ((self.spawnAPP_maxFL_edit.value() - 1) // 10 + 1) * 10
			settings.solo_TWR_range_dist = self.TWRrangeDistance_edit.value()
			settings.solo_TWR_ceiling_FL = self.TWRrangeCeiling_edit.value()
		settings.ATIS_custom_appendix = self.atisCustomAppendix_edit.toPlainText()
		
		signals.localSettingsChanged.emit()
		self.accept()




