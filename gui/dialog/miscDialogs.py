
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import re
from math import atan, degrees

from PyQt5.QtCore import Qt, QAbstractTableModel
from PyQt5.QtWidgets import QDialog, QMessageBox, QInputDialog
from PyQt5.QtGui import QIcon

from ui.envInfoDialog import Ui_envInfoDialog
from ui.aboutDialog import Ui_aboutDialog
from ui.cpdlcXfrOptionsDialog import Ui_cpdlcXfrOptionsDialog
from ui.routeSpecsLostDialog import Ui_routeSpecsLostDialog
from ui.discardedStripsDialog import Ui_discardedStripsDialog
from ui.editRackDialog import Ui_editRackDialog

from base.cpdlc import CPDLC_element_display_text
from base.coords import m2NM
from base.strip import rack_detail, runway_box_detail, recycled_detail, shelved_detail, sent_to_detail
from base.util import some

from session.config import settings, version_string
from session.env import env
from session.models.liveStrips import default_rack_name

from ext.xplane import surface_types

from gui.misc import signals, IconFile
from gui.widgets.miscWidgets import RadioKeyEventFilter


# ---------- Constants ----------

version_string_placeholder = '##version##'

# -------------------------------

def yesNo_question(parent, title, text, question=None):
	if question is not None:
		text += '\n' + question
	return QMessageBox.question(parent, title, text) == QMessageBox.Yes


def hostPort_input(parent, title, prompt):
	"""
	returns (host, port) pair, with None host if input cancelled by user.
	"""
	text, ok = QInputDialog.getText(parent, title, prompt)
	while ok:
		split = text.rsplit(':', maxsplit=1)
		if len(split) == 2 and len(split[0]) > 0 and split[1].isdecimal():
			return split[0], int(split[1])
		QMessageBox.critical(parent, title, 'Bad "host:port" format.')
		text, ok = QInputDialog.getText(parent, title, prompt)
	return None, None



class AboutDialog(QDialog, Ui_aboutDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.text_browser.setHtml(re.sub(version_string_placeholder, version_string, self.text_browser.toHtml()))



class RwyInfoTableModel(QAbstractTableModel):
	"""
	CAUTION: Do not build if no airport data
	"""
	def __init__(self, parent):
		QAbstractTableModel.__init__(self, parent)
		self.column_headers = ['RWY', 'LOC freq.', 'GS', 'Surface', 'Orientation', 'Length', 'Width', 'DTHR', 'THR elev.']
		self.runways = env.airport_data.allRunways(sortByName=True)
	
	def headerData(self, section, orientation, role):
		if role == Qt.DisplayRole:
			if orientation == Qt.Horizontal:
				return self.column_headers[section]

	def rowCount(self, parent=None):
		return len(self.runways)

	def columnCount(self, parent=None):
		return len(self.column_headers)

	def data(self, index, role):
		if role == Qt.DisplayRole:
			rwy = self.runways[index.row()]
			col = index.column()
			width, surface = env.airport_data.physicalRunwayData(rwy.physicalRwyIndex())
			if col == 0: # RWY
				return rwy.name
			elif col == 1: # LOC freq.
				txt = some(rwy.LOC_freq, 'none')
				if rwy.ILS_cat is not None:
					txt += ' (%s)' % rwy.ILS_cat
				return txt
			elif col == 2: # GS
				if rwy.hasILS():
					return '%.1f%% / %.1f°' % (rwy.param_FPA, degrees(atan(rwy.param_FPA / 100)))
				else:
					return 'none'
			elif col == 3: # Surface
				return surface_types.get(surface, 'Unknown')
			elif col == 4: # Orientation
				return '%s°' % rwy.orientation().read()
			elif col == 5: # Length
				return '%d m' % (rwy.length(dthr=False) / m2NM)
			elif col == 6: # Width
				return '%d m' % width
			elif col == 7: # DTHR
				return 'none' if rwy.dthr == 0 else '%d m' % rwy.dthr
			elif col == 8: # THR elev.
				return 'N/A' if env.elevation_map is None else '%.1f ft' % env.elevation(rwy.threshold())


class HelipadInfoTableModel(QAbstractTableModel):
	"""
	CAUTION: Do not build if no airport data
	"""
	def __init__(self, parent):
		QAbstractTableModel.__init__(self, parent)
		self.column_headers = ['Helipad', 'Surface', 'Width', 'Elev.']
		self.helipads = sorted(env.airport_data.helipads, key=(lambda r: r.name))
	
	def headerData(self, section, orientation, role):
		if role == Qt.DisplayRole:
			if orientation == Qt.Horizontal:
				return self.column_headers[section]

	def rowCount(self, parent=None):
		return len(self.helipads)

	def columnCount(self, parent=None):
		return len(self.column_headers)

	def data(self, index, role):
		if role == Qt.DisplayRole:
			pad = self.helipads[index.row()]
			col = index.column()
			if col == 0: # Helipad
				return pad.name
			elif col == 1: # Surface
				return surface_types.get(pad.surface, 'Unknown')
			elif col == 2: # Width
				return '%d m' % min(pad.width, pad.length)
			elif col == 3: # Elev.
				return 'N/A' if env.elevation_map is None else '%.1f ft' % env.elevation(pad.centre)


class EnvironmentInfoDialog(QDialog, Ui_envInfoDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.magneticDeclination_infoLabel.setText(some(env.readDeclination(), 'N/A'))
		self.radarPosition_infoLabel.setText(str(env.radarPos()))
		if env.airport_data is None:
			self.airport_tab.setEnabled(False)
		else:
			self.airportName_infoLabel.setText(env.locationName())
			self.airportElevation_infoLabel.setText('%.1f ft' % env.airport_data.field_elevation)
			rwy_table_model = RwyInfoTableModel(self)
			self.runway_tableView.setModel(rwy_table_model)
			if rwy_table_model.rowCount() == 0:
				self.runway_tableView.setVisible(False)
			else:
				for i in range(rwy_table_model.columnCount()):
					self.runway_tableView.resizeColumnToContents(i)
			helipad_table_model = HelipadInfoTableModel(self)
			self.helipad_tableView.setModel(helipad_table_model)
			if helipad_table_model.rowCount() == 0:
				self.helipad_tableView.setVisible(False)
			else:
				for i in range(helipad_table_model.columnCount()):
					self.helipad_tableView.resizeColumnToContents(i)
	
	def showEvent(self, event):
		racked = env.strips.count(lambda s: s.lookup(rack_detail) is not None)
		boxed = env.strips.count(lambda s: s.lookup(runway_box_detail) is not None)
		total = env.strips.count()
		sent = env.discarded_strips.count(lambda s: s.lookup(sent_to_detail) is not None)
		shelved = env.discarded_strips.count(lambda s: s.lookup(shelved_detail))
		self.rackedStrips_infoLabel.setText(str(racked))
		self.looseStrips_infoLabel.setText(str(total - racked - boxed))
		self.boxedStrips_infoLabel.setText(str(boxed))
		self.activeStrips_infoLabel.setText(str(total))
		self.sentStrips_infoLabel.setText(str(sent))
		self.shelvedStrips_infoLabel.setText(str(shelved))
		self.releasedStrips_infoLabel.setText(str(sent + shelved))
		self.xpdrLinkedStrips_infoLabel.setText(str(env.strips.count(lambda s: s.linkedAircraft() is not None)))
		self.fplLinkedStrips_infoLabel.setText(str(env.strips.count(lambda s: s.linkedFPL() is not None)))



class RouteSpecsLostDialog(QDialog, Ui_routeSpecsLostDialog):
	def __init__(self, parent, title, lost_specs_text):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setWindowTitle(title)
		self.lostSpecs_box.setText(lost_specs_text)
	
	def mustOpenStripDetails(self):
		return self.openStripDetailSheet_tickBox.isChecked()



class CpdlcXfrOptionsDialog(QDialog, Ui_cpdlcXfrOptionsDialog):
	def __init__(self, parent, handover_msg_elt):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.contactInstruction_info.setText(CPDLC_element_display_text(handover_msg_elt))
	
	def transferOptionSelected(self):
		return self.cpdlcTransfer_option.isChecked()
	
	def instructionOptionSelected(self):
		return self.contactInstruction_option.isChecked()



class DiscardedStripsDialog(QDialog, Ui_discardedStripsDialog):
	def __init__(self, parent, view_model, dialog_title):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.clear_button.setIcon(QIcon(IconFile.button_clear))
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setWindowTitle(dialog_title)
		self.model = view_model
		self.strip_view.setModel(view_model)
		self.clear_button.clicked.connect(self.model.forgetStrips)
		self.recall_button.clicked.connect(self.recallSelectedStrips)
		self.close_button.clicked.connect(self.accept)
	
	def recallSelectedStrips(self):
		strips = [self.model.stripAt(index) for index in self.strip_view.selectedIndexes()]
		if strips:
			for strip in strips:
				signals.stripRecall.emit(strip)
			self.accept()



class EditRackDialog(QDialog, Ui_editRackDialog):
	def __init__(self, parent, rack_name):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.deleteRack_info.clear()
		self.installEventFilter(RadioKeyEventFilter(self))
		self.initial_rack_name = rack_name
		self.flagged_for_deletion = False
		self.rackName_edit.setText(self.initial_rack_name)
		self.privateRack_tickBox.setChecked(self.initial_rack_name in settings.private_racks)
		self.pickColour_button.setChoice(settings.rack_colours.get(rack_name, None))
		if rack_name == default_rack_name:
			self.rackName_edit.setEnabled(False)
			self.collectedStrips_box.setVisible(False)
			self.deleteRack_button.setEnabled(False)
			self.deleteRack_info.setText('Default rack cannot be deleted')
		else:
			self.collectsFrom_edit.setPlainText('\n'.join(atc for atc, rack in settings.ATC_collecting_racks.items() if rack == rack_name))
			self.collectAutoPrintedStrips_tickBox.setChecked(settings.auto_print_collecting_rack == self.initial_rack_name)
			self.rackName_edit.selectAll()
			self.deleteRack_button.toggled.connect(self.flagRackForDeletion)
		self.buttonBox.rejected.connect(self.reject)
		self.buttonBox.accepted.connect(self.doOK)

	def flagRackForDeletion(self, toggle):
		if toggle:
			if env.strips.count(lambda s: s.lookup(rack_detail) == self.initial_rack_name) > 0:
				QMessageBox.warning(self, 'Non-empty rack deletion', 'Rack not empty. Strips will be reracked if deletion confirmed.')
			self.deleteRack_info.setText('Flagged for deletion')
		else:
			self.deleteRack_info.clear()
	
	def doOK(self):
		if self.deleteRack_button.isChecked():
			for strip in env.strips.listAll():
				if strip.lookup(rack_detail) == self.initial_rack_name:
					strip.writeDetail(recycled_detail, True)
					env.strips.repositionStrip(strip, default_rack_name)
			for atc, rack in list(settings.ATC_collecting_racks.items()):
				if rack == self.initial_rack_name:
					del settings.ATC_collecting_racks[atc]
			if settings.auto_print_collecting_rack == self.initial_rack_name:
				settings.auto_print_collecting_rack = None
			env.strips.removeRack(self.initial_rack_name)
		else: # rack NOT being deleted
			new_name = self.rackName_edit.text()
			# UPDATE SETTINGS
			if new_name != self.initial_rack_name: # renaming
				if env.strips.validNewRackName(new_name):
					env.strips.renameRack(self.initial_rack_name, new_name)
				else:
					QMessageBox.critical(self, 'Rack name error', 'Name is reserved or already used.')
					return # abort
			# private
			if self.initial_rack_name in settings.private_racks:
				settings.private_racks.remove(self.initial_rack_name)
			if self.privateRack_tickBox.isChecked():
				settings.private_racks.add(new_name)
			# colour
			new_colour = self.pickColour_button.getChoice()
			if self.initial_rack_name in settings.rack_colours:
				del settings.rack_colours[self.initial_rack_name]
			if new_colour is not None:
				settings.rack_colours[new_name] = new_colour
			# collecting racks
			for atc, rack in list(settings.ATC_collecting_racks.items()):
				if rack == self.initial_rack_name:
					del settings.ATC_collecting_racks[atc]
			for atc in self.collectsFrom_edit.toPlainText().split('\n'):
				if atc != '':
					settings.ATC_collecting_racks[atc] = new_name
			if self.collectAutoPrintedStrips_tickBox.isChecked(): # should not be ticked if default rack
				settings.auto_print_collecting_rack = new_name
			elif settings.auto_print_collecting_rack == self.initial_rack_name:
				settings.auto_print_collecting_rack = None # back to default if box has been unticked
			# DONE
			signals.rackEdited.emit(self.initial_rack_name, new_name)
		self.accept()
