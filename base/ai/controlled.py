
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from datetime import timedelta
from random import random, choice

from PyQt5.QtCore import QTimer

from base.conflict import ground_separated
from base.coords import EarthCoords
from base.db import take_off_speed, touch_down_speed, stall_speed, maximum_speed, cruise_speed
from base.instr import Instruction
from base.nav import Airfield, NavpointError, world_navpoint_db
from base.params import AltFlSpec, PressureAlt, Speed, distance_travelled, wind_effect
from base.route import Route
from base.text import ChatMessage
from base.util import pop_all, some
from base.ai.status import Status
from base.ai.baseAcft import AbstractAiAcft

from session.config import settings
from session.env import env
from session.manager import SessionType

from gui.misc import signals
from ext.tts import new_voice, speech_str2txt, speech_str2tts, \
		speak_callsign_commercial_flight, speak_callsign_tail_number


# ---------- Constants ----------

pilot_turn_speed = 3 # degrees per second
pilot_vert_speed = 1800 # ft / min
pilot_accel = 3 # kt / s
pilot_hdg_precision = 2 # degrees
pilot_alt_precision = 20 # ft
pilot_spd_precision = 5 # kt
pilot_nav_precision = 1 # NM
pilot_taxi_precision = .002 # NM
pilot_sight_range = 7.5 # NM
pilot_sight_ceiling = PressureAlt.fromFL(100)

fast_turn_factor = 2.5
fast_climb_descend_factor = 1.75
fast_accel_decel_factor = 1.75

cockpit_IAS_reduction_floor = PressureAlt.fromFL(90)
cockpit_IAS_reduction_rate = .2

touch_down_distance_tolerance = .03 # NM
touch_down_height_tolerance = 50 # ft
touch_down_heading_tolerance = 5 # degrees
touch_down_speed_tolerance = 25 # kt
touch_down_speed_drop = 20 # kt
min_clearToLand_height = 50 # ft
taxi_max_turn_without_decel = 5 # degrees
intercept_max_angle = 10 # degrees on each side

taxi_speed = Speed(15)
taxi_turn_speed = Speed(2)
ldg_roll_speed = Speed(25)
minimum_initial_climb_AMSL = 5000 # ft
minimum_initial_climb_ASFC = 2000 # ft

short_final_dist = 4 # NM
inbound_speed_reduce_start_FL = 150
descent_max_speed = Speed(235)
default_turn_off_angle = -60 # degrees
turn_off_choice_prob = .5
approach_angle = 30 # degrees
init_hldg_turn = 120 # degrees
hldg_leg_fly_time = timedelta(minutes=1)
ready_max_dist_to_rwy = .05 # NM
park_max_dist_to_gate_node = .1 # NM

simulated_radio_signal_timeout = 2000 # ms

# -------------------------------



def ck_instr(accept_condition, msg_if_rejected):
	if not accept_condition:
		raise Instruction.Error(msg_if_rejected)


def GS_alt(thr_elev, fpa, dist):
	return PressureAlt.fromAMSL(thr_elev + 60.761 * fpa * dist, qnh=env.QNH())


def default_initial_climb_spec():
	return AltFlSpec(False, max(minimum_initial_climb_AMSL,
								minimum_initial_climb_ASFC + int(env.airport_data.field_elevation / 1000 + 1) * 1000))



class ControlledAircraft(AbstractAiAcft):
	"""
	This class represents an AI aircraft in radio contact (controlled),
	usually with intentions (goal) unless acting as teacher.
	"""
	
	def __init__(self, callsign, acft_type, init_params, goal):
		"""
		goal parameter is:
		 - bool if and only if landing at base airport (True=ILS; False=visual)
		 - str if and only if requesting a parking position
		 - tuple (Navpoint, cruise alt/lvl, Airfield) if outbound: to be brought to navpoint and FL (either can be None if don't matter), AD is destination
		 - Airfield (destination) if it is transiting through airspace
		 - None if none of the above (teacher aircraft)
		"""
		if cruise_speed(acft_type) is None:
			raise ValueError('Aborting ControlledAircraft construction: unknown cruise speed for %s' % acft_type)
		AbstractAiAcft.__init__(self, callsign, acft_type, init_params)
		self.pilot_voice = new_voice() if settings.session_manager.session_type == SessionType.SOLO else None
		self.goal = goal
		self.touch_and_go_on_LDG = False # set by teacher panel
		self.skid_off_RWY_on_LDG = False # set by teacher panel
		self.instructions = []
	
	
	## GENERAL ACCESS METHODS
	
	def pilotVoice(self):
		return self.pilot_voice
	
	def isInboundGoal(self):
		return isinstance(self.goal, bool) or isinstance(self.goal, str)
	
	def isOutboundGoal(self):
		return isinstance(self.goal, tuple)
	
	def wantsToPark(self):
		return isinstance(self.goal, str)
	
	def wantsVisualApp(self):
		return self.goal is False # keep strict equality: self.goal not necessarily a bool
	
	def wantsILS(self):
		return self.goal is True # keep strict equality: self.goal not necessarily a bool
	
	def canPark(self):
		if self.wantsToPark() and env.airport_data is not None: # wants a gate/pkpos
			pkg_pos = env.airport_data.ground_net.parkingPosition(self.goal)
			return self.params.position.distanceTo(pkg_pos) <= park_max_dist_to_gate_node
		else:
			return False
	
	def isReadyForDeparture(self):
		return self.statusType() in [Status.READY, Status.LINED_UP]
	
	def instrOfType(self, t):
		"""
		returns the instruction of given type, or None
		"""
		return next((i for i in self.instructions if i.type == t), None)
	
	def groundPointInSight(self, point):
		return self.params.altitude.diff(pilot_sight_ceiling) <= 0 \
			and self.params.position.distanceTo(point) <= pilot_sight_range
	
	def maxTurn(self, td):
		return pilot_turn_speed * td.total_seconds()
	
	def maxClimb(self, td):
		return pilot_vert_speed * td.total_seconds() / 60
	
	def maxSpdIncr(self, td):
		return pilot_accel * td.total_seconds()
	
	
	
	## TICKING
	
	def doTick(self):
		#DEBUGprint(self.identifier, '(%s)' % self.params.status, ', '.join(str(i) for i in self.instructions))
		for instr in self.instructions:
			self.followInstruction(instr)
		if not self.isGroundStatus(): # control horiz. displacement
			if self.instrOfType(Instruction.VECTOR_SPD) is None: # see if we still want (or have) to change speed
				if self.params.ias.diff(Speed(250)) >= 0 and self.params.altitude.diff(PressureAlt.fromFL(100)) <= 0:
					self.accelDecelTowards(Speed(250), fast=True) # enforce speed restriction under FL100
				elif self.isInboundGoal():
					if self.statusType() != Status.LANDING: # landing status is covered by approachDescent method
						decel_for_descent = self.params.altitude.FL() <= inbound_speed_reduce_start_FL
						if not decel_for_descent:
							instr = self.instrOfType(Instruction.VECTOR_ALT)
							decel_for_descent = instr is not None and env.pressureAlt(instr.arg).FL() <= inbound_speed_reduce_start_FL
						if decel_for_descent:
							self.accelDecelTowards(descent_max_speed, accelOK=False) # we may already be slower
				else:
					self.accelDecelTowards(cruise_speed(self.aircraft_type))
			if self.params.altitude.diff(cockpit_IAS_reduction_floor) < 0:
				eq_alt = self.params.altitude
			else: # reduce IAS-TAS diff. for less crazy speeds (IRL, IAS is set lower than cruise speed at high levels)
				eq_alt = cockpit_IAS_reduction_floor + cockpit_IAS_reduction_rate * self.params.altitude.diff(cockpit_IAS_reduction_floor)
			tas = self.params.ias.ias2tas(eq_alt)
			w = env.primaryWeather()
			wind_info = None if w is None else w.mainWind()
			if self.statusType() == Status.LANDING or wind_info is None or wind_info[0] is None:
				course = self.params.heading
				ground_speed = tas
			else: # WARNING: unit "kt" assumed for wind speed
				course, ground_speed = wind_effect(self.params.heading, tas, wind_info[0], Speed(wind_info[1]))
			self.params.position = self.params.position.moved(course, distance_travelled(self.tick_interval, ground_speed))
		pop_all(self.instructions, self.instructionDone)
	
	
	# # # # # # # # # # # # #
	#     INSTRUCTIONS      #
	# # # # # # # # # # # # #
	
	## This is where the conditions are given for getting rid of instructions in ACFT instr. lists
	def instructionDone(self, instr):
		if instr.type in [Instruction.VECTOR_HDG, Instruction.VECTOR_ALT, Instruction.VECTOR_SPD]:
			return False
		elif instr.type == Instruction.VECTOR_DCT: # navpoint already resolved
			return self.params.position.distanceTo(instr.add_data['resolved_navpoint'].coordinates) <= pilot_nav_precision
		elif instr.type == Instruction.FOLLOW_ROUTE: # route already resolved
			rte = instr.add_data['resolved_route']
			return self.params.position.distanceTo(rte.waypoint(rte.legCount() - 1).coordinates) <= pilot_nav_precision
		elif instr.type == Instruction.CANCEL_VECTOR_SPD:
			return self.instrOfType(Instruction.VECTOR_SPD) is None
		elif instr.type == Instruction.HOLD:
			return False
		elif instr.type == Instruction.SQUAWK:
			return self.params.XPDR_code == instr.arg
		elif instr.type == Instruction.CANCEL_APP:
			return self.statusType() == Status.AIRBORNE and self.instrOfType(Instruction.CLEARED_TO_LAND) is None \
					and self.instrOfType(Instruction.CLEARED_APP) is None
		elif instr.type == Instruction.INTERCEPT_NAV:
			return False
		elif instr.type == Instruction.INTERCEPT_LOC:
			return self.instrOfType(Instruction.CLEARED_APP) is not None
		elif instr.type == Instruction.EXPECT_RWY:
			return self.statusType() in [Status.RWY_LDG, Status.READY]
		elif instr.type == Instruction.TAXI:
			return len(instr.arg) == 0 and instr.arg2 is None
		elif instr.type == Instruction.HOLD_POSITION:
			return self.params.ias.diff(Speed(0)) == 0
		elif instr.type == Instruction.CLEARED_APP:
			return self.statusType() == Status.LANDING
		elif instr.type == Instruction.CLEARED_TO_LAND:
			return self.statusType() in [Status.TAXIING, Status.RWY_TKOF] # RWY cleared or performing touch-and-go
		elif instr.type == Instruction.LINE_UP:
			return self.statusType() == Status.LINED_UP
		elif instr.type == Instruction.CLEARED_TKOF:
			return self.statusType() == Status.AIRBORNE
		elif instr.type == Instruction.HAND_OVER:
			return self.released
		elif instr.type == Instruction.SAY_INTENTIONS: # This is followed in the read-back, so must be finished by now.
			return True
		else:
			assert False, 'instructionDone: unknown instruction %s' % instr
	
	## This is where instruction is given, possibly rejected by ACFT if makes no sense
	def instruct(self, instructions, read_back):
		"""
		instructions might replace some in place, but they ALWAYS end up in the list if no error is generated
		"""
		backup = self.instructions[:]
		try:
			for instr in instructions:
				self.ingestInstruction(instr) # this modifies self.instructions if the instr is not rejected
			self.instructions.extend(instructions) # no instruction error raised; ADD instructions to list
			if read_back:
				self.readBack(instructions)
		except Instruction.Error as exn:
			self.instructions = backup
			if read_back:
				self.say('Unable. %s' % exn, True)
			raise exn
	
	## This is where AI pilot does something about an instruction of his list that he must follow
	def followInstruction(self, instr):
		if instr.type == Instruction.VECTOR_HDG:
			self.turnTowards(instr.arg, tolerance=pilot_hdg_precision, rightTurn=instr.arg2)
			
		elif instr.type == Instruction.VECTOR_DCT: # navpoint prior resolved when instruction ingested
			self.flyTowards(instr.add_data['resolved_navpoint'].coordinates)
			
		elif instr.type == Instruction.VECTOR_ALT:
			if not self.isGroundStatus():
				self.climbDescendTowards(env.pressureAlt(instr.arg))
			
		elif instr.type == Instruction.VECTOR_SPD:
			self.accelDecelTowards(instr.arg)
			
		elif instr.type == Instruction.FOLLOW_ROUTE:
			self.flyTowards(instr.add_data['resolved_route'].currentWaypoint(self.params.position).coordinates)
			
		elif instr.type == Instruction.CANCEL_VECTOR_SPD:
			pass # this instruction is immediately performed on "instruct"
		
		elif instr.type == Instruction.HOLD: # navpoint prior resolved when instruction ingested
			if self.statusType() != Status.HLDG:
				self.params.status = Status(Status.HLDG, arg=None)
			if self.params.status.arg is None: # going for fix
				if self.params.position.distanceTo(instr.add_data['resolved_navpoint'].coordinates) <= pilot_nav_precision: # got there
					hldg_hdg = self.params.heading + init_hldg_turn
					self.params.status = Status(Status.HLDG, arg=(hldg_hdg, hldg_leg_fly_time))
				else:
					self.flyTowards(instr.add_data['resolved_navpoint'].coordinates)
			else: # in the loop
				hldg_hdg, outbound_ttf = self.params.status.arg
				if outbound_ttf > timedelta(0): # flying outbound leg
					if self.params.heading.diff(hldg_hdg, tolerance=pilot_hdg_precision) == 0:
						self.params.status.arg = hldg_hdg, self.params.status.arg[1] - self.tick_interval
					else:
						self.turnTowards(hldg_hdg, tolerance=pilot_hdg_precision, rightTurn=True)
				else: # flying inbound leg
					self.flyTowards(instr.add_data['resolved_navpoint'].coordinates)
					if self.params.position.distanceTo(instr.add_data['resolved_navpoint'].coordinates) <= pilot_nav_precision:
						self.params.status.arg = hldg_hdg, hldg_leg_fly_time
			
		elif instr.type == Instruction.SQUAWK:
			self.params.XPDR_code = instr.arg
			
		elif instr.type == Instruction.CANCEL_APP:
			self.MISAP()
				
		elif instr.type == Instruction.INTERCEPT_NAV: # navpoint prior resolved when instruction ingested
			self.intercept(instr.add_data['resolved_navpoint'].coordinates, instr.arg2) # FUTURE navaid intercept range limit?
				
		elif instr.type == Instruction.INTERCEPT_LOC:
			rwy = env.airport_data.runway(self.instrOfType(Instruction.EXPECT_RWY).arg)
			self.intercept(rwy.threshold(dthr=True), rwy.appCourse(), rangeLimit=rwy.LOC_range) # FUTURE limit to/from intercept
			
		elif instr.type == Instruction.EXPECT_RWY:
			rwy = env.airport_data.runway(instr.arg)
			if self.isGroundStatus():
				if self.instrOfType(Instruction.TAXI) is None and self.statusType() != Status.RWY_TKOF: # should we report ready?
					thr = rwy.threshold(dthr=False)
					rwy_limit = thr.moved(rwy.orientation(), rwy.length() / 2) # FUTURE roll-off dist depending on ACFT type
					if self.params.position.toRadarCoords().isBetween(thr.toRadarCoords(), rwy_limit.toRadarCoords(), ready_max_dist_to_rwy, offsetBeyondEnds=True):
						self.params.status = Status(Status.READY, arg=instr.arg)
						self.say('Short of \\RWY{%s}, ready for departure.' % rwy.name, False)
			else:
				td_point = rwy.threshold(dthr=True)
				if self.statusType() == Status.LANDING:
					self.intercept(td_point, rwy.appCourse(), tolerant=False, force=True)
					if self.instrOfType(Instruction.CANCEL_APP) is None:
						self.approachDescent(rwy)
				elif self.wantsVisualApp() and not self.params.runway_reported_in_sight and self.groundPointInSight(td_point):
					# Start visual approach
					self.say('Runway \\RWY{%s} in sight.' % rwy.name, False)
					self.params.runway_reported_in_sight = True
		
		elif instr.type == Instruction.TAXI:
			if self.statusType() != Status.RWY_LDG: # Other on-ground status with RWY argument
				self.params.status = Status(Status.TAXIING)
			if len(instr.arg) > 0: # still got nodes to taxi
				next_target = env.airport_data.ground_net.nodePosition(instr.arg[0])
			elif instr.arg2 is not None: # final pkg pos
				next_target = env.airport_data.ground_net.parkingPosition(instr.arg2)
			else: # no taxi goal left
				next_target = None
			if next_target is not None:
				if self.taxiTowardsReached(next_target):
					if len(instr.arg) == 0:
						instr.arg2 = None
					else: # still got nodes to taxi
						del instr.arg[0]
						if self.canPark() and len(instr.arg) == 0:
							self.say('Request contact with ramp.', False)
			
		elif instr.type == Instruction.HOLD_POSITION:
			self.params.ias = Speed(0)
			
		elif instr.type == Instruction.CLEARED_APP:
			rwy = env.airport_data.runway(self.instrOfType(Instruction.EXPECT_RWY).arg)
			if self.params.runway_reported_in_sight \
					or self.intercept(rwy.threshold(dthr=True), rwy.appCourse(), tolerant=False, rangeLimit=rwy.LOC_range):
				pop_all(self.instructions, lambda i: i.type == Instruction.VECTOR_ALT)
				self.params.status = Status(Status.LANDING, arg=rwy.name)
			
		elif instr.type == Instruction.LINE_UP:
			if self.statusType() == Status.READY:
				rwy = env.airport_data.runway(self.params.status.arg)
				rwy_end = rwy.opposite().threshold(dthr=False)
				minimum_roll_dist = rwy.length() * 2 / 3 # FUTURE roll-off dist depending on ACFT type
				gn = env.airport_data.ground_net
				line_up_nodes = gn.nodes(lambda n: gn.nodeIsInSourceData(n) and gn.nodeIsOnRunway(n, rwy.name)
						and gn.nodePosition(n).distanceTo(rwy_end) >= minimum_roll_dist)
				if len(line_up_nodes) > 0:
					line_up_point = min((gn.nodePosition(n) for n in line_up_nodes), key=self.params.position.distanceTo)
				else: # no nodes; choose a direct point on RWY
					rwy_dthr = rwy.threshold(dthr=True)
					rcoords_me = self.params.position.toRadarCoords()
					rcoords_dthr = rwy_dthr.toRadarCoords()
					rcoords_end = rwy_end.toRadarCoords()
					if rcoords_me.isBetween(rcoords_dthr, rcoords_end, ready_max_dist_to_rwy + 1, offsetBeyondEnds=False):
						line_up_point = EarthCoords.fromRadarCoords(rcoords_me.orthProj(rcoords_dthr, rcoords_end))
					else: # ACFT is behind the DTHR (not between RWY ends)
						line_up_point = rwy_dthr.moved(rwy.orientation(), .03)
				if self.taxiTowardsReached(line_up_point):
					hdg = rwy.orientation()
					if self.params.heading.diff(hdg, tolerance=.01) != 0:
						self.turnTowards(hdg, fastOK=True)
					else:
						pop_all(self.instructions, lambda i: i.type == Instruction.LINE_UP)
						self.params.status.type = Status.LINED_UP
			
		elif instr.type == Instruction.CLEARED_TKOF:
			if self.statusType() == Status.LINED_UP:
				self.params.status.type = Status.RWY_TKOF
			elif self.statusType() == Status.RWY_TKOF:
				self.taxiForward()
				self.params.ias += self.maxSpdIncr(self.tick_interval)
				if self.params.ias.diff(take_off_speed(self.aircraft_type)) >= 0:
					self.params.status = Status(Status.AIRBORNE)
					if self.instrOfType(Instruction.VECTOR_ALT) is None:
						self.instructions.append(Instruction(Instruction.VECTOR_ALT, arg=default_initial_climb_spec()))
			
		elif instr.type == Instruction.CLEARED_TO_LAND and self.statusType() == Status.RWY_LDG and self.params.RWY_excursion_stage != 2:
			# 3 stages after touch down (stage 1 for all planes; then either skid off RWY or stages 2-3 for normal LDG):
			#   1. slow down (speed > ldg_roll_speed)
			#   2. LDG roll until turn-off point ahead if any (speed == ldg_roll_speed)
			#   3. turn/taxi off RWY (speed == taxi_speed)
			#      (3a) following taxi routes if possible (a TAXI instruction is present)
			#      (3b) "into the wild" if no ground net (no TAXI instruction present)
			if self.instrOfType(Instruction.TAXI) is None: # A taxi instruction (from a stage 3b) will take care of forward move
				self.taxiForward()
			rwy = env.airport_data.runway(self.params.status.arg)
			if self.params.ias.diff(ldg_roll_speed) > 0: # In stage 1
				self.accelDecelTowards(ldg_roll_speed, fast=True, tol=0)
			elif self.params.RWY_excursion_stage != 0:
				if self.params.RWY_excursion_stage == 1: # not finished
					finish_heading = rwy.orientation() + default_turn_off_angle
					self.turnTowards(finish_heading, fastOK=True)
					if self.params.heading.diff(finish_heading, tolerance=.1) == 0: # just finished skidding
						self.params.RWY_excursion_stage = 2
						self.say('Oops!', False)
			else:
				turning_off = into_the_wild = False # init
				roll_dist = rwy.threshold().distanceTo(self.params.position)
				l1, l2, l3, l4 = env.airport_data.ground_net.runwayTurnOffs(rwy, minroll=roll_dist)
				fwd_turn_offs_available = l1 if l1 != [] else l2
				if self.instrOfType(Instruction.TAXI) is not None: # In stage 3a
					turning_off = True
				elif len(fwd_turn_offs_available) == 0:
					if len(l3) == 0: # No turn-off ahead + no possible backtrack. Going to stage (3b).
						turning_off = into_the_wild = True
					else: # Must stop, ACFT will need a backtrack
						self.accelDecelTowards(Speed(0), fast=(roll_dist > rwy.length(dthr=True) * 3 / 4), tol=0)
						if self.params.ias.diff(Speed(0)) == 0:
							self.params.status = Status(Status.TAXIING) # This finishes the instruction
							self.say('Requesting backtrack runway \\RWY{%s}' % rwy.name, False)
				else: # In stage 2
					next_turn_off_point = env.airport_data.ground_net.nodePosition(fwd_turn_offs_available[0][0])
					#DEBUGprint('Found node %s for turn off onto %s, distance %f' % (fwd_turn_offs_available[0][0], fwd_turn_offs_available[0][1], next_turn_off_point.distanceTo(env.airport_data.ground_net.nodePosition(fwd_turn_offs_available[0][1]))))
					if self.params.position.distanceTo(next_turn_off_point) < 2 * pilot_taxi_precision: # chance to turn off
						if len(fwd_turn_offs_available) == 1 or random() < turn_off_choice_prob: # turn off RWY! going to stage 3a
							self.instructions.append(Instruction(Instruction.TAXI, arg=[fwd_turn_offs_available[0][1]]))
				if turning_off: # In stage 3
					if into_the_wild:
						if self.params.ias.diff(taxi_speed) > .1: # slow down first
							self.accelDecelTowards(taxi_speed, fast=True, tol=0)
						else:
							finish_heading = rwy.orientation() + default_turn_off_angle
							self.turnTowards(finish_heading, fastOK=True)
							turning_off = self.params.heading.diff(finish_heading, tolerance=.1) != 0
					else:
						turning_off = self.instrOfType(Instruction.TAXI) is None # reached RWY cleared point
					if not turning_off: # turn-off finished
						self.params.ias = Speed(0)
						if settings.session_manager.session_type == SessionType.SOLO and settings.solo_role_GND:
							self.goal = choice(env.airport_data.ground_net.parkingPositions(acftType=self.aircraft_type))
							pkinfo = env.airport_data.ground_net.parkingPosInfo(self.goal)
							self.say('Runway \\RWY{%s} clear. Requesting taxi to %s \\SPELL_ALPHANUMS{%s}.' %
												(rwy.name, pkinfo[2], self.goal), False)
						else:
							self.say('Runway \\RWY{%s} clear.' % rwy.name, False)
						self.params.status = Status(Status.TAXIING) # This finishes the instruction
			
		elif instr.type == Instruction.SAY_INTENTIONS:
			pass # This is followed on read-back. Nothing to do at this point.
			
		elif instr.type == Instruction.HAND_OVER:
			link = env.cpdlc.liveDataLink(self.identifier)
			if link is not None:
				link.terminate(False)
			self.released = True
	
	
	## INSTRUCTION CHECKING
	
	def ingestInstruction(self, instr):
		if instr.type in [Instruction.VECTOR_DCT, Instruction.HOLD, Instruction.INTERCEPT_NAV]: # arg is navpoint name
			try:
				instr.add_data['resolved_navpoint'] = env.navpoints.findClosest(env.radarPos(), code=instr.arg)
			except NavpointError as err:
				raise Instruction.Error('Cannot identify \\NAVPOINT{%s}' % err)
		elif instr.type == Instruction.FOLLOW_ROUTE: # arg is a string (route to go), optionally containing past (DEP) spec
			# ensure destination if possible (never mind duplicating it)
			if self.isInboundGoal():
				dest = env.airport_data.navpoint
			elif self.isOutboundGoal():
				dest = self.goal[2]
			elif isinstance(self.goal, Airfield): # transiting in solo CTR session
				dest = self.goal
			else: # we need to get a destination token from the end of the route string
				rsplit = instr.arg.rsplit(maxsplit=1)
				if len(rsplit) == 0:
					raise Instruction.Error('Empty route string.')
				try:
					dest = world_navpoint_db.fromSpec(rsplit[-1])
				except NavpointError as err:
					raise Instruction.Error('Could not interpret route destination %s' % err)
			instr.add_data['resolved_route'] = Route(self.params.position, dest, instr.arg)
		
		if instr.type in [Instruction.VECTOR_HDG, Instruction.VECTOR_DCT, Instruction.FOLLOW_ROUTE]:
			if instr.type == Instruction.VECTOR_DCT:
				ck_instr(self.params.position.distanceTo(instr.add_data['resolved_navpoint'].coordinates) > pilot_nav_precision,
						'Already at %s.' % instr.add_data['resolved_navpoint'].code)
			ck_instr(self.instrOfType(Instruction.CLEARED_APP) is None,
					'Already cleared for approach. Should I cancel clearance?')
			ck_instr(self.statusType() in [Status.AIRBORNE, Status.HLDG], 'Sorry, not a time for vectors.')
			pop_all(self.instructions, lambda i: i.type in [Instruction.VECTOR_HDG, Instruction.VECTOR_DCT,
					Instruction.INTERCEPT_NAV, Instruction.INTERCEPT_LOC, Instruction.FOLLOW_ROUTE, Instruction.HOLD])
			if self.statusType() == Status.HLDG:
				self.params.status = Status(Status.AIRBORNE)
			
		elif instr.type == Instruction.VECTOR_ALT:
			if self.isGroundStatus(): # instr = initial climb
				ck_instr(not self.isInboundGoal(), 'Not outbound.')
			else: # instr = climb/descend
				ck_instr(self.instrOfType(Instruction.CLEARED_APP) is None, 'Already cleared for approach. Should I cancel clearance?')
			pop_all(self.instructions, lambda i: i.type == Instruction.VECTOR_ALT)
			
		elif instr.type == Instruction.VECTOR_SPD:
			ck_instr(not self.isGroundStatus(), 'Not airborne.')
			if self.statusType() == Status.LANDING:
				ck_instr(self.params.position.distanceTo(env.airport_data.navpoint.coordinates) > short_final_dist, 'On short final.')
			ck_instr(instr.arg.diff(stall_speed(self.aircraft_type)) >= 0, 'Speed is too low.')
			ck_instr(instr.arg.diff(maximum_speed(self.aircraft_type)) <= 0, 'Cannot reach such speed.')
			pop_all(self.instructions, lambda i: i.type == Instruction.VECTOR_SPD)
			
		elif instr.type == Instruction.CANCEL_VECTOR_SPD:
			ck_instr(not self.isGroundStatus(), 'Not airborne.')
			pop_all(self.instructions, lambda i: i.type in [Instruction.CANCEL_VECTOR_SPD, Instruction.VECTOR_SPD])
			
		elif instr.type == Instruction.INTERCEPT_NAV:
			ck_instr(self.statusType() == Status.AIRBORNE and self.instrOfType(Instruction.CLEARED_APP) is None,
					'Sorry, not a time for vectors.')
			pop_all(self.instructions, lambda i: i.type in [Instruction.INTERCEPT_NAV, Instruction.INTERCEPT_LOC])
			
		elif instr.type == Instruction.INTERCEPT_LOC:
			ck_instr(not self.isGroundStatus(), 'Not airborne!')
			ck_instr(self.statusType() != Status.LANDING, 'Already landing.')
			ck_instr(self.statusType() != Status.HLDG, 'Still on hold.')
			self.ckVoiceInstrAndEnsureRwy(instr)
			ck_instr(self.instrOfType(Instruction.CLEARED_APP) is None, 'Already cleared for approach. Should I cancel clearance?')
			ck_instr(not self.wantsVisualApp(), 'Requesting visual approach.')
			pop_all(self.instructions, lambda i: i.type in [Instruction.INTERCEPT_NAV, Instruction.INTERCEPT_LOC])
			
		elif instr.type == Instruction.HOLD:
			ck_instr(self.statusType() in [Status.AIRBORNE, Status.HLDG], 'Sorry, not a time to hold.')
			pop_all(self.instructions, lambda i: i.type in [Instruction.VECTOR_HDG, Instruction.VECTOR_DCT,
					Instruction.INTERCEPT_NAV, Instruction.INTERCEPT_LOC, Instruction.FOLLOW_ROUTE, Instruction.HOLD])
			
		elif instr.type == Instruction.SQUAWK:
			pop_all(self.instructions, lambda i: i.type == Instruction.SQUAWK)
			
		elif instr.type == Instruction.CANCEL_APP:
			ck_instr(self.statusType() == Status.LANDING or self.instrOfType(Instruction.CLEARED_APP) is not None, 'Not on approach.')
			pop_all(self.instructions, lambda i: i.type == Instruction.CANCEL_APP)
			
		elif instr.type == Instruction.LINE_UP:
			ck_instr(self.statusType() == Status.READY, 'Not ready.')
			if instr.isVoiceRecognised():
				str_voice_rwys = instr.add_data['voice_rwy']
				if str_voice_rwys != '':
					ck_instr(str_voice_rwys == self.params.status.arg,
							'Ready for departure from \\RWY{%s}. Wrong runway?' % self.params.status.arg)
			pop_all(self.instructions, lambda i: i.type in [Instruction.LINE_UP, Instruction.CLEARED_TKOF])
			
		elif instr.type == Instruction.CLEARED_TKOF:
			ck_instr(self.statusType() in [Status.READY, Status.LINED_UP], 'Not waiting for departure.')
			if instr.isVoiceRecognised():
				str_voice_rwys = instr.add_data['voice_rwy']
				if str_voice_rwys != '':
					ck_instr(str_voice_rwys == self.params.status.arg,
							'Ready for departure from \\RWY{%s}. Wrong runway?' % self.params.status.arg)
			if self.statusType() == Status.READY:
				self.instructions.append(Instruction(Instruction.LINE_UP, arg=self.params.status.arg))
			pop_all(self.instructions, lambda i: i.type == Instruction.CLEARED_TKOF)
			
		elif instr.type == Instruction.EXPECT_RWY:
			ck_instr(env.airport_data is not None and instr.arg in env.airport_data.runwayNames(), 'Which runway??')
			if self.isGroundStatus():
				ck_instr(self.statusType() != Status.RWY_TKOF, 'Already taking off.')
				ck_instr(not self.isInboundGoal(), 'Not requesting departure.')
			else: # Not on ground
				ck_instr(not self.isOutboundGoal(), 'Outbound.')
				ck_instr(self.statusType() != Status.LANDING, 'Already landing.')
				ck_instr(self.instrOfType(Instruction.CLEARED_APP) is None, 'Already cleared for approach. Should I cancel clearance?')
				if instr.isVoiceRecognised(): # cannot be teacher mode
					app = instr.add_data['voice_app']
					ck_instr(app is None or app == self.wantsILS(), 'Requesting %s approach.' % self.ttsAppWanted())
				if self.wantsILS(): # RWY exists, requires ILS-capable
					ck_instr(env.airport_data.runway(instr.arg).hasILS(), 'Runway %s has no \\SPLIT_CHARS{ILS}.' % instr.arg)
			pop_all(self.instructions, lambda i: i.type in [Instruction.EXPECT_RWY, Instruction.INTERCEPT_LOC])
			self.params.runway_reported_in_sight = False
		
		elif instr.type == Instruction.TAXI:
			ck_instr(self.isGroundStatus(), 'Currently airborne!')
			ck_instr(self.statusType() != Status.RWY_TKOF, 'Already taking off.')
			pop_all(self.instructions, lambda i: i.type in [Instruction.TAXI, Instruction.HOLD_POSITION])
		
		elif instr.type == Instruction.HOLD_POSITION:
			ck_instr(self.statusType() != Status.RWY_TKOF, 'Already taking off.')
			ck_instr(self.statusType() in [Status.TAXIING, Status.READY, Status.LINED_UP], 'Not taxiing.')
			pop_all(self.instructions, lambda i: i.type in
					[Instruction.HOLD_POSITION, Instruction.LINE_UP, Instruction.CLEARED_TKOF, Instruction.TAXI])
			
		elif instr.type == Instruction.CLEARED_APP:
			ck_instr(not self.isGroundStatus(), 'Not airborne!')
			ck_instr(self.statusType() != Status.LANDING, 'Already landing.')
			ck_instr(self.statusType() != Status.HLDG, 'Still on hold.')
			self.ckVoiceInstrAndEnsureRwy(instr)
			if settings.session_manager.session_type != SessionType.TEACHER:
				if instr.isVoiceRecognised():
					app = instr.add_data['voice_app']
					ck_instr(app is None or app == self.wantsILS(), 'Requesting %s approach.' % self.ttsAppWanted())
				ck_instr(not self.wantsVisualApp() or self.params.runway_reported_in_sight, 'Runway not in sight yet.')
			pop_all(self.instructions, lambda i: i.type == Instruction.CLEARED_APP)
			
		elif instr.type == Instruction.CLEARED_TO_LAND:
			ck_instr(settings.session_manager.session_type == SessionType.TEACHER or settings.solo_role_TWR,
					'Only \\ATC{TWR} can issue this instruction.')
			ck_instr(self.statusType() == Status.LANDING, 'Not correctly on final.')
			got_expect_runway = self.instrOfType(Instruction.EXPECT_RWY)
			ck_instr(got_expect_runway is not None, 'REPORT BUG! Landing without a runway :-(')
			if instr.isVoiceRecognised():
				voice_rwys = instr.add_data['voice_rwy']
				if voice_rwys != '':
					ck_instr(voice_rwys == got_expect_runway.arg, 'Established on final for \\RWY{%s}.' % got_expect_runway.arg)
			pop_all(self.instructions, lambda i: i.type == Instruction.CLEARED_TO_LAND)
			
		elif instr.type == Instruction.HAND_OVER:
			if settings.session_manager.session_type == SessionType.SOLO:
				ck_instr(settings.session_manager.handoverGuard(self, instr.arg) is None, 'Staying with you.')
			pop_all(self.instructions, lambda i: i.type == Instruction.HAND_OVER)
	
	def ckVoiceInstrAndEnsureRwy(self, instr):
		if instr.isVoiceRecognised():
			voice_rwys = instr.add_data['voice_rwy']
			if voice_rwys != '':
				expect_instr = self.instrOfType(Instruction.EXPECT_RWY)
				if expect_instr is None: # was not expecting a runway; is one included in this voice instruction?
					self.instruct([Instruction(Instruction.EXPECT_RWY, arg=voice_rwys)], False) # if more than one listed here, name will be rejected
				else: # already expecting a runway; if one is included here, it should match
					ck_instr(voice_rwys == expect_instr.arg, 'Expecting runway \\RWY{%s}. Cancel this approach?' % expect_instr.arg)
		ck_instr(self.instrOfType(Instruction.EXPECT_RWY) is not None, 'No runway given.')
	
	
	## AUXILIARY METHODS FOR INSTRUCTION FOLLOWING
	
	def intercept(self, point, hdg, tolerant=True, force=False, rangeLimit=None):
		dct = self.params.position.toRadarCoords().headingTo(point.toRadarCoords())
		opp = dct.opposite()
		interception = abs(hdg.diff(opp)) <= intercept_max_angle or abs(hdg.opposite().diff(opp)) <= intercept_max_angle # angle
		if rangeLimit is not None:
			interception &= self.params.position.distanceTo(point) <= rangeLimit
		if interception or force: # in LOC/QDM/QDR cone
			pop_all(self.instructions, lambda i: i.type in [Instruction.VECTOR_HDG, Instruction.VECTOR_DCT, Instruction.FOLLOW_ROUTE])
			diff = dct.diff(hdg)
			delta = diff if abs(diff) < 90 else opp.diff(hdg)
			tol = pilot_hdg_precision if tolerant else 0
			self.turnTowards(hdg + delta * approach_angle / intercept_max_angle, tolerance=tol, fastOK=True)
		return interception
		
	def approachDescent(self, runway):
		touch_down_point = runway.threshold(dthr=True)
		touch_down_dist = self.params.position.distanceTo(touch_down_point)
		touch_down_elev = env.elevation(touch_down_point)
		# NOTE: Line above assumes XPDR in the wheels. OK for radar; live FGMS pos packet corrected if FGFS model height is known.
		gs_diff = self.params.altitude.diff(GS_alt(touch_down_elev, runway.param_FPA, touch_down_dist), tolerance=0)
		if gs_diff > 0: # must descend
			drop = min(fast_climb_descend_factor * self.maxClimb(self.tick_interval), gs_diff)
			self.params.altitude = self.params.altitude - drop
		on_short = touch_down_dist <= short_final_dist
		if on_short:
			pop_all(self.instructions, lambda i: i.type == Instruction.VECTOR_SPD)
		if self.instrOfType(Instruction.VECTOR_SPD) is None:
			self.accelDecelTowards(touch_down_speed(self.aircraft_type), fast=on_short)
		rwy_ori = runway.orientation()
		if abs(self.params.position.headingTo(touch_down_point).diff(rwy_ori)) >= 90:
			self.say('Facing wrong direction! Executing missed approach.', False)
			self.MISAP()
			return
		height = self.params.altitude.diff(env.groundPressureAlt(self.params.position))
		if not settings.teacher_ACFT_touch_down_without_clearance \
				and height < min_clearToLand_height and self.instrOfType(Instruction.CLEARED_TO_LAND) is None:
			self.say('Going around; not cleared to land.', False)
			self.MISAP()
		elif touch_down_dist <= touch_down_distance_tolerance: # Attempt touch down!
			alt_check = height <= touch_down_height_tolerance
			hdg_check = self.params.heading.diff(rwy_ori, tolerance=touch_down_heading_tolerance) == 0
			speed_check = self.params.ias.diff(touch_down_speed(self.aircraft_type), tolerance=touch_down_speed_tolerance) <= 0
			if alt_check and hdg_check and speed_check: # TOUCH DOWN!
				self.params.heading = rwy_ori
				self.params.ias -= touch_down_speed_drop
				if self.touch_and_go_on_LDG:
					self.params.status.type = Status.RWY_TKOF
					self.instructions.append(Instruction(Instruction.CLEARED_TKOF))
				else:
					self.params.status.type = Status.RWY_LDG
					if self.skid_off_RWY_on_LDG:
						self.params.RWY_excursion_stage = 1
			else: # Missed approach
				reason = ('not lined up' if speed_check else 'too fast') if alt_check else 'too high'
				self.say('Missed touch down: %s, going around.' % reason, False)
				self.MISAP()
	
	def taxiTowardsReached(self, target):
		dist = self.params.position.distanceTo(target)
		if dist <= pilot_taxi_precision: # target reached: stop
			self.params.ias = Speed(0)
			return True
		else: # must move
			hdg = self.params.position.headingTo(target)
			diff = self.params.heading.diff(hdg, tolerance=taxi_max_turn_without_decel)
			if diff == 0: # more or less facing goal
				self.params.heading = hdg
				self.params.ias = taxi_speed
			else: # must turn towards target point
				self.turnTowards(hdg, fastOK=True)
				self.params.ias = taxi_turn_speed
			self.taxiForward(maxdist=dist)
			return False # target not known to be reached yet
	
	def taxiForward(self, maxdist=None):
		dist = distance_travelled(self.tick_interval, self.params.ias)
		if maxdist is not None and dist > maxdist:
			dist = maxdist
		new_pos = self.params.position.moved(self.params.heading, dist)
		if self.statusType() not in [Status.TAXIING, Status.READY, Status.LINED_UP] \
				or all(ground_separated(other, new_pos, self.aircraft_type) or
					new_pos.distanceTo(other.params.position) > self.params.position.distanceTo(other.params.position)
					for other in settings.session_manager.getAircraft() if other is not self and other.isGroundStatus()):
			self.params.position = new_pos
			self.params.altitude = env.groundPressureAlt(self.params.position)
	
	def turnTowards(self, hdg, tolerance=0, fastOK=False, rightTurn=None):
		"""
		works airborne and on ground. "direction": True for a right turn, False for a left turn
		"""
		diff = hdg.diff(self.params.heading, tolerance)
		if diff != 0:
			max_abs_turn = (fast_turn_factor if fastOK else 1) * self.maxTurn(self.tick_interval)
			self.params.heading += (1 if some(rightTurn, diff > 0) else -1) * min(abs(diff), max_abs_turn)
	
	def flyTowards(self, coords):
		self.turnTowards(self.params.position.headingTo(coords), tolerance=pilot_hdg_precision)
	
	def climbDescendTowards(self, alt, climbOK=True, descendOK=True):
		diff = alt.diff(self.params.altitude, tolerance=pilot_alt_precision)
		if diff < 0 and descendOK or diff > 0 and climbOK:
			vert = min(self.maxClimb(self.tick_interval), abs(diff))
			self.params.altitude = self.params.altitude + (vert if diff > 0 else -vert)

	def accelDecelTowards(self, spd, accelOK=True, decelOK=True, fast=False, tol=pilot_spd_precision):
		diff = spd.diff(self.params.ias, tolerance=tol)
		if diff < 0 and decelOK or diff > 0 and accelOK:
			spdincr = min((fast_accel_decel_factor if fast else 1) * self.maxSpdIncr(self.tick_interval), abs(diff))
			self.params.ias = self.params.ias + (spdincr if diff > 0 else -spdincr)
	
	def MISAP(self):
		self.params.status = Status(Status.AIRBORNE)
		pop_all(self.instructions, lambda i: i.type in [Instruction.CLEARED_TO_LAND, Instruction.CLEARED_APP, Instruction.INTERCEPT_LOC])
		alt_spec = default_initial_climb_spec()
		if self.params.altitude.diff(env.pressureAlt(alt_spec)) < 0:
			self.instructions.append(Instruction(Instruction.VECTOR_ALT, arg=alt_spec))
	
	
	## RADIO
	
	def say(self, txt_message, responding, initAddressee=None):
		"""
		responding: True if callsign should come last on the radio; False will place callsign first.
		initAddressee: msg starts with addressee callsign, followed by own without shortening.
		"""
		signals.incomingRadioChatMsg.emit(ChatMessage(self.identifier, speech_str2txt(txt_message)))
		if settings.session_manager.session_type == SessionType.SOLO:
			if settings.solo_voice_readback and self.pilotVoice() is not None:
				if self.airline is None:
					cs = speak_callsign_tail_number(self.identifier, shorten=(initAddressee is None))
				else:
					cs = speak_callsign_commercial_flight(self.airline, self.identifier[len(self.airline):])
				msg = speech_str2tts(txt_message)
				if initAddressee is None:
					tts_struct = [msg, cs] if responding else [cs, msg]
				else: # explicitly addressing
					tts_struct = [initAddressee, cs, msg]
				signals.voiceMsg.emit(self, ', '.join(tts_struct)) # takes care of the RDF signal
			else: # Not synthesising voice, but should simulate a radio signal for RDF system
				self.beginRdfUpdates(self.identifier) # NOTE: assuming unique radio
				QTimer.singleShot(simulated_radio_signal_timeout, lambda: self.endRdfUpdates(self.identifier))
	
	def ttsAppWanted(self, default=''):
		if self.wantsVisualApp():
			return 'visual'
		elif self.wantsILS():
			return '\\SPLIT_CHARS{ILS}'
		else: # this only happens sometimes in teacher mode
			return default
	
	def makeInitialContact(self):
		msg = 'Hello, '
		
		if self.statusType() == Status.READY:
			msg += 'short of runway \\RWY{%s}, ready for departure.' % self.params.status.arg
			
		elif self.statusType() == Status.LANDING:
			if self.wantsVisualApp():
				msg += 'on visual approach runway \\RWY{%s}' % self.params.status.arg
			else: # ILS
				msg += 'established \\SPLIT_CHARS{ILS} \\RWY{%s}' % self.params.status.arg
			
		elif self.statusType() == Status.TAXIING:
			if self.wantsToPark():
				msg += 'runway cleared, for parking at \\SPELL_ALPHANUMS{%s}' % self.goal
			else:
				if env.airport_data is not None:
					pk = env.airport_data.ground_net.closestParkingPosition(self.params.position, maxdist=.1)
					if pk is not None:
						msg += 'standing at \\SPELL_ALPHANUMS{%s}, ' % pk
				msg += 'ready to taxi'
		
		elif self.isInboundGoal():
			msg += '\\FL_ALT{%s}' % env.specifyAltFl(self.params.altitude).toStr()
			msg += ', inbound for %s approach' % self.ttsAppWanted()
			instr = self.instrOfType(Instruction.EXPECT_RWY)
			if instr is not None:
				msg += ' runway \\RWY{%s}' % instr.arg
			
		elif self.isOutboundGoal(): # Normally a departure received from TWR
			msg += 'passing \\FL_ALT{%s}' % env.specifyAltFl(self.params.altitude).toStr()
			instr = self.instrOfType(Instruction.VECTOR_ALT)
			if instr is not None:
				msg += ' for \\FL_ALT{%s}' % instr.arg.toStr()
		
		else: # Transit for CTR
			msg += '\\FL_ALT{%s}' % env.specifyAltFl(self.params.altitude).toStr()
		## Now SAY IT!
		self.say(msg, False, initAddressee=settings.location_radio_name)
	
	def readBack(self, instr_sequence):
		lst = []
		for instr in instr_sequence:
			msg = 'Please report: undefined read-back message' # should be overridden by switch case below
			if instr.type == Instruction.VECTOR_HDG:
				if instr.arg2 is None:
					msg = 'Heading '
				else:
					msg = 'Turn ' + ('right' if instr.arg2 else 'left') + ' heading '
				msg += '\\SPELL_ALPHANUMS{%s}' % instr.arg.read()
			elif instr.type == Instruction.VECTOR_ALT:
				msg = '\\FL_ALT{%s}' % instr.arg.toStr()
			elif instr.type == Instruction.VECTOR_SPD:
				msg = '\\SPEED{%s}' % instr.arg
			elif instr.type == Instruction.VECTOR_DCT:
				msg = 'Direct \\NAVPOINT{%s}' % instr.add_data['resolved_navpoint'].code
			elif instr.type == Instruction.CANCEL_VECTOR_SPD:
				msg = 'Speed my discretion'
			elif instr.type == Instruction.FOLLOW_ROUTE:
				msg = 'Route copied, proceeding to \\NAVPOINT{%s}' % instr.add_data['resolved_route'].currentWaypoint(self.params.position)
			elif instr.type == Instruction.HOLD:
				msg = 'Hold at \\NAVPOINT{%s}' % instr.add_data['resolved_navpoint'].code
			elif instr.type == Instruction.SQUAWK:
				msg = '\\SPELL_ALPHANUMS{%04o}' % instr.arg
			elif instr.type == Instruction.CANCEL_APP:
				msg = 'Cancel approach'
			elif instr.type == Instruction.HAND_OVER:
				msg = 'With \\ATC{%s}, thank you, good bye.' % instr.arg
			elif instr.type == Instruction.LINE_UP:
				msg = 'Line up and wait'
			elif instr.type == Instruction.INTERCEPT_NAV:
				msg = 'Intercept \\NAVPOINT{%s} \\SPELL_ALPHANUMS{%s}' % (instr.add_data['resolved_navpoint'].code, instr.arg2.read())
			elif instr.type == Instruction.INTERCEPT_LOC:
				msg = 'Intercept localiser \\RWY{%s}' % self.instrOfType(Instruction.EXPECT_RWY).arg
			elif instr.type == Instruction.EXPECT_RWY:
				if self.isGroundStatus():
					msg = 'Runway \\RWY{%s}, will report ready for departure' % instr.arg
				else:
					msg = 'Expecting \\RWY{%s} for %s approach' % (instr.arg, self.ttsAppWanted(default='the'))
			elif instr.type == Instruction.TAXI:
				if env.airport_data is None:
					msg = 'Unable to taxi'
				else:
					msg = env.airport_data.ground_net.taxiInstrStr(instr.arg, finalNonNode=instr.arg2, tts=True)
			elif instr.type == Instruction.HOLD_POSITION:
				msg = 'Hold position'
			elif instr.type == Instruction.CLEARED_APP:
				msg = 'Cleared %s \\RWY{%s}' % (self.ttsAppWanted(default='approach'), self.instrOfType(Instruction.EXPECT_RWY).arg)
			elif instr.type == Instruction.CLEARED_TKOF:
				msg = 'Cleared for take-off \\RWY{%s}' % self.params.status.arg
			elif instr.type == Instruction.CLEARED_TO_LAND:
				msg = 'Clear to land runway \\RWY{%s}' % self.instrOfType(Instruction.EXPECT_RWY).arg
			elif instr.type == Instruction.SAY_INTENTIONS:
				if self.wantsToPark():
					msg = 'Park at \\SPELL_ALPHANUMS{%s}' % self.goal
				elif self.isInboundGoal():
					msg = '%s approach' % self.ttsAppWanted()
					instr2 = self.instrOfType(Instruction.EXPECT_RWY)
					if instr2 is not None:
						msg += ', expecting runway \\RWY{%s}' % instr2.arg
				elif self.isOutboundGoal():
					msg = 'Departing to \\SPELL_ALPHANUMS{%s}' % self.goal[2]
					if self.goal[0] is not None:
						msg += ' via \\NAVPOINT{%s}' % self.goal[0].code
					if self.goal[1] is not None:
						msg += ', cruise \\FL_ALT{%s}' % self.goal[1]
				elif isinstance(self.goal, Airfield): # Transiting with destination
					msg = 'En-route to \\SPELL_ALPHANUMS{%s}' % self.goal
				else:
					msg = 'No intentions'
			lst.append(msg)
		## Now SAY IT!
		self.say(', '.join(lst), True)
	
	
	## SNAPSHOTS
	@staticmethod
	def fromStatusSnapshot(snapshot):
		cs, t, params, goal, spawned, frozen, instr = snapshot
		acft = ControlledAircraft(cs, t, params.dup(), goal)
		acft.instructions = [i.dup() for i in instr]
		acft.spawned = spawned
		acft.frozen = frozen
		return acft
	
	def statusSnapshot(self):
		return self.identifier, self.aircraft_type, self.params.dup(), self.goal, \
				self.spawned, self.frozen, [i.dup() for i in self.instructions]
