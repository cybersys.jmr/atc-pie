
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import struct
from math import sqrt
from sys import stderr
try:
	from pyaudio import PyAudio, paInt16
	pyaudio_available = True
except ImportError:
	pyaudio_available = False

from session.env import env

from gui.misc import signals


# ---------- Constants ----------

# -------------------------------

class LandLineStatus:
	IDLE, PLACED_CALL, INCOMING_CALL, IN_CALL = range(4)



def RMS_amplitude(block):
	count = len(block) / 2
	sum_squares = 0
	for sample in struct.unpack('%dh' % count, block): # iterate over 16-bit (short int) samples
		n = sample / 32768 # normalise short to 1
		sum_squares += n * n
	return sqrt(sum_squares / count)



class AbstractLandLineManager:
	"""
	Subclasses should redefine the following methods:
		- startVoiceWith (str arg: ATC callsign)
		- stopVoice()
		- sendLandLineRequest(str arg: ATC callsign)
		- sendLandLineDrop(str arg: ATC callsign)
	"""
	def requestLandLine(self, atc):
		if env.ATCs.outgoingLandLine(atc):
			print('WARNING: Phone line "%s" already requested.' % atc, file=stderr)
		else:
			self.sendLandLineRequest(atc) # keep before "startVoiceWith" or ATC does not receive LL request message
			if env.ATCs.incomingLandLine(atc):  # answering an incoming request
				for ll in env.ATCs.landLinesWithStatus(LandLineStatus.IN_CALL):  # currently in call (normally only one)
					self.dropLandLine(ll)  # put on hold
				self.startVoiceWith(atc)
				env.ATCs.setLandLineStatus(atc, LandLineStatus.IN_CALL)
			else:  # placing a call
				env.ATCs.setLandLineStatus(atc, LandLineStatus.PLACED_CALL, flash=True)
	
	def dropLandLine(self, atc):
		if env.ATCs.outgoingLandLine(atc):
			self.sendLandLineDrop(atc)
			if env.ATCs.incomingLandLine(atc):  # hanging up the current call (or putting on hold)
				self.stopVoice()
				env.ATCs.setLandLineStatus(atc, LandLineStatus.INCOMING_CALL, flash=False)
			else:  # cancelling my request
				env.ATCs.setLandLineStatus(atc, LandLineStatus.IDLE)
		else:
			print('WARNING: Phone line "%s" not requested.' % atc, file=stderr)
	
	
	# Methods to call from subclasses
	
	def createLandLine(self, atc):
		if env.ATCs.landLineStatus(atc) is None:
			env.ATCs.setLandLineStatus(atc, LandLineStatus.IDLE)
		else:
			print('WARNING: Phone line "%s" already exists.' % atc, file=stderr)
	
	def destroyLandLine(self, atc):
		if env.ATCs.landLineStatus(atc) is None:
			print('WARNING: Phone line "%s" does not exist.' % atc, file=stderr)
		else:
			if env.ATCs.outgoingLandLine(atc):
				self.dropLandLine(atc)
			env.ATCs.setLandLineStatus(atc, None)
	
	def receiveLandLineRequest(self, atc):
		if env.ATCs.incomingLandLine(atc):
			print('WARNING: Phone line "%s" already incoming.' % atc, file=stderr)
		else:
			if env.ATCs.outgoingLandLine(atc):  # they answer my request
				if len(env.ATCs.landLinesWithStatus(LandLineStatus.IN_CALL)) > 0:  # currently in (another) call; turn my request into theirs
					self.dropLandLine(atc)  # makes phone line IDLE
					env.ATCs.setLandLineStatus(atc, LandLineStatus.INCOMING_CALL, flash=True)
				else:  # they are picking up my requested call
					self.startVoiceWith(atc)
					env.ATCs.setLandLineStatus(atc, LandLineStatus.IN_CALL)
					signals.phoneCallAnswered.emit(atc)
			else:
				env.ATCs.setLandLineStatus(atc, LandLineStatus.INCOMING_CALL, flash=True)
				signals.incomingLandLineCall.emit(atc)
	
	def receiveLandLineDrop(self, atc):
		if env.ATCs.incomingLandLine(atc):
			if env.ATCs.outgoingLandLine(atc):  # they are hanging up
				self.stopVoice()
				env.ATCs.setLandLineStatus(atc, LandLineStatus.PLACED_CALL, flash=False)
			else:
				env.ATCs.setLandLineStatus(atc, LandLineStatus.IDLE)
			signals.phoneCallDropped.emit(atc)
		else:
			print('WARNING: Phone line "%s" not incoming.' % atc, file=stderr)

	# Methods to implement follow
	def startVoiceWith(self, atc):
		raise NotImplementedError()
	
	def stopVoice(self):
		raise NotImplementedError()
	
	def sendLandLineRequest(self, atc):
		raise NotImplementedError()
	
	def sendLandLineDrop(self, atc):
		raise NotImplementedError()
