
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from copy import copy

from base.params import Heading, AltFlSpec, Speed
from base.util import some

from session.config import settings
from session.env import env
from session.manager import SessionType


# ---------- Constants ----------

taxi_instr_node_max_dist = .1 # NM

# -------------------------------

# Instructions and arg types; "optional" arg2 means it can be None
#
# VECTOR_HDG:    arg is Heading; optional arg2 is bool turn direction (True for right)
# VECTOR_ALT:    arg is AltFlSpec
# VECTOR_SPD:    arg is Speed
# VECTOR_DCT:    arg is str navpoint name
# CANCEL_VECTOR_SPD
# FOLLOW_ROUTE:  arg is str route (waypoints and leg specs)
# HOLD:          arg is str navpoint name
# SQUAWK:        arg is int
# HAND_OVER:     arg is str next ATC callsign; optional arg2 is str frequency
# CANCEL_APP
# LINE_UP
# INTERCEPT_NAV: arg is str navpoint name; obligatory arg2 is radial/bearing Heading
# INTERCEPT_LOC
# EXPECT_RWY:    arg is str RWY name
# TAXI:          arg is GND net node sequence (list); optional arg2 is the off-node PKG position after the taxi route
# HOLD_POSITION
# CLEARED_APP
# CLEARED_TKOF
# CLEARED_TO_LAND
# SAY_INTENTIONS


class Instruction:
	enum = \
		VECTOR_HDG, VECTOR_ALT, VECTOR_SPD, VECTOR_DCT, CANCEL_VECTOR_SPD, FOLLOW_ROUTE, \
		HOLD, SQUAWK, HAND_OVER, CANCEL_APP, LINE_UP, INTERCEPT_NAV, INTERCEPT_LOC, EXPECT_RWY, \
		TAXI, HOLD_POSITION, CLEARED_APP, CLEARED_TKOF, CLEARED_TO_LAND, SAY_INTENTIONS = range(20)

	@staticmethod
	def type2str(t):
		return {
				Instruction.VECTOR_HDG: 'VECTOR_HDG',
				Instruction.VECTOR_ALT: 'VECTOR_ALT',
				Instruction.VECTOR_SPD: 'VECTOR_SPD',
				Instruction.VECTOR_DCT: 'VECTOR_DCT',
				Instruction.CANCEL_VECTOR_SPD: 'CANCEL_VECTOR_SPD',
				Instruction.FOLLOW_ROUTE: 'FOLLOW_ROUTE',
				Instruction.HOLD: 'HOLD',
				Instruction.SQUAWK: 'SQUAWK',
				Instruction.HAND_OVER: 'HAND_OVER',
				Instruction.CANCEL_APP: 'CANCEL_APP',
				Instruction.LINE_UP: 'LINE_UP',
				Instruction.INTERCEPT_NAV: 'INTERCEPT_NAV',
				Instruction.INTERCEPT_LOC: 'INTERCEPT_LOC',
				Instruction.EXPECT_RWY: 'EXPECT_RWY',
				Instruction.TAXI: 'TAXI',
				Instruction.HOLD_POSITION: 'HOLD_POSITION',
				Instruction.CLEARED_APP: 'CLEARED_APP',
				Instruction.CLEARED_TKOF: 'CLEARED_TKOF',
				Instruction.CLEARED_TO_LAND: 'CLEARED_TO_LAND',
				Instruction.SAY_INTENTIONS: 'SAY_INTENTIONS'
			}[t]
	
	class Error(Exception):
		pass
	
	@staticmethod
	def fromCpdlcMsgElement(msg_element, uplink):
		id_split = msg_element.split(' ', maxsplit=1)
		elt_id = id_split[0]
		argstr = '' if len(id_split) < 2 else id_split[1]
		if uplink: # used for teacher receiving student's uplinks
			if elt_id == 'LATU-11' or elt_id == 'LATU-16': # resp. "turn left/right heading" and "fly heading"
				return Instruction(Instruction.VECTOR_HDG, arg=Heading(int(argstr[-3:]), False),
													arg2=(argstr.startswith('R') if elt_id[-1] == '1' else None))
			elif elt_id == 'LVLU-5' or elt_id == 'LVLU-6' or elt_id == 'LVLU-9':
				return Instruction(Instruction.VECTOR_ALT, arg=AltFlSpec.fromStr(argstr))
			elif elt_id == 'SPDU-9' or elt_id == 'SPDU-11':
				return Instruction(Instruction.VECTOR_SPD, arg=Speed(int(argstr)))
			elif elt_id == 'RTEU-2':
				return Instruction(Instruction.VECTOR_DCT, arg=argstr)
			elif elt_id == 'SPDU-13':
				return Instruction(Instruction.CANCEL_VECTOR_SPD)
			elif elt_id == 'RTEU-7': # fails recognising Route if departure and arrival are not provided, but worth a try
				return Instruction(Instruction.FOLLOW_ROUTE, arg=argstr)
			elif elt_id == 'RTEU-12':
				return Instruction(Instruction.HOLD, arg=argstr)
			elif elt_id == 'ADVU-9':
				return Instruction(Instruction.SQUAWK, arg=int(argstr, base=8))
			elif elt_id == 'COMU-1':
				split = argstr.split(' ', maxsplit=1)
				if split[0] == '':
					raise ValueError('Empty callsign after COMU-1')
				frq = None if len(split) < 2 or split[1] == '' else split[1]
				return Instruction(Instruction.HAND_OVER, arg=split[0], arg2=frq)
		
		else: # downlink
			if elt_id == 'RTED-6':
				return Instruction(Instruction.VECTOR_HDG, arg=Heading(int(argstr), False))
			elif elt_id == 'LVLD-1':
				return Instruction(Instruction.VECTOR_ALT, arg=AltFlSpec.fromStr(argstr))
			elif elt_id == 'SPDD-1':
				return Instruction(Instruction.VECTOR_SPD, arg=Speed(int(argstr)))
			elif elt_id == 'RTED-1':
				return Instruction(Instruction.VECTOR_DCT, arg=argstr)
			elif elt_id == 'RTED-3': # fails recognising Route if departure and arrival are not provided, but worth a try
				return Instruction(Instruction.FOLLOW_ROUTE, arg=argstr)
			
		raise ValueError('Unhandled message element identifier "%s" or mismatching up-/downlink direction' % elt_id)
	
	def __init__(self, init_type, arg=None, arg2=None, addData=None):
		self.type = init_type
		self.arg = arg
		self.arg2 = arg2
		self.add_data = some(addData, {}) # known keys: voice, voice_rwy, voice_app, resolved_navpoint, resolved_route
	
	def __str__(self):
		return 'I:%d:%s:%s' % (self.type, self.arg, self.arg2)
	
	def dup(self):
		return Instruction(self.type, arg=copy(self.arg), arg2=copy(self.arg2), addData=copy(self.add_data))
	
	def isVoiceRecognised(self):
		return self.add_data.get('voice', False)
	
	def readOutStr(self, acft):
		"""
		NOTE acft can be None, then a more generic read-out will be suggested
		"""
		if self.type == Instruction.VECTOR_HDG:
			if self.arg2 is None:
				verb_str = 'Fly'
			else:
				verb_str = 'Turn right' if self.arg2 else 'Turn left'
			return '%s heading %s' % (verb_str, self.arg.read())
		elif self.type == Instruction.VECTOR_ALT:
			verb_prefix = 'Fly'
			qnh_suffix = ''
			if acft is not None:
				if acft.considerOnGround():
					verb_prefix = 'Initial climb' # Override
				else:
					c_alt = acft.xpdrAlt()
					if c_alt is not None:
						try:
							v_alt = env.pressureAlt(self.arg)
						except ValueError:
							pass
						else:
							if c_alt.diff(v_alt) < 0:
								verb_prefix = 'Climb' # Override
							else:
								verb_prefix = 'Descend' # Override
								qnh = env.QNH(noneSafe=False)
								if qnh is not None and v_alt.FL() < env.transitionLevel() <= c_alt.FL():
									qnh_suffix = ', QNH %d' % qnh # Override
			return '%s %s%s' % (verb_prefix, self.arg.toStr(), qnh_suffix)
		elif self.type == Instruction.VECTOR_SPD:
			return 'Speed %s' % self.arg
		elif self.type == Instruction.VECTOR_DCT:
			return 'Proceed direct %s' % self.arg
		elif self.type == Instruction.CANCEL_VECTOR_SPD:
			return 'Speed your discretion'
		elif self.type == Instruction.FOLLOW_ROUTE:
			if acft is None or acft.considerOnGround():
				return 'Cleared route %s' % self.arg
			else:
				return 'Proceed %s' % self.arg
		elif self.type == Instruction.HOLD:
			return 'Hold at %s, as published' % self.arg
		elif self.type == Instruction.SQUAWK:
			return 'Squawk %04o' % self.arg
		elif self.type == Instruction.HAND_OVER:
			return 'Contact %s%s, good bye.' % (self.arg, ('' if self.arg2 is None else ' on %s' % self.arg2))
		elif self.type == Instruction.CANCEL_APP:
			return 'Cancel approach, stand by for vectors'
		elif self.type == Instruction.LINE_UP:
			if acft is not None and settings.session_manager.session_type == SessionType.SOLO and acft.isReadyForDeparture():
				return 'Runway %s, line up and wait' % acft.params.status.arg
			else:
				return 'Line up and wait'
		elif self.type == Instruction.INTERCEPT_NAV:
			return 'Intercept %s from/to %s' % (self.arg2.read(), self.arg)
		elif self.type == Instruction.INTERCEPT_LOC:
			msg = 'Intercept localiser'
			if acft is not None and settings.session_manager.session_type == SessionType.SOLO:
				instr = acft.instrOfType(Instruction.EXPECT_RWY)
				if instr is not None:
					msg += ' for runway %s' % instr.arg
			return msg
		elif self.type == Instruction.EXPECT_RWY:
			return 'Expect runway %s' % self.arg
		elif self.type == Instruction.TAXI:
			if env.airport_data is None:
				return 'Taxi... without an airport!'
			else:
				return env.airport_data.ground_net.taxiInstrStr(self.arg, finalNonNode=self.arg2)
		elif self.type == Instruction.HOLD_POSITION:
			return 'Hold position'
		elif self.type == Instruction.CLEARED_APP:
			if acft is not None and settings.session_manager.session_type == SessionType.SOLO \
					and acft.isInboundGoal() and not acft.wantsToPark():
				msg = 'Cleared for %s approach' % ('ILS' if acft.goal else 'visual')
				instr = acft.instrOfType(Instruction.EXPECT_RWY)
				if instr is not None:
					msg += ' runway %s' % instr.arg
				return msg
			else:
				return 'Cleared for approach'
		elif self.type == Instruction.CLEARED_TKOF:
			if acft is not None and settings.session_manager.session_type == SessionType.SOLO and acft.isReadyForDeparture():
				tkof_str = 'Runway %s, cleared for take-off' % acft.params.status.arg
			else:
				tkof_str = 'Cleared for take-off'
			w = env.primaryWeather()
			if w is not None:
				tkof_str += ', wind %s' % w.readWind()
			return tkof_str
		elif self.type == Instruction.CLEARED_TO_LAND:
			rwy = None
			if acft is not None and settings.session_manager.session_type == SessionType.SOLO:
				instr = acft.instrOfType(Instruction.EXPECT_RWY)
				if instr is not None:
					rwy = instr.arg
			if rwy is None:
				ldg_str = 'Cleared to land'
			else:
				ldg_str = 'Runway %s, cleared to land' % rwy
			w = env.primaryWeather()
			if w is not None:
				ldg_str += ', wind %s' % w.readWind()
			return ldg_str
		elif self.type == Instruction.SAY_INTENTIONS:
			return 'Say intentions?'
	
	
	## CPDLC MESSAGE ELEMENTS
	
	def toCpdlcUplinkMsgElt(self, acft):
		if self.type == Instruction.VECTOR_HDG:
			if self.arg2 is None:
				return 'LATU-16 %s' % self.arg.read() # fly heading
			else: # turn direction specified in instruction
				return 'LATU-11 %s %s' % (('RIGHT' if self.arg2 else 'LEFT'), self.arg.read())
		elif self.type == Instruction.VECTOR_ALT: # NOTE might be used for "initial climb"
			argstr = self.arg.toStr(unit=False)
			alt = None if acft is None else acft.xpdrAlt()
			if alt is not None:
				try:
					tgt = env.pressureAlt(self.arg)
					if alt.diff(tgt, tolerance=100) < 0:
						return 'LVLU-6 %s' % argstr # climb
					elif alt.diff(tgt, tolerance=100) > 0:
						return 'LVLU-9 %s' % argstr # descend
				except ValueError: # syntax error in arg reading
					pass
			return 'LVLU-5 %s' % argstr # maintain
		elif self.type == Instruction.VECTOR_SPD:
			ias = None if acft is None else acft.IAS()
			if ias is None or ias.diff(self.arg) < 0: # capture None case because no escape CPDLC option
				return 'SPDU-9 %03d' % self.arg.kt # increase speed
			else:
				return 'SPDU-11 %03d' % self.arg.kt # reduce speed
		elif self.type == Instruction.VECTOR_DCT:
			return 'RTEU-2 %s' % self.arg
		elif self.type == Instruction.CANCEL_VECTOR_SPD:
			return 'SPDU-13'
		elif self.type == Instruction.FOLLOW_ROUTE:
			return 'RTEU-7 %s' % self.arg
		elif self.type == Instruction.HOLD:
			return 'RTEU-12 %s' % self.arg
		elif self.type == Instruction.SQUAWK:
			return 'ADVU-9 %04o' % self.arg
		elif self.type == Instruction.HAND_OVER:
			return 'COMU-1 %s %s' % (self.arg, some(self.arg2, ''))
		else:
			return 'TXTU-1 %s' % self.readOutStr(acft).upper()
	
	def toCpdlcDownlinkMsgElt(self, acft):
		if self.type == Instruction.VECTOR_HDG:
			return 'RTED-6 %s' % self.arg.read()
		elif self.type == Instruction.VECTOR_ALT: # NOTE might be used for "initial climb"
			return 'LVLD-1 %s' % self.arg.toStr(unit=False)
		elif self.type == Instruction.VECTOR_SPD:
			return 'SPDD-1 %s' % self.arg
		elif self.type == Instruction.VECTOR_DCT:
			return 'RTED-1 %s' % self.arg
		elif self.type == Instruction.FOLLOW_ROUTE:
			return 'RTED-3 %s' % self.arg
		elif self.type == Instruction.HAND_OVER:
			return 'TXTD-1 REQUEST TO CONTACT %s' % self.arg
		else:
			raise ValueError('Downlink request not supported for this instruction.')
