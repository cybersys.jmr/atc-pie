
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from base.instr import Instruction
from base.utc import now

from session.config import settings
from session.manager import SessionType


# ---------- Constants ----------

cpdlc_msg_element_separator = '|' # keep non-alpha-num and without spaces

# -------------------------------


class CpdlcMessage:
	def __init__(self, uplink, contents):
		"""
		uplink: bool = is from ATC to ACFT
		msg_elements: str for a single element, or str list for multiple elements, each with element identifier as first word
		"""
		self.time_stamp = now()
		self.is_uplink = uplink
		self.msg_elements = [contents] if isinstance(contents, str) else contents
		try:
			self.recognised_instructions = [Instruction.fromCpdlcMsgElement(instr, uplink) for instr in self.msg_elements]
		except ValueError:
			self.recognised_instructions = None

	@staticmethod
	def fromEncodedStr(uplink, encoded):
		sep = encoded.split(' ', maxsplit=1)[0]
		if len(sep) > 0 and not sep[0].isalnum():  # custom separator defined (to be surrounded by spaces between elements)
			elt_split = encoded[:len(sep) + 1].split(' %s ' % sep)
		else:  # simple separation
			elt_split = encoded.split(cpdlc_msg_element_separator)
		return CpdlcMessage(uplink, elt_split)
	
	def timeStamp(self):
		return self.time_stamp
	
	def isFromMe(self):
		return self.is_uplink == (settings.session_manager.session_type != SessionType.TEACHER)
	
	def isUplink(self):
		return self.is_uplink
	
	def isDownlink(self):
		return not self.is_uplink
	
	def isAcknowledgement(self):  # single ROGER or WILCO, uplink or downlink
		return len(self.msg_elements) == 1 \
				and self.msg_elements[0] in [RspId.uplink_ROGER, RspId.downlink_ROGER, RspId.downlink_WILCO]
	
	def isStandby(self):  # single STANDBY, uplink or downlink
		return len(self.msg_elements) == 1 \
				and (self.msg_elements[0] == RspId.uplink_STANDBY or self.msg_elements[0] == RspId.downlink_STANDBY)
	
	def containsUnable(self):  # contains UNABLE, uplink or downlink
		return any(elt == RspId.uplink_UNABLE or elt == RspId.downlink_UNABLE for elt in self.msg_elements)
	
	def expectsAnswer(self): # FUTURE this can be refined (there are a few more elements not requiring an answer)
		return not self.isAcknowledgement() and not all(elt.startswith('SYS') for elt in self.msg_elements)
	
	def elements(self):
		return self.msg_elements
	
	def recognisedInstructions(self):
		return self.recognised_instructions
	
	def displayText(self):
		return '. '.join(CPDLC_element_display_text(elt) for elt in self.msg_elements)
	
	def toEncodedStr(self):
		sep = cpdlc_msg_element_separator
		if len(self.msg_elements) >= 2 and any(sep in elt for elt in self.msg_elements):
			while any(sep in elt for elt in self.msg_elements):
				sep += cpdlc_msg_element_separator
		if sep == cpdlc_msg_element_separator:
			return sep.join(self.msg_elements)
		else:
			return '%s ' % sep + (' %s ' % sep).join(self.msg_elements)


class RspId:
	uplink_UNABLE = 'RSPU-1'
	uplink_STANDBY = 'RSPU-2'
	uplink_ROGER = 'RSPU-4'
	uplink_AFFIRM = 'RSPU-5'
	uplink_NEGATIVE = 'RSPU-6'
	downlink_WILCO = 'RSPD-1'
	downlink_UNABLE = 'RSPD-2'
	downlink_STANDBY = 'RSPD-3'
	downlink_ROGER = 'RSPD-4'
	downlink_AFFIRM = 'RSPD-5'
	downlink_NEGATIVE = 'RSPD-6'


def CPDLC_free_text_element(uplink, text, num=1):
	return 'TXT%s-%d %s' % ('DU'[uplink], num, text.upper())


def _argTuple(n, argstr):  # only useful below, for splitting args when n >= 2
	split = argstr.split(' ', maxsplit=(n - 1))
	while len(split) < n:
		split.append('')
	return tuple(split)


def CPDLC_element_display_text(msg_element):
	idsplit = msg_element.split(' ', maxsplit=1)
	if len(idsplit) == 0:
		raise ValueError('Empty message element')
	elt_id = idsplit[0]
	argstr = '' if len(idsplit) < 2 else idsplit[1]
	
	## UPLINKS
	# RTE (1..17)
	if elt_id == 'RTEU-2':
		return 'PROCEED DIRECT TO %s' % argstr
	if elt_id == 'RTEU-3':
		return 'AT TIME %s PROCEED DIRECT TO %s' % _argTuple(2, argstr)
	if elt_id == 'RTEU-4':
		return 'AT %s PROCEED DIRECT TO %s' % _argTuple(2, argstr)
	if elt_id == 'RTEU-6':
		return 'CLEARED TO %s VIA %s' % _argTuple(2, argstr)
	elif elt_id == 'RTEU-7':
		return 'CLEARED %s' % argstr
	elif elt_id == 'RTEU-11':
		return 'AT %s HOLD INBOUND TRACK %s %s TURNS %s LEGS' % _argTuple(4, argstr)
	if elt_id == 'RTEU-12':
		return 'AT %s HOLD AS PUBLISHED' % argstr
	if elt_id == 'RTEU-13':
		return 'EXPECT FURTHER CLEARANCE AT %s' % argstr
	if elt_id == 'RTEU-16':
		return 'REQUEST POSITION REPORT'
	if elt_id == 'RTEU-17':
		return 'ADVISE ETA %s' % argstr
	# LAT (1..19)
	elif elt_id == 'LATU-9':
		return 'RESUME OWN NAVIGATION'
	elif elt_id == 'LATU-11':
		return 'TURN %s HEADING %s' % _argTuple(2, argstr)
	elif elt_id == 'LATU-12':
		return 'TURN %s GROUND TRACK %s' % _argTuple(2, argstr)
	elif elt_id == 'LATU-16':
		return 'FLY HEADING %s' % argstr
	elif elt_id == 'LATU-19':
		return 'REPORT PASSING %s' % argstr
	# LVL (1..32)
	elif elt_id == 'LVLU-5':
		return 'MAINTAIN %s' % argstr
	elif elt_id == 'LVLU-6':
		return 'CLIMB TO %s' % argstr
	elif elt_id == 'LVLU-8':
		return 'AT %s CLIMB TO %s' % _argTuple(2, argstr)
	elif elt_id == 'LVLU-9':
		return 'DESCEND TO %s' % argstr
	elif elt_id == 'LVLU-11':
		return 'AT %s DESCEND TO %s' % _argTuple(2, argstr)
	# CST (1..15)
	elif elt_id == 'CSTU-1':
		return 'CROSS %s AT %s' % _argTuple(2, argstr)
	elif elt_id == 'CSTU-2':
		return 'CROSS %s AT OR ABOVE %s' % _argTuple(2, argstr)
	elif elt_id == 'CSTU-3':
		return 'CROSS %s AT OR BELOW %s' % _argTuple(2, argstr)
	elif elt_id == 'CSTU-4':
		return 'CROSS %s AT TIME %s' % _argTuple(2, argstr)
	elif elt_id == 'CSTU-5':
		return 'CROSS %s BEFORE %s' % _argTuple(2, argstr)
	elif elt_id == 'CSTU-6':
		return 'CROSS %s AFTER %s' % _argTuple(2, argstr)
	elif elt_id == 'CSTU-7':
		return 'CROSS %s BETWEEN TIME %s AND TIME %s' % _argTuple(3, argstr)
	# SPD (1..17)
	elif elt_id == 'SPDU-4':
		return 'MAINTAIN %s' % argstr
	elif elt_id == 'SPDU-5':
		return 'MAINTAIN PRESENT SPEED'
	elif elt_id == 'SPDU-9':
		return 'INCREASE SPEED TO %s' % argstr
	elif elt_id == 'SPDU-11':
		return 'REDUCE SPEED TO %s' % argstr
	elif elt_id == 'SPDU-13':
		return 'RESUME NORMAL SPEED'
	# ADV (1..19)
	elif elt_id == 'ADVU-2':
		return 'SURVEILLANCE SERVICE TERMINATED'
	elif elt_id == 'ADVU-3':
		return 'IDENTIFIED %s' % argstr
	elif elt_id == 'ADVU-4':
		return 'IDENTIFICATION LOST'
	elif elt_id == 'ADVU-6':
		return 'REQUEST AGAIN WITH NEXT ATC UNIT'
	elif elt_id == 'ADVU-9':
		return 'SQUAWK %s' % argstr
	elif elt_id == 'ADVU-15':
		return 'SQUAWK IDENT'
	elif elt_id == 'ADVU-19':
		return '%s DEVIATION DETECTED. VERIFY AND ADVISE' % argstr
	# COM (1..9)
	elif elt_id == 'COMU-1':
		return 'CONTACT %s %s' % _argTuple(2, argstr)
	elif elt_id == 'COMU-2':
		return 'AT %s CONTACT %s %s' % _argTuple(3, argstr)
	elif elt_id == 'COMU-5':
		return 'MONITOR %s %s' % _argTuple(2, argstr)
	# SPC (1..5)
	# EMG (1..3)
	elif elt_id == 'EMGU-1':
		return 'REPORT ENDURANCE AND PERSONS ON BOARD'
	# RSP (1..8)
	elif elt_id == 'RSPU-1':
		return 'UNABLE'
	elif elt_id == 'RSPU-2':
		return 'STANDBY'
	elif elt_id == 'RSPU-4':
		return 'ROGER'
	elif elt_id == 'RSPU-5':
		return 'AFFIRM'
	elif elt_id == 'RSPU-6':
		return 'NEGATIVE'
	# SUP (1..4)
	elif elt_id == 'SUPU-1':
		return 'WHEN READY'
	elif elt_id == 'SUPU-2':
		return 'DUE TO %s' % argstr
	elif elt_id == 'SUPU-3':
		return 'EXPEDITE'
	# TXT (1..5)
	elif elt_id.startswith('TXTU-') and elt_id[5:].isdigit():
		return argstr
	# SYS (1..7)
	elif elt_id == 'SYSU-2':
		return 'NEXT DATA AUTHORITY %s' % argstr
	
	## DOWNLINKS
	# RTE (1..10)
	if elt_id == 'RTED-1':
		return 'REQUEST DIRECT TO %s' % argstr
	elif elt_id == 'RTED-3':
		return 'REQUEST CLEARANCE %s' % argstr
	elif elt_id == 'RTED-5':
		return 'POSITION REPORT %s' % argstr
	elif elt_id == 'RTED-6':
		return 'REQUEST HEADING %s' % argstr
	elif elt_id == 'RTED-7':
		return 'REQUEST GROUND TRACK %s' % argstr
	elif elt_id == 'RTED-8':
		return 'WHEN CAN WE EXPECT BACK ON ROUTE'
	elif elt_id == 'RTED-10':
		return 'ETA %s TIME %s' % _argTuple(2, argstr)
	# LAT (1..8)
	elif elt_id == 'LATD-3':
		return 'CLEAR OF WEATHER'
	elif elt_id == 'LATD-4':
		return 'BACK ON ROUTE'
	elif elt_id == 'LATD-8':
		return 'PASSING %s' % argstr
	# LVL (1..18)
	elif elt_id == 'LVLD-1':
		return 'REQUEST %s' % argstr
	elif elt_id == 'LVLD-6':
		return 'WHEN CAN WE EXPECT LOWER LEVEL'
	elif elt_id == 'LVLD-7':
		return 'WHEN CAN WE EXPECT HIGHER LEVEL'
	# SPD (1..6)
	elif elt_id == 'SPDD-1':
		return 'REQUEST %s' % argstr
	elif elt_id == 'SPDD-2':
		return 'WHEN CAN WE EXPECT %s' % argstr
	elif elt_id == 'SPDD-6':
		return 'WE CANNOT ACCEPT %s' % argstr
	# ADV (1..2)
	elif elt_id == 'ADVD-1':
		return 'SQUAWKING %s' % argstr
	# COM (1..2)
	elif elt_id == 'COMD-1':
		return 'REQUEST VOICE CONTACT %s' % argstr
	# SPC (1..5)
	# EMG (1..4)
	elif elt_id == 'EMGD-1':
		return 'PAN PAN PAN'
	elif elt_id == 'EMGD-2':
		return 'MAYDAY MAYDAY MAYDAY'
	elif elt_id == 'EMGD-4':
		return 'CANCEL EMERGENCY'
	# RSP (1..6)
	elif elt_id == 'RSPD-1':
		return 'WILCO'
	elif elt_id == 'RSPD-2':
		return 'UNABLE'
	elif elt_id == 'RSPD-3':
		return 'STANDBY'
	elif elt_id == 'RSPD-4':
		return 'ROGER'
	elif elt_id == 'RSPD-5':
		return 'AFFIRM'
	elif elt_id == 'RSPD-6':
		return 'NEGATIVE'
	# SUP (single 1)
	elif elt_id == 'SUPD-1':
		return 'DUE TO %s' % argstr
	# TXT (1..2)
	elif elt_id.startswith('TXTD-') and elt_id[5:].isdigit():
		return argstr
	# SYS (1..7)
	
	return '(undecoded) ' + msg_element
