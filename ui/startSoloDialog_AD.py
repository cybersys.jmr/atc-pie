# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'startSoloDialog_AD.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_startSoloDialog_AD(object):
    def setupUi(self, startSoloDialog_AD):
        startSoloDialog_AD.setObjectName("startSoloDialog_AD")
        startSoloDialog_AD.resize(344, 357)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(startSoloDialog_AD)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.groupBox = QtWidgets.QGroupBox(startSoloDialog_AD)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.GND_tickBox = QtWidgets.QCheckBox(self.groupBox)
        self.GND_tickBox.setObjectName("GND_tickBox")
        self.verticalLayout.addWidget(self.GND_tickBox)
        self.TWR_tickBox = QtWidgets.QCheckBox(self.groupBox)
        self.TWR_tickBox.setObjectName("TWR_tickBox")
        self.verticalLayout.addWidget(self.TWR_tickBox)
        self.APP_tickBox = QtWidgets.QCheckBox(self.groupBox)
        self.APP_tickBox.setObjectName("APP_tickBox")
        self.verticalLayout.addWidget(self.APP_tickBox)
        self.DEP_tickBox = QtWidgets.QCheckBox(self.groupBox)
        self.DEP_tickBox.setObjectName("DEP_tickBox")
        self.verticalLayout.addWidget(self.DEP_tickBox)
        self.verticalLayout_2.addWidget(self.groupBox)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(startSoloDialog_AD)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.initTrafficCount_edit = QtWidgets.QSpinBox(startSoloDialog_AD)
        self.initTrafficCount_edit.setProperty("value", 2)
        self.initTrafficCount_edit.setObjectName("initTrafficCount_edit")
        self.horizontalLayout_2.addWidget(self.initTrafficCount_edit)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        self.line = QtWidgets.QFrame(startSoloDialog_AD)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout_2.addWidget(self.line)
        self.label_2 = QtWidgets.QLabel(startSoloDialog_AD)
        font = QtGui.QFont()
        font.setItalic(True)
        self.label_2.setFont(font)
        self.label_2.setWordWrap(True)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_2.addWidget(self.label_2)
        self.buttonBox = QtWidgets.QDialogButtonBox(startSoloDialog_AD)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout_2.addWidget(self.buttonBox)
        self.label.setBuddy(self.initTrafficCount_edit)

        self.retranslateUi(startSoloDialog_AD)
        QtCore.QMetaObject.connectSlotsByName(startSoloDialog_AD)
        startSoloDialog_AD.setTabOrder(self.GND_tickBox, self.TWR_tickBox)
        startSoloDialog_AD.setTabOrder(self.TWR_tickBox, self.APP_tickBox)
        startSoloDialog_AD.setTabOrder(self.APP_tickBox, self.DEP_tickBox)
        startSoloDialog_AD.setTabOrder(self.DEP_tickBox, self.initTrafficCount_edit)

    def retranslateUi(self, startSoloDialog_AD):
        _translate = QtCore.QCoreApplication.translate
        startSoloDialog_AD.setWindowTitle(_translate("startSoloDialog_AD", "Solo airport simulation"))
        self.groupBox.setTitle(_translate("startSoloDialog_AD", "Select ATC positions"))
        self.GND_tickBox.setText(_translate("startSoloDialog_AD", "GND: control the ground moving area"))
        self.TWR_tickBox.setText(_translate("startSoloDialog_AD", "TWR: control runways"))
        self.APP_tickBox.setText(_translate("startSoloDialog_AD", "APP: vector arrivals onto their final leg"))
        self.DEP_tickBox.setText(_translate("startSoloDialog_AD", "DEP: vector departures to their first waypoint"))
        self.label.setText(_translate("startSoloDialog_AD", "Initial traffic count:"))
        self.label_2.setText(_translate("startSoloDialog_AD", "<html>\n"
"<head/>\n"
"<body>\n"
"<p>Notes on solo airport sessions:</p>\n"
"<ul>\n"
"<li>initial wind will be favourable to selected runways if any, randomised otherwise;</li>\n"
"<li>it is a good idea to check the solo session settings before starting the simulation.</li>\n"
"</ul>\n"
"</body>\n"
"</html>"))

