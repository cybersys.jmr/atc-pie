# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'startStudentSessionDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_startStudentSessionDialog(object):
    def setupUi(self, startStudentSessionDialog):
        startStudentSessionDialog.setObjectName("startStudentSessionDialog")
        startStudentSessionDialog.resize(273, 120)
        self.verticalLayout = QtWidgets.QVBoxLayout(startStudentSessionDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(startStudentSessionDialog)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.teachingServiceHost_edit = QtWidgets.QLineEdit(startStudentSessionDialog)
        self.teachingServiceHost_edit.setObjectName("teachingServiceHost_edit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.teachingServiceHost_edit)
        self.label_3 = QtWidgets.QLabel(startStudentSessionDialog)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.teachingServicePort_edit = QtWidgets.QSpinBox(startStudentSessionDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.teachingServicePort_edit.sizePolicy().hasHeightForWidth())
        self.teachingServicePort_edit.setSizePolicy(sizePolicy)
        self.teachingServicePort_edit.setMaximum(99999)
        self.teachingServicePort_edit.setObjectName("teachingServicePort_edit")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.teachingServicePort_edit)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(startStudentSessionDialog)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.label.setBuddy(self.teachingServiceHost_edit)
        self.label_3.setBuddy(self.teachingServicePort_edit)

        self.retranslateUi(startStudentSessionDialog)
        QtCore.QMetaObject.connectSlotsByName(startStudentSessionDialog)
        startStudentSessionDialog.setTabOrder(self.teachingServiceHost_edit, self.teachingServicePort_edit)

    def retranslateUi(self, startStudentSessionDialog):
        _translate = QtCore.QCoreApplication.translate
        startStudentSessionDialog.setWindowTitle(_translate("startStudentSessionDialog", "Start a student session"))
        self.label.setText(_translate("startStudentSessionDialog", "Teacher host:"))
        self.label_3.setText(_translate("startStudentSessionDialog", "Service port:"))
