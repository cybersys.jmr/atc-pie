# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'startFsdDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_startFsdDialog(object):
    def setupUi(self, startFsdDialog):
        startFsdDialog.setObjectName("startFsdDialog")
        startFsdDialog.resize(550, 514)
        self.verticalLayout = QtWidgets.QVBoxLayout(startFsdDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox_2 = QtWidgets.QGroupBox(startFsdDialog)
        self.groupBox_2.setObjectName("groupBox_2")
        self.formLayout_2 = QtWidgets.QFormLayout(self.groupBox_2)
        self.formLayout_2.setObjectName("formLayout_2")
        self.label_2 = QtWidgets.QLabel(self.groupBox_2)
        self.label_2.setObjectName("label_2")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.fsdServer_info = QtWidgets.QLabel(self.groupBox_2)
        self.fsdServer_info.setObjectName("fsdServer_info")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.fsdServer_info)
        self.label = QtWidgets.QLabel(self.groupBox_2)
        self.label.setObjectName("label")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label)
        self.callsign_edit = QtWidgets.QLineEdit(self.groupBox_2)
        self.callsign_edit.setObjectName("callsign_edit")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.callsign_edit)
        self.label_4 = QtWidgets.QLabel(self.groupBox_2)
        self.label_4.setObjectName("label_4")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.visibilityRange_edit = QtWidgets.QSpinBox(self.groupBox_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.visibilityRange_edit.sizePolicy().hasHeightForWidth())
        self.visibilityRange_edit.setSizePolicy(sizePolicy)
        self.visibilityRange_edit.setMinimum(10)
        self.visibilityRange_edit.setMaximum(600)
        self.visibilityRange_edit.setSingleStep(10)
        self.visibilityRange_edit.setObjectName("visibilityRange_edit")
        self.formLayout_2.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.visibilityRange_edit)
        self.use_landlines_tickBox = QtWidgets.QCheckBox(self.groupBox_2)
        self.use_landlines_tickBox.setObjectName("use_landlines_tickBox")
        self.formLayout_2.setWidget(3, QtWidgets.QFormLayout.SpanningRole, self.use_landlines_tickBox)
        self.verticalLayout.addWidget(self.groupBox_2)
        self.line = QtWidgets.QFrame(startFsdDialog)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.label_3 = QtWidgets.QLabel(startFsdDialog)
        font = QtGui.QFont()
        font.setItalic(True)
        self.label_3.setFont(font)
        self.label_3.setWordWrap(True)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.buttonBox = QtWidgets.QDialogButtonBox(startFsdDialog)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.label.setBuddy(self.callsign_edit)
        self.label_4.setBuddy(self.visibilityRange_edit)

        self.retranslateUi(startFsdDialog)
        QtCore.QMetaObject.connectSlotsByName(startFsdDialog)
        startFsdDialog.setTabOrder(self.callsign_edit, self.visibilityRange_edit)
        startFsdDialog.setTabOrder(self.visibilityRange_edit, self.buttonBox)

    def retranslateUi(self, startFsdDialog):
        _translate = QtCore.QCoreApplication.translate
        startFsdDialog.setWindowTitle(_translate("startFsdDialog", "Connect to FSD server"))
        self.groupBox_2.setTitle(_translate("startFsdDialog", "Session options"))
        self.label_2.setText(_translate("startFsdDialog", "Connecting to:"))
        self.fsdServer_info.setText(_translate("startFsdDialog", "###"))
        self.label.setText(_translate("startFsdDialog", "Callsign:"))
        self.label_4.setText(_translate("startFsdDialog", "Range:"))
        self.visibilityRange_edit.setSuffix(_translate("startFsdDialog", " NM"))
        self.use_landlines_tickBox.setText(_translate("startFsdDialog", "Enable ATC phone lines"))
        self.label_3.setText(_translate("startFsdDialog", "<html>\n"
"<body>\n"
"<p>Notes on FSD sessions:</p>\n"
"<ul>\n"
"  <li>ask pilots to prefix their &quot;real name&quot; setting with the ICAO type designator of their aircraft and a double slash, e.g. &quot;C172//Joanna Doe&quot;—this will allow to render the correct model in the FG views, and to display the correct XPDR info when you cheat it;</li>\n"
"  <li>radio text chat is tuned to the frequency publicised from the radio panel;</li>\n"
"  <li>handovers to non ATC-pie clients will be lossy (no details sent but callsign), and only ATC-pie clients will respond to who-has requests.</li>\n"
"</ul>\n"
"<p>Protocol limitations:</p>\n"
"<ul>\n"
"  <li>transponders are not capable of mode S, and squawked altitudes/FLs are not pressure-altitudes;</li>\n"
"  <li>flight plans can only be filed or amended by the pilots;</li>\n"
"  <li>RDF and CPDLC are not supported.</li>\n"
"</ul>\n"
"</body>\n"
"</html>"))
