# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'atcTextChat.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_atcTextChatPanel(object):
    def setupUi(self, atcTextChatPanel):
        atcTextChatPanel.setObjectName("atcTextChatPanel")
        atcTextChatPanel.resize(470, 379)
        self.verticalLayout = QtWidgets.QVBoxLayout(atcTextChatPanel)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.channel_info = QtWidgets.QLabel(atcTextChatPanel)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.channel_info.setFont(font)
        self.channel_info.setObjectName("channel_info")
        self.horizontalLayout_2.addWidget(self.channel_info)
        self.line = QtWidgets.QFrame(atcTextChatPanel)
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.horizontalLayout_2.addWidget(self.line)
        self.generalChatRoom_button = QtWidgets.QToolButton(atcTextChatPanel)
        self.generalChatRoom_button.setObjectName("generalChatRoom_button")
        self.horizontalLayout_2.addWidget(self.generalChatRoom_button)
        self.selectPrivateChannel_button = QtWidgets.QToolButton(atcTextChatPanel)
        self.selectPrivateChannel_button.setObjectName("selectPrivateChannel_button")
        self.horizontalLayout_2.addWidget(self.selectPrivateChannel_button)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.chatHistory_view = QtWidgets.QTableView(atcTextChatPanel)
        self.chatHistory_view.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.chatHistory_view.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.chatHistory_view.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.chatHistory_view.setShowGrid(False)
        self.chatHistory_view.setObjectName("chatHistory_view")
        self.chatHistory_view.horizontalHeader().setVisible(False)
        self.chatHistory_view.horizontalHeader().setStretchLastSection(True)
        self.chatHistory_view.verticalHeader().setVisible(False)
        self.verticalLayout.addWidget(self.chatHistory_view)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.msgLine_edit = QtWidgets.QLineEdit(atcTextChatPanel)
        self.msgLine_edit.setObjectName("msgLine_edit")
        self.horizontalLayout.addWidget(self.msgLine_edit)
        self.send_button = QtWidgets.QToolButton(atcTextChatPanel)
        self.send_button.setEnabled(True)
        self.send_button.setObjectName("send_button")
        self.horizontalLayout.addWidget(self.send_button)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.channel_info.setBuddy(self.msgLine_edit)

        self.retranslateUi(atcTextChatPanel)
        QtCore.QMetaObject.connectSlotsByName(atcTextChatPanel)
        atcTextChatPanel.setTabOrder(self.msgLine_edit, self.send_button)
        atcTextChatPanel.setTabOrder(self.send_button, self.generalChatRoom_button)
        atcTextChatPanel.setTabOrder(self.generalChatRoom_button, self.selectPrivateChannel_button)
        atcTextChatPanel.setTabOrder(self.selectPrivateChannel_button, self.chatHistory_view)

    def retranslateUi(self, atcTextChatPanel):
        _translate = QtCore.QCoreApplication.translate
        atcTextChatPanel.setWindowTitle(_translate("atcTextChatPanel", "ATC text chat"))
        self.channel_info.setText(_translate("atcTextChatPanel", "###"))
        self.generalChatRoom_button.setToolTip(_translate("atcTextChatPanel", "Go to general ATC channel"))
        self.generalChatRoom_button.setText(_translate("atcTextChatPanel", "G"))
        self.selectPrivateChannel_button.setToolTip(_translate("atcTextChatPanel", "Select private channel"))
        self.selectPrivateChannel_button.setText(_translate("atcTextChatPanel", "P..."))
        self.send_button.setText(_translate("atcTextChatPanel", "Send"))
