# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'atcCoord.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_atcPane(object):
    def setupUi(self, atcPane):
        atcPane.setObjectName("atcPane")
        atcPane.resize(338, 382)
        self.verticalLayout = QtWidgets.QVBoxLayout(atcPane)
        self.verticalLayout.setObjectName("verticalLayout")
        self.ATC_view = AtcTableView(atcPane)
        self.ATC_view.setAcceptDrops(True)
        self.ATC_view.setDragDropMode(QtWidgets.QAbstractItemView.DropOnly)
        self.ATC_view.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.ATC_view.setShowGrid(False)
        self.ATC_view.setObjectName("ATC_view")
        self.ATC_view.horizontalHeader().setVisible(False)
        self.ATC_view.verticalHeader().setVisible(False)
        self.ATC_view.verticalHeader().setDefaultSectionSize(35)
        self.verticalLayout.addWidget(self.ATC_view)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.call_button = QtWidgets.QToolButton(atcPane)
        self.call_button.setObjectName("call_button")
        self.horizontalLayout.addWidget(self.call_button)
        self.phoneSquelch_widget = QtWidgets.QWidget(atcPane)
        self.phoneSquelch_widget.setObjectName("phoneSquelch_widget")
        self.formLayout = QtWidgets.QFormLayout(self.phoneSquelch_widget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.phoneSquelch_widget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.phoneSquelch_edit = QtWidgets.QSpinBox(self.phoneSquelch_widget)
        self.phoneSquelch_edit.setMinimum(1)
        self.phoneSquelch_edit.setMaximum(20)
        self.phoneSquelch_edit.setObjectName("phoneSquelch_edit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.phoneSquelch_edit)
        self.horizontalLayout.addWidget(self.phoneSquelch_widget)
        spacerItem = QtWidgets.QSpacerItem(10, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.whoHas_button = QtWidgets.QToolButton(atcPane)
        self.whoHas_button.setObjectName("whoHas_button")
        self.horizontalLayout.addWidget(self.whoHas_button)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.label.setBuddy(self.phoneSquelch_edit)

        self.retranslateUi(atcPane)
        QtCore.QMetaObject.connectSlotsByName(atcPane)
        atcPane.setTabOrder(self.ATC_view, self.call_button)
        atcPane.setTabOrder(self.call_button, self.phoneSquelch_edit)
        atcPane.setTabOrder(self.phoneSquelch_edit, self.whoHas_button)

    def retranslateUi(self, atcPane):
        _translate = QtCore.QCoreApplication.translate
        self.call_button.setToolTip(_translate("atcPane", "Place a phone call"))
        self.call_button.setText(_translate("atcPane", "Call..."))
        self.label.setText(_translate("atcPane", "Squelch:"))
        self.phoneSquelch_edit.setToolTip(_translate("atcPane", "Adjust microphone sensitivity in phone calls"))
        self.whoHas_button.setToolTip(_translate("atcPane", "Send out a who-has query"))
        self.whoHas_button.setText(_translate("atcPane", "Who has?"))
from gui.widgets.atcTableView import AtcTableView
