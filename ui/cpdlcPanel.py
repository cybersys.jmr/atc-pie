# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'cpdlcPanel.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_cpdlcPanel(object):
    def setupUi(self, cpdlcPanel):
        cpdlcPanel.setObjectName("cpdlcPanel")
        cpdlcPanel.resize(331, 307)
        self.verticalLayout = QtWidgets.QVBoxLayout(cpdlcPanel)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.activeConnections_radioButton = QtWidgets.QRadioButton(cpdlcPanel)
        self.activeConnections_radioButton.setObjectName("activeConnections_radioButton")
        self.horizontalLayout.addWidget(self.activeConnections_radioButton)
        self.historyWith_radioButton = QtWidgets.QRadioButton(cpdlcPanel)
        self.historyWith_radioButton.setObjectName("historyWith_radioButton")
        self.horizontalLayout.addWidget(self.historyWith_radioButton)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.connections_tableView = QtWidgets.QTableView(cpdlcPanel)
        self.connections_tableView.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.connections_tableView.setShowGrid(False)
        self.connections_tableView.setObjectName("connections_tableView")
        self.connections_tableView.horizontalHeader().setVisible(False)
        self.connections_tableView.horizontalHeader().setStretchLastSection(True)
        self.connections_tableView.verticalHeader().setVisible(False)
        self.verticalLayout.addWidget(self.connections_tableView)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.closeOpenWindows_button = QtWidgets.QToolButton(cpdlcPanel)
        self.closeOpenWindows_button.setObjectName("closeOpenWindows_button")
        self.horizontalLayout_2.addWidget(self.closeOpenWindows_button)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.clearHistory_button = QtWidgets.QToolButton(cpdlcPanel)
        self.clearHistory_button.setObjectName("clearHistory_button")
        self.horizontalLayout_2.addWidget(self.clearHistory_button)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(cpdlcPanel)
        QtCore.QMetaObject.connectSlotsByName(cpdlcPanel)
        cpdlcPanel.setTabOrder(self.connections_tableView, self.activeConnections_radioButton)
        cpdlcPanel.setTabOrder(self.activeConnections_radioButton, self.historyWith_radioButton)
        cpdlcPanel.setTabOrder(self.historyWith_radioButton, self.closeOpenWindows_button)
        cpdlcPanel.setTabOrder(self.closeOpenWindows_button, self.clearHistory_button)

    def retranslateUi(self, cpdlcPanel):
        _translate = QtCore.QCoreApplication.translate
        self.activeConnections_radioButton.setToolTip(_translate("cpdlcPanel", "Show pending transfers and live data links"))
        self.activeConnections_radioButton.setText(_translate("cpdlcPanel", "Active links"))
        self.historyWith_radioButton.setText(_translate("cpdlcPanel", "History with callsign"))
        self.closeOpenWindows_button.setText(_translate("cpdlcPanel", "Close windows"))
        self.clearHistory_button.setToolTip(_translate("cpdlcPanel", "Clear terminated dialogues from CPDLC history"))
        self.clearHistory_button.setText(_translate("cpdlcPanel", "..."))
