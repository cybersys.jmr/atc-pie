
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

# First things first
UI_FILES:=mainWindow.py launcher.py

# Docks/frames
UI_FILES:=$(UI_FILES) fplPanel.py radarScope.py stripPane.py looseStripBay.py
UI_FILES:=$(UI_FILES) notifier.py notepads.py navigator.py weather.py radios.py
UI_FILES:=$(UI_FILES) atcCoord.py atcTextChat.py radioTextChat.py cpdlcPanel.py
UI_FILES:=$(UI_FILES) selectionInfoPane.py selectionInfoToolbarWidget.py
UI_FILES:=$(UI_FILES) instructions.py teachingConsole.py towerView.py

# Dialog boxes
UI_FILES:=$(UI_FILES) stripDetailsDialog.py fplDetailsDialog.py discardedStripsDialog.py
UI_FILES:=$(UI_FILES) envInfoDialog.py fgcomProcessDialog.py recordAtisDialog.py
UI_FILES:=$(UI_FILES) positionDrawingDialog.py routeSpecsLostDialog.py aboutDialog.py
UI_FILES:=$(UI_FILES) routeDialog.py editRackDialog.py rackVisibilityDialog.py
UI_FILES:=$(UI_FILES) rwyUseDialog.py cpdlcXfrOptionsDialog.py createTrafficDialog.py
UI_FILES:=$(UI_FILES) systemSettingsDialog.py soloSessionSettingsDialog.py
UI_FILES:=$(UI_FILES) generalSettingsDialog.py localSettingsDialog.py
UI_FILES:=$(UI_FILES) startSoloDialog_AD.py startFgSessionDialog.py startFsdDialog.py
UI_FILES:=$(UI_FILES) startStudentSessionDialog.py startTeacherSessionDialog.py

# Misc. widgets
UI_FILES:=$(UI_FILES) radiobox.py rwyParamsWidget.py xpdrCodeSelector.py
UI_FILES:=$(UI_FILES) weatherDispWidget.py quickReference.py routeEditWidget.py
UI_FILES:=$(UI_FILES) airportSearchWidget.py unitConv.py cpdlcConnection.py


.PHONY: all
all: $(UI_FILES)

%.py : %.ui
	pyuic5 -o $@ $<
