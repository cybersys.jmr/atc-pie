# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'startTeacherSessionDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_startTeacherSessionDialog(object):
    def setupUi(self, startTeacherSessionDialog):
        startTeacherSessionDialog.setObjectName("startTeacherSessionDialog")
        startTeacherSessionDialog.resize(343, 121)
        self.verticalLayout = QtWidgets.QVBoxLayout(startTeacherSessionDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label_3 = QtWidgets.QLabel(startTeacherSessionDialog)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.teachingServicePort_edit = QtWidgets.QSpinBox(startTeacherSessionDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.teachingServicePort_edit.sizePolicy().hasHeightForWidth())
        self.teachingServicePort_edit.setSizePolicy(sizePolicy)
        self.teachingServicePort_edit.setMaximum(99999)
        self.teachingServicePort_edit.setObjectName("teachingServicePort_edit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.teachingServicePort_edit)
        self.usePyAudio_tickBox = QtWidgets.QCheckBox(startTeacherSessionDialog)
        self.usePyAudio_tickBox.setObjectName("usePyAudio_tickBox")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.SpanningRole, self.usePyAudio_tickBox)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(startTeacherSessionDialog)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.label_3.setBuddy(self.teachingServicePort_edit)

        self.retranslateUi(startTeacherSessionDialog)
        QtCore.QMetaObject.connectSlotsByName(startTeacherSessionDialog)

    def retranslateUi(self, startTeacherSessionDialog):
        _translate = QtCore.QCoreApplication.translate
        startTeacherSessionDialog.setWindowTitle(_translate("startTeacherSessionDialog", "Start a teacher session"))
        self.label_3.setText(_translate("startTeacherSessionDialog", "Service port:"))
        self.usePyAudio_tickBox.setToolTip(_translate("startTeacherSessionDialog", "Requires PyAudio on both connecting hosts"))
        self.usePyAudio_tickBox.setText(_translate("startTeacherSessionDialog", "Enable radio and phone audio support"))
