# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'routeEditWidget.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_routeEditWidget(object):
    def setupUi(self, routeEditWidget):
        routeEditWidget.setObjectName("routeEditWidget")
        routeEditWidget.resize(309, 78)
        self.gridLayout_2 = QtWidgets.QGridLayout(routeEditWidget)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.route_edit = QtWidgets.QPlainTextEdit(routeEditWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.route_edit.sizePolicy().hasHeightForWidth())
        self.route_edit.setSizePolicy(sizePolicy)
        self.route_edit.setTabChangesFocus(True)
        self.route_edit.setObjectName("route_edit")
        self.gridLayout_2.addWidget(self.route_edit, 0, 0, 1, 1)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName("gridLayout")
        self.view_button = QtWidgets.QToolButton(routeEditWidget)
        self.view_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.view_button.setAutoRaise(True)
        self.view_button.setObjectName("view_button")
        self.gridLayout.addWidget(self.view_button, 0, 0, 1, 1)
        self.clear_button = QtWidgets.QToolButton(routeEditWidget)
        self.clear_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.clear_button.setAutoRaise(True)
        self.clear_button.setObjectName("clear_button")
        self.gridLayout.addWidget(self.clear_button, 1, 0, 1, 1)
        self.recallPreset_button = QtWidgets.QToolButton(routeEditWidget)
        self.recallPreset_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.recallPreset_button.setAutoRaise(True)
        self.recallPreset_button.setObjectName("recallPreset_button")
        self.gridLayout.addWidget(self.recallPreset_button, 0, 2, 1, 1)
        self.suggest_button = QtWidgets.QToolButton(routeEditWidget)
        self.suggest_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.suggest_button.setAutoRaise(True)
        self.suggest_button.setObjectName("suggest_button")
        self.gridLayout.addWidget(self.suggest_button, 0, 1, 1, 1)
        self.saveAsPreset_button = QtWidgets.QToolButton(routeEditWidget)
        self.saveAsPreset_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.saveAsPreset_button.setAutoRaise(True)
        self.saveAsPreset_button.setObjectName("saveAsPreset_button")
        self.gridLayout.addWidget(self.saveAsPreset_button, 1, 2, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 1, 1, 1)

        self.retranslateUi(routeEditWidget)
        QtCore.QMetaObject.connectSlotsByName(routeEditWidget)

    def retranslateUi(self, routeEditWidget):
        _translate = QtCore.QCoreApplication.translate
        self.view_button.setToolTip(_translate("routeEditWidget", "View route analysis"))
        self.view_button.setText(_translate("routeEditWidget", "V"))
        self.clear_button.setToolTip(_translate("routeEditWidget", "Clear route field"))
        self.clear_button.setText(_translate("routeEditWidget", "X"))
        self.recallPreset_button.setToolTip(_translate("routeEditWidget", "Recall a saved preset"))
        self.recallPreset_button.setText(_translate("routeEditWidget", "R"))
        self.suggest_button.setToolTip(_translate("routeEditWidget", "Suggest a route"))
        self.suggest_button.setText(_translate("routeEditWidget", "!"))
        self.saveAsPreset_button.setToolTip(_translate("routeEditWidget", "Save current entry as route preset"))
        self.saveAsPreset_button.setText(_translate("routeEditWidget", "S"))

