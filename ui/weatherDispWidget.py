# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'weatherDispWidget.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_weatherDispWidget(object):
    def setupUi(self, weatherDispWidget):
        weatherDispWidget.setObjectName("weatherDispWidget")
        weatherDispWidget.resize(229, 154)
        self.verticalLayout = QtWidgets.QVBoxLayout(weatherDispWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(weatherDispWidget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.wind_info = QtWidgets.QLabel(weatherDispWidget)
        self.wind_info.setObjectName("wind_info")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.wind_info)
        self.label_2 = QtWidgets.QLabel(weatherDispWidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.QNH_info = QtWidgets.QLabel(weatherDispWidget)
        self.QNH_info.setObjectName("QNH_info")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.QNH_info)
        self.label_3 = QtWidgets.QLabel(weatherDispWidget)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.visibility_info = QtWidgets.QLabel(weatherDispWidget)
        self.visibility_info.setObjectName("visibility_info")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.visibility_info)
        self.verticalLayout.addLayout(self.formLayout)
        self.line = QtWidgets.QFrame(weatherDispWidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.METAR_info = QtWidgets.QLabel(weatherDispWidget)
        self.METAR_info.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.METAR_info.setWordWrap(True)
        self.METAR_info.setObjectName("METAR_info")
        self.verticalLayout.addWidget(self.METAR_info)

        self.retranslateUi(weatherDispWidget)
        QtCore.QMetaObject.connectSlotsByName(weatherDispWidget)

    def retranslateUi(self, weatherDispWidget):
        _translate = QtCore.QCoreApplication.translate
        self.label.setText(_translate("weatherDispWidget", "Wind:"))
        self.wind_info.setText(_translate("weatherDispWidget", "N/A"))
        self.label_2.setText(_translate("weatherDispWidget", "QNH:"))
        self.QNH_info.setText(_translate("weatherDispWidget", "N/A"))
        self.label_3.setText(_translate("weatherDispWidget", "Visibility:"))
        self.visibility_info.setText(_translate("weatherDispWidget", "N/A"))
        self.METAR_info.setToolTip(_translate("weatherDispWidget", "Raw METAR string"))
        self.METAR_info.setText(_translate("weatherDispWidget", "N/A"))

