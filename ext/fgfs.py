
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from sys import stderr
from telnetlib import Telnet

from PyQt5.QtCore import QProcess, QThread

from base.coords import m2NM

from session.config import settings
from session.env import env

from gui.misc import signals


# ---------- Constants ----------

fgfs_viewing_acft_model = 'ufo'
dummy_viewer_callsign = 'ATC-pie'
telnet_read_timeout = 5 # seconds (or None)
initial_FOV = 55 # degrees of horizontal angle covered

ATCpie_model_string = 'ATC-pie'

# -------------------------------

FGFS_ACFT_recogniser = []
FGFS_ATC_recogniser = []

FGFS_model_chooser = {}    # str type dez -> (float height, float lat offset, float fwd offset)
FGFS_model_liveries = {}   # str type dez -> (str airline -> str livery file)


def is_ATC_model(fgfs_model):
	return fgfs_model == ATCpie_model_string or any(regexp.match(fgfs_model) for regexp in FGFS_ATC_recogniser)


# FGFS model -> ICAO type identification (used when reading FGMS packets)
def ICAO_aircraft_type(fgfs_model):
	return next((icao for regexp, icao in FGFS_ACFT_recogniser if regexp.match(fgfs_model)), fgfs_model)


def FGFS_model_position(icao_type_dez, coords, amsl, heading):
	try:
		model, (height, lat_offset, fwd_offset) = FGFS_model_chooser[icao_type_dez]
		lat_dir = heading + (90 if lat_offset >= 0 else 270)
		fwd_dir = heading if fwd_offset >= 0 else heading.opposite()
		return model, coords.moved(lat_dir, m2NM * abs(lat_offset)).moved(fwd_dir, m2NM * abs(fwd_offset)), amsl + height
	except KeyError:  # fallback on ICAO type designator itself
		return icao_type_dez, coords, amsl







def fgTwrCommonOptions():
	assert env.airport_data is not None
	pos, alt = env.viewpoint()
	return ['--lat=%s' % pos.lat, '--lon=%s' % pos.lon, '--altitude=%g' % alt, '--heading=360',
			'--aircraft=%s' % fgfs_viewing_acft_model, '--fdm=null']



class FlightGearTowerViewer:
	def __init__(self, gui):
		self.gui = gui
		self.internal_process = QProcess(gui)
		self.internal_process.setStandardErrorFile(settings.outputFileName('fgfs', ext='stderr'))
		self.internal_process.stateChanged.connect(lambda state: self.notifyStartStop(state == QProcess.Running))
		self.running = False
	
	def start(self):
		weather = env.primaryWeather()
		if settings.external_tower_viewer_process:
			self.notifyStartStop(True)
			if weather is not None:
				self.setWeather(weather)
		else:
			fgfs_options = fgTwrCommonOptions()
			fgfs_options.append('--roll=0')
			fgfs_options.append('--pitch=0')
			fgfs_options.append('--vc=0')
			fgfs_options.append('--fov=%g' % initial_FOV)
			# Env. options
			fgfs_options.append('--time-match-real')
			if weather is None:
				fgfs_options.append('--disable-real-weather-fetch')
			else:
				fgfs_options.append('--metar=%s' % weather.METAR()) # implies --disable-real-weather-fetch
			# Local directory options
			if settings.FGFS_root_dir != '':
				fgfs_options.append('--fg-root=%s' % settings.FGFS_root_dir)
			if settings.FGFS_aircraft_dir != '':
				fgfs_options.append('--fg-aircraft=%s' % settings.FGFS_aircraft_dir)
			if settings.FGFS_scenery_dir != '':
				fgfs_options.append('--fg-scenery=%s' % settings.FGFS_scenery_dir)
			# Connection options
			fgfs_options.append('--callsign=%s' % dummy_viewer_callsign)
			fgfs_options.append('--multiplay=out,100,localhost,%d' % settings.FGFS_views_send_port)
			fgfs_options.append('--multiplay=in,100,localhost,%d' % settings.tower_viewer_UDP_port)
			fgfs_options.append('--telnet=,,100,,%d,' % settings.tower_viewer_telnet_port)
			# Options for lightweight (interface and CPU load)
			fgfs_options.append('--disable-ai-traffic')
			fgfs_options.append('--disable-panel')
			fgfs_options.append('--disable-sound')
			fgfs_options.append('--disable-hud')
			fgfs_options.append('--disable-fullscreen')
			fgfs_options.append('--prop:/sim/menubar/visibility=false')
			# Now run
			self.internal_process.setProgram(settings.FGFS_executable)
			self.internal_process.setArguments(fgfs_options)
			#print('Running: %s %s' % (settings.FGFS_executable, ' '.join(fgfs_options)))
			self.internal_process.start()
		
	def stop(self, wait=False):
		if self.running:
			if settings.external_tower_viewer_process:
				self.notifyStartStop(False)
			else:
				self.internal_process.terminate()
				if wait:
					self.internal_process.waitForFinished() # default time-out
	
	def notifyStartStop(self, b):
		self.running = b
		signals.towerViewProcessToggled.emit(b)
	
	def sendCmd(self, cmd): # cmd can be a single command (str) or a command list
		if self.running:
			if isinstance(cmd, str):
				cmd = [cmd]
			TelnetSessionThreader(self.gui, cmd).start()
	
	# # # convenient methods for sending telnet commands
	
	def setWeather(self, weather):
		self.sendCmd('set /environment/metar/data %s' % weather.METAR())





def send_packet_to_views(udp_packet):
	if settings.controlled_tower_viewer.running:
		tower_viewer_host = settings.external_tower_viewer_host if settings.external_tower_viewer_process else 'localhost'
		send_packet_to_view(udp_packet, (tower_viewer_host, settings.tower_viewer_UDP_port))
		#print('Sent packet to %s:%d' % (tower_viewer_host, settings.tower_viewer_UDP_port))
	if settings.additional_views_active:
		for address in settings.additional_views:
			send_packet_to_view(udp_packet, address)


def send_packet_to_view(packet, addr):
	try:
		settings.FGFS_views_send_socket.sendto(packet, addr)
	except OSError as err:
		print('Error sending data to FG viewer.', str(err), file=stderr)






class TelnetSessionThreader(QThread):
	def __init__(self, gui, commands, loopInterval=None):
		"""
		"commands" can be a list of str commands or a function (no args) that generates the list
		"loopInterval" will keep the session open and repeat it after the given timeout
		"""
		QThread.__init__(self, gui)
		self.connection = None
		self.loop_interval = loopInterval
		self.generate_commands = (lambda: commands) if isinstance(commands, list) else commands
	
	def run(self):
		try:
			tower_viewer_host = settings.external_tower_viewer_host if settings.external_tower_viewer_process else 'localhost'
			#DEBUG('Creating Telnet connection to %s:%d' % (tower_viewer_host, settings.tower_viewer_telnet_port))
			self.connection = Telnet(tower_viewer_host, port=settings.tower_viewer_telnet_port)
			self.loop = self.loop_interval is not None
			self._runCommandsOnce()
			while self.loop: # set by an outside call to stop()
				QThread.msleep(self.loop_interval)
				self._runCommandsOnce()
			self.connection.close()
		except ConnectionError as err:
			print('Telnet connection error. Is tower view window open?', str(err), file=stderr)
	
	def stop(self):
		self.loop = False
	
	def _runCommandsOnce(self):
		for cmd in self.generate_commands():
			self.sendCommand(cmd)
		
	def sendCommand(self, line, answer=False):
		#DEBUGprint('Sending command: %s' % line)
		self.connection.write(line.encode('utf8') + b'\r\n')
		if answer:
			read = self.connection.read_until(b'\n', timeout=telnet_read_timeout)
			#DEBUG('Received bytes: %s' % read)
			return read.decode('utf8').strip()
